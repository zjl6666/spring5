package com.zjl.SpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.nio.charset.Charset;

/**
 * spring6
 *      jdk要求最低 17
 *      maven 3.6+
 *      idea:2022.1.2
 *      SpringBoot:3.x
 *
 * --spring.profiles.active=boot
 */
//@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
//@SpringBootConfiguration
//@ComponentScan("com.zjl.SpringBoot.第01章_基础")
@SpringBootApplication(
        //exclude = DataSourceAutoConfiguration.class   和关闭数据库的配置加载
        exclude = {DataSourceAutoConfiguration.class},
        scanBasePackages = {"com.zjl.SpringBoot.第01章_基础"}//默认就是此包 就是 run中类的 package
)//告诉这是一个spring boot程序
public class SpringBootMainApplication {

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","web");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(SpringBootMainApplication.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");

    }

}

