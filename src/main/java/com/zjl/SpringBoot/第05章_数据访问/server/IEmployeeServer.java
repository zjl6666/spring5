package com.zjl.SpringBoot.第05章_数据访问.server;


import com.zjl.SpringBoot.第05章_数据访问.bean.Employee;
import org.apache.ibatis.annotations.Param;

import java.io.IOException;
import java.util.List;

public interface IEmployeeServer {

    List<Employee> selectAll();
    Employee select(Employee employee);
    Employee selectById(@Param("id") int id);
    int insert(Employee employee);
    int deleteOne(Employee employee);
    int truncateAll();

}
