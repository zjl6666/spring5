package com.zjl.SpringBoot.第05章_数据访问.server.impl;

import com.zjl.SpringBoot.第05章_数据访问.bean.Employee;
import com.zjl.SpringBoot.第05章_数据访问.mapper.EmployeeMapper;
import com.zjl.SpringBoot.第05章_数据访问.server.IEmployeeServer;
import jakarta.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Random;

@Service
public class EmployeeServerImpl implements IEmployeeServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServerImpl.class);

    @Resource
    private EmployeeMapper employeeMapper;

    @Override
    public List<Employee> selectAll() {
        return employeeMapper.selectAll();
    }

    @Override
    public Employee select(Employee employee) {
        return employeeMapper.select(employee);
    }

    @Override
    public Employee selectById(int id) {
        return employeeMapper.selectById(id);
    }

    @Override
    public int insert(Employee employee) {
        return employeeMapper.insert(employee);
    }

    @Override
    public int deleteOne(Employee employee) {
        return employeeMapper.deleteOne(employee);
    }

    @Override
    public int truncateAll() {
        return employeeMapper.truncateAll();
    }


}
