package com.zjl.SpringBoot.第05章_数据访问.bean;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
@Schema(title = "用户表")
public class Employee {
    @Schema(title = "用户id")
    private Long id;
    @Schema(title = "名字")
    private String name;//名字
    @Schema(title = "年龄")
    private Integer age;//年龄
    @Schema(title = "性别")
    private Character sex;//性别
}
