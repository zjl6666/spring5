package com.zjl.SpringBoot.第05章_数据访问.controller;

import com.zjl.SpringBoot.第05章_数据访问.bean.Employee;
import com.zjl.SpringBoot.第05章_数据访问.server.IEmployeeServer;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@Tag(name = "用户",description = "部门的操作")
@RestController
@RequestMapping("employee")
public class EmployeeController {
    @Autowired
    IEmployeeServer iEmployeeServer;

    @Operation(description = "插入用户",summary = "保存新用户")
    @GetMapping("insert")//添加一条 http://127.0.0.1:7777/employee/insert?id=13&name=李四2&age=55&sex=男
    public String insert(Employee employee) throws IOException {
        return "插入了：" + iEmployeeServer.insert(employee) + "条";
    }
    @Operation(description = "查询用户",summary = "查询指定id老用户")
    @GetMapping("select")//查询 http://127.0.0.1:7777/employee/select?id=1
    public Employee select(Employee employee) throws IOException {
        return iEmployeeServer.select(employee);
    }
    @Operation(description = "查询用户",summary = "查询指定id老用户")
    @GetMapping("query/{id}")//查询一条 http://127.0.0.1:7777/employee/query/1
    public Employee query(@PathVariable("id") Integer id) throws IOException {
        return iEmployeeServer.selectById(id);
    }
    @GetMapping("selectAll")//查询所有 http://127.0.0.1:7777/employee/selectAll
    @Operation(description = "查询所有用户",summary = "查询所有老用户")
    public List<Employee> selectAll() throws IOException {
        return iEmployeeServer.selectAll();
    }


    @Operation(description = "清空所有用户",summary = "清空所有老用户")
    @GetMapping("truncateAll")//清空http://127.0.0.1:7777/truncateAll
    public Integer truncateAll() throws IOException {
        return iEmployeeServer.truncateAll();
    }

    @Operation(description = "删除指定用户",summary = "删除指定老用户")
    @GetMapping("deleteOne")//删除一条 http://127.0.0.1:7777/deleteOne?id=13
    public Integer deleteOne(Employee employee) throws IOException {
        return iEmployeeServer.deleteOne(employee);
    }
}
