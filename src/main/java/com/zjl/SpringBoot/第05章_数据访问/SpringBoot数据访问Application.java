package com.zjl.SpringBoot.第05章_数据访问;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.nio.charset.Charset;

/**
 * 在 application-数据访问.yml 的配置文件中  有
 * spring:
 *   sql:
 *     init:
 *       schema-locations: #DDL 脚本位置
 *         - classpath:employee.sql
 *       #data-locations: #DML 脚本位置
 * #      mode: always #初始化脚本 sql
 *       mode: never #不初始化脚本 sql
 * 记得改
 *
 */
@MapperScan(basePackages = "com.zjl.SpringBoot.第05章_数据访问.mapper")//可无
@SpringBootApplication()
public class SpringBoot数据访问Application {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","数据访问");
        SpringApplication.run(SpringBoot数据访问Application.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");
    }
}

