package com.zjl.SpringBoot.第05章_数据访问;

import com.baomidou.mybatisplus.autoconfigure.DdlApplicationRunner;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusLanguageDriverAutoConfiguration;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusProperties;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScannerRegistrar;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.StandardAnnotationMetadata;

/**
 * 我这里用的是 mybatis-plus
 * mybatis-plus-spring-boot-autoconfigure-3.5.6.jar
 *
 * 自动配置原理
 * @see com.baomidou.mybatisplus.autoconfigure.MybatisPlusInnerInterceptorAutoConfiguration
 * @see com.baomidou.mybatisplus.autoconfigure.IdentifierGeneratorAutoConfiguration
 * @see com.baomidou.mybatisplus.autoconfigure.MybatisPlusLanguageDriverAutoConfiguration
 * @see com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration
 *      所有配置 绑定在{@link MybatisPlusProperties}
 *      会在 @AutoConfigureAfter({DataSourceAutoConfiguration.class, MybatisPlusLanguageDriverAutoConfiguration.class})
 *      {@link DataSourceAutoConfiguration}
 *      {@link MybatisPlusLanguageDriverAutoConfiguration}
 *      这些配置配置完才加载
 *      会放入 {@link SqlSessionFactory} 创建和数据库的一次会话
 *      会放入 {@link SqlSessionTemplate} 操作数据库
 *      会放入 {@link DdlApplicationRunner}
 *
 *      每个 mapper 的接口代理对象怎么创建在容器中的
 *       {@link MapperScan} 中 利用 @Import({MapperScannerRegistrar.class})
 *       加载了  {@link MapperScannerRegistrar} 解析每一个 Mapper接口类常见bean定义信息，注册到容器中
 *
 *
 *
 *
 */
public class B_mybatis自动配置 {
    public static void main(String[] args) {
        StandardAnnotationMetadata standardAnnotationMetadata = new StandardAnnotationMetadata(SpringBoot数据访问Application.class);
        AnnotationAttributes mapperScanAttrs = AnnotationAttributes.fromMap(standardAnnotationMetadata.getAnnotationAttributes(MapperScan.class.getName()));
        System.out.println(mapperScanAttrs);
    }
}
