package com.zjl.SpringBoot.第05章_数据访问;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * 在 spring-boot-autoconfigure-3.2.7.jar  中
 * META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports 文件中
 * 会启动时的自动配置类
 *
 * 数据访问相关的:
 * @see org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
 *          数据源的数据配置 {@link DataSourceProperties} 在这个里面配置
 *          默认加载 {@link org.springframework.boot.autoconfigure.jdbc.DataSourceCheckpointRestoreConfiguration}
 *          默认使用 {@link org.springframework.boot.autoconfigure.jdbc.DataSourceConfiguration.Hikari}
 * @see org.springframework.boot.autoconfigure.jdbc.JdbcClientAutoConfiguration
 * @see org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration
 *          中 @Import({ ..., JdbcTemplateConfiguration.class,...}) 有个操作数据库的小工具
 *          {@link org.springframework.boot.autoconfigure.jdbc.JdbcTemplateConfiguration}
 *          {@link JdbcTemplate}
 * @see org.springframework.boot.autoconfigure.jdbc.JndiDataSourceAutoConfiguration
 * @see org.springframework.boot.autoconfigure.jdbc.XADataSourceAutoConfiguration
 *          分布式事务的数据源  基于 XA二阶提交协议
 * @see org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration
 *          支持事务
 *
 *
 */
public class A_spring_jdbc的自动配置 {
    public static void main(String[] args) {

    }
}
