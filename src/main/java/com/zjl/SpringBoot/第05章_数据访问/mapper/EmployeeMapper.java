package com.zjl.SpringBoot.第05章_数据访问.mapper;

import com.zjl.SpringBoot.第05章_数据访问.bean.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * mybatis 帮我自己找mapper下的xml
 */
@Mapper
public interface EmployeeMapper {

    List<Employee> selectAll();
    Employee select(Employee employee);
    Employee selectById(@Param("id") int id);
    int insert(Employee employee);
    int deleteOne(Employee employee);
    int truncateAll();

}
