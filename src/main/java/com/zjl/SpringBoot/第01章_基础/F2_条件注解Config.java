package com.zjl.SpringBoot.第01章_基础;

import com.zjl.SpringBoot.第01章_基础.bean.Pet;
import com.zjl.SpringBoot.第01章_基础.bean.User;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.data.ConditionalOnRepositoryType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ConditionalOnRepositoryType (org.springframework.boot.autoconfigure.data)
 * @ConditionalOnDefaultWebSecurity (org.springframework boot.autoconfigure.security)
 * @ConditionalOnSingleCandidate (org.springframework boot.autoconfigure.condition)
 * @ConditionalOnWebApplication (org.springframework boot.autoconfigure.condition)
 * @ConditionalOnWarDeployment (org.springframework.boot.autoconfigure.condition)
 * @ConditionalOnJndi (org.springframework.boot.autoconfigure.condition)
 * @ConditionalOnResource (org.springframework.boot.autoconfigure.condition)
 * @ConditionalOnExpression (org.springframework.boot.autoconfigure.condition)
 * @ConditionalOnClass (org.springframework.boot.autoconfigure.condition)
 * @ConditionalOnEnabledResourceChain (org.springframework boot.autoconfigure.web)
 * @ConditionalOnMissingClass (org.springframework boot.autoconfigure.condition)
 * @ConditionalOnNotWebApplication (org.springframework.boot.autoconfigure.condition)
 * @ConditionalOnProperty[ (org.springframework.boot.autoconfigure.condition)
 * @ConditionalOnCloudPlatform (org.springframework.boot.autoconfigure.condition)
 * @ConditionalOnBean (org.springframework.boot.autoconfigure.condition)
 * @ConditionalOnMissingBean (org.springframework.boot.autoconfigure.condition)
 * @ConditionalOnMissingFilterBean (org.springframework.boot.autoconfigure.web.servlet)
 * @Profile (org.springframework.context.annotation)
 * @ConditionalOnIlnitializedRestarter (org.springframework.boot.devtools.restart)
 * @ConditionalOnGraphQlSchema (org.springframework boot.autoconfigure graphql)
 * @ConditionalOnJava (org.springframework.boot.autoconfigure.condition)
 *
 *
 * 放在类上是对整个类判断，方法上是此方法的判断
 */
@Configuration
public class F2_条件注解Config {


    //推荐用全类名，因为全类名不存在不会抛错，用 .class 有次包才能 .class
    @ConditionalOnClass(value = {User.class},name = "com.zjl.SpringBoot.第01章_基础.bean.User")//如果类路径存在这个类则触发指定行为
    @ConditionalOnMissingClass//如果类路径不存在这个类则触发指定行为
    @ConditionalOnBean(value = {User.class})//如果存在这个Bean组件则触发指定行为
    @ConditionalOnMissingBean//如果不存在这个Bean组件则触发指定行为
    @Bean
    public Pet getPet(){
        Pet pet = new Pet();
        pet.setName("宠物");
        return pet;
    }
}
