package com.zjl.SpringBoot.第01章_基础;

/**
 * 1.简化配置
 *      导入相关的场景，拥有相关的功能。场景启动器
 *      默认支持的所有场景:
 *           https://docs.spring.io/spring-boot/reference/using/build-systems.html#using.build-systems.starters
 *      ●官方提供的场景:命名为: spring-boot-starter-*
 *      ●第三方提供场景:命名为: *-spring-boot-starter
 * 2.简化开发
 * 3.简化配置
 *      application.properties 一个端口即可
 *      默认支持的所有配置:
 *      https://docs.spring.io/spring-boot/appendix/application-properties/index.html#appendix.application-properties
 * 4.简化部署
 * 5.简化运维
 */
public class B_简化 {
}
