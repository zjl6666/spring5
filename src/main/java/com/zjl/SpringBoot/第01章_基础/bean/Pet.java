package com.zjl.SpringBoot.第01章_基础.bean;

import lombok.*;

@ToString//toString方法
@Data//get set方法
@NoArgsConstructor//无参构造器
@AllArgsConstructor//全参构造器
@EqualsAndHashCode//EqualsAndHashCode方法
public class Pet {//pet:宠物
    private String name;
    private Double weight;

}
