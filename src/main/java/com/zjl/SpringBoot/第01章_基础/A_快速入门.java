package com.zjl.SpringBoot.第01章_基础;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * SpringBoot帮我们简单、快速地创建一个独立的、生产级别的Spring应用
 * 大多数SpringBoot应用只需要编写少量配置即可快速整合Spring平台以及第三方技术
 * 特性:
 *      ●快速创建独立Spring应用
 *      ●接嵌入 Tomcat、Jetty or Undertow (无需部署war包)
 *          以前  linux java tomcat mysqL: war 放到tomcat的webapps下
 *          现在  jar: java环境;  java -jar
 *      ●提供可选的 starter,简化应用整合
 *          以前 场景启动器(starter) : web、json、 邮件、oss (对象存储)、步、定时任务、缓存...导包一堆,控制好版本。
 *          现在 为每一种场景准备了一个依赖; web-starter、mybatis-starter
 *      ●按需自动配置Spring以及第三方库
 *          如果这些场景我要使用(生效)。这个场景的所有配置都会自动配置好。
 *          约定大于配置:每个场景都有很多默认配置。
 *          自定义:配置文件中修改几项就可以
 *      ●提供生产级特性:如监控指标、健康检查、外部化配置等
 *          监控指标、健康检查(k8s)、外部化配置(加载外部配置文件)
 *      ●无代码生成、无xml
 *
 * 总结:简化开发,简化配置，简化整合，简化部署，简化监控,简化运维。
 *
 * 在jar包的同一目录放置 application.properties 文件，就会优先 此文件的配置
 *
 */
@Controller//表示我是一个配置类
@ResponseBody//表示返回是数据
@RestController//等于上面俩
public class A_快速入门 {

    @RequestMapping("/hello")//对外暴露的接口
    public String hello(){
        return "Hello,Spring Boot 3!!!!!!";//templates 会自己找  指定字符.html
    }


}
