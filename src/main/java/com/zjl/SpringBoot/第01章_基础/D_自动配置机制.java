package com.zjl.SpringBoot.第01章_基础;

import com.zjl.SpringBoot.SpringBootMainApplication;
import com.zjl.SpringBoot.第01章_基础.bean.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * ●自动配置的 Tomcat、 SpringMVC等
 *       以前: DispatcherServlet、 ViewResolver、 CharacterEncodingilt...
 *       现在:自动配置好的这些组件
 *
 * 默认的包的扫描路径：主程序所在的包及其子包
 *      @SpringBootApplication
 *      等同于
 *      @SpringBootConfiguration
 *      @EnableAutoConfiguration
 *      @ComponentScan
 *
 * 许多配置的 如
 * @see org.springframework.boot.autoconfigure.web.ServerProperties
 *      ●配置默认值
 *         配置文件的所有配置项是和某个类的对象值进行一绑定的。
 *          可参考官方文档
 *              https://docs.spring.io/spring-boot/appendix/application-properties/index.html#appendix.application-properties
 *        **按需加载
 *          每一个 spring-boot-starter 的pom里
 *          会有一个    spring-boot-starter  的pom里
 *          会有一个   spring-boot-autoconfigure  的pom里
 *      @see org.springframework.boot.autoconfigure
 *              存在了大量自动配置类，但是有些是不加载的，有这些包才会加载
 *              导入了哪些场景，加载那些场景
 *
 *
 *
 */
//@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
//@SpringBootConfiguration
//@ComponentScan("com.zjl.SpringBoot.第01章_基础")//扫描包的自定路径
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class},
        scanBasePackages = {"com.zjl.SpringBoot.第01章_基础"}//默认就是此包 就是 run中类的 package
)//告诉这是一个spring boot程序
public class D_自动配置机制 {

    public static void main(String[] args) {
        //java 10 局部类型自动推断
        if(false){
            var xx = SpringApplication.run(D_自动配置机制.class, args);
        }
        ConfigurableApplicationContext run = SpringApplication.run(D_自动配置机制.class, args);
        System.out.println("**************加载了哪些bean**************************************");
        String[] beanDefinitionNames = run.getBeanDefinitionNames();//获取加载了那些Bean
        for (String beanDefinitionName:beanDefinitionNames){
            System.out.println(beanDefinitionName);
        }

        String[] beanNamesForType = run.getBeanNamesForType(User.class);//获取容器中指定类型的所有相关


    }

}
