package com.zjl.SpringBoot.第01章_基础;

/**
 * 1、为什么导入starter-web 所有相关依赖都导入进来?
 *      ● 开发什么场景，导入什么场景启动器。
 *      ● maven依赖传递原则。A-B-C: A就拥有B和C
 *      ● 导入场景启动器。场景启动器自动把这个场景的所有核心依赖全部导入进来
 * 2、为什么版本号都不用写?
 *      每个boot项目都有一个父项目 spring-boot-starter-parent
 *              他的父项目 spring-boot-dependencies  里保存了大量常见jar依赖版本
 * 3、自定义版本号
 *      利用maven的就近原则
 *          。直接在当前项目properties 标签中声明父项目用的版本属性的key
 *          。直接在导入依赖的时候声明版本
 * 4、第三包的jar包
 *      ●boot父项目没有管理的需要自行声好
 *
 *
 *
 */
public class C_依赖管理机制 {
}
