package com.zjl.SpringBoot.第01章_基础;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigurationImportSelector;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * SpringBoot的自动配置完整流程
 * 1.导入 starter-web ：导入了web开发场景
 *      ●1.场景启动器导入了相关场景的所有依赖: starter-json、starter-tomcat、springmvc
 *      ●2、每个场景启动器都引入了一个spring-boot-starter ,核心场景动器。
 *      ●3、核心场景都引入了  spring-boot-autoconfigure 包
 *      ●4、spring-boot-autoconfigure 囊括了所有场景的所有配置
 *      ●5、只要这个包下的所有类生效，那么相当于SpringBoot馆方写好的整合功能就生效了。
 *      ●6、SpringBoot默认缺拄描不到 spring-boot-autoconfigure下写好的所有配置类。
 *          (这些配置给我们做了整合操作)，默认只扫描主程序所在包
 * 2.主程序  @SpringBootApplication 有三个注解生成
 *      1).@SpringBootConfiguration
 *      2).@EnableAutoConfiguration//自动开启自动配置-- 及扫描 spring-boot-autoconfigure
 *              ●@Import(AutoConfigurationImportSelector.class)//给容器中自动创建出这个类型的组件
 *                      AutoConfigurationImportSelector这个类就是批量导入组件
 *                          selectImports这个方法 就是导入逻辑
 *                              通过
 *                              @see org.springframework.boot.context.annotation.ImportCandidates 的load 获取需要自动装配那些
 *                              从 spring-boot-autoconfigure包的这个路径下加载自动配置类属性，并使用指定加载器加载
 *                              此文件在不同版本查看的文件不一致
 *                              //META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports
 *
 *                              利用@Import注解全部加载指定类 autoconfigure包下的所有类
 *              ●按需生效，通过条件注解 @ConditionalOnXxxx，注册
 *      3).@ComponentScan("扫描指定组件包")//默认扫描自己层级的包及其子包
 *  3.XxxxAutoConfiguration 会通过@Bean 注入大量注解
 *           如 Tomcat、Undertow、Netty、 等类
 *      @see org.springframework.boot.autoconfigure.web.embedded.EmbeddedWebServerFactoryCustomizerAutoConfiguration
 *      这些配置类都有 @EnableConfigurationProperties 这个类，就指定类和配置文件绑定，来指定其配置文件的配置
 *      把指定前缀属性封装到XxxProperties 属性中。作为容器中组件的核心参数 如端口号等
 *
 * 最佳实战
 *  ●选场景,导入到项目
 *      。官方: starter
 *      。第三方:去仓库搜 xxx-starter
 *  ●写配置，改配置文件关键项
 *      。数据库参数(连接地址、账号密码..
 *  ●分析这个场景给我们导入了哪些能用的组件
 *      。自动装配这些组件进行后续使用
 *      。不满意boot提供的自动配好的默认组件
 *          定制化
 *              ●改配置
 *              ●自定义组件
 *
 */
@EnableAutoConfiguration
@Import(AutoConfigurationImportSelector.class)

@SpringBootConfiguration
@ComponentScan
@SpringBootApplication(

        excludeName = {//不加载次类
                "org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration"
        },
        scanBasePackages = {"com.zjl.SpringBoot.第01章_基础"}//默认就是此包 就是 run中类的 package
)//告诉这是一个spring boot程序
public class SpringBoot的自动配置完整流程 {



    public static void main(String[] args) {

        //META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports
        System.out.println(String.format("META-INF/spring/%s.imports", AutoConfiguration.class.getName()));

        SpringApplication.run(SpringBoot的自动配置完整流程.class, args);
    }

}

