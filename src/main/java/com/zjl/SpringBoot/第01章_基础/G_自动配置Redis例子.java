package com.zjl.SpringBoot.第01章_基础;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @see  org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration
 * 中
 * @EnableConfigurationProperties(RedisProperties.class) 注解的
 *
 *@see  org.springframework.boot.autoconfigure.data.redis.RedisProperties
 * 写了redis的  properties  文件的配置
 *
 */
@SpringBootApplication(
        scanBasePackages = {"com.zjl.SpringBoot.第01章_基础"}//默认就是此包 就是 run中类的 package
)//告诉这是一个spring boot程序
@RequestMapping(value = "/redis")//请求地址必须唯一   否者报错，
@Controller
@ResponseBody//这个地址转换  不能使用 此注解，此注解的意思是返回值是数据，就不能被 templates 所识别了
public class G_自动配置Redis例子 {
    @Autowired
    RedisTemplate<Object, Object> redisTemplate;

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","redis");//  同理 --spring.profiles.active=属性绑定
        ConfigurableApplicationContext run = SpringApplication.run(G_自动配置Redis例子.class, args);

    }

    @RequestMapping(value = "/set")
    public void set(String key,String value){
        //http://127.0.0.1:8080/redis/set?key=k1&value=v1
        redisTemplate.opsForValue().set(key,value);
    }
    @RequestMapping(value = "/get")
    public Object get(String key){
        //http://127.0.0.1:8080/redis/get?key=k1
        return redisTemplate.opsForValue().get(key);
    }
    @RequestMapping(value = "/delete")
    public Boolean  delete(String key){
        //http://127.0.0.1:8080/redis/delete?key=k1
        return redisTemplate.delete(key);
    }
}
