package com.zjl.SpringBoot.第01章_基础;


import com.zjl.SpringBoot.第01章_基础.bean.Pet;
import com.zjl.SpringBoot.第01章_基础.bean.User;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * @Configuration
 * 最佳实战    如果有依赖  最好是true(单实例 每次使用都会查询内部有没有)
 *            如果没依赖  最好是false  这样少了一次遍历所有组件的时间  效率高
 *     配置类组件之间无依赖关系用Lite模式加速容器启动过程,减少判断
 *     配置类组件之间有依赖关系,方法会被调用得到之前单实例组件,用Full模式
 *
 *
 * 4.@Import({User.class})
 *      给容器中自动创建出这个类型的组件   默认组建的名称就是"全类名"
 *
 * @EnableConfigurationProperties({Car.class,Person.class})
 * //1.开启Car的配置绑定功能
 * //2.把这个Car的个组件自动注册到容器中
 *
 */
@Configuration//(proxyBeanMethods = false)//这是一个配置类 == 配置文件告诉spring boot这是一个配置类
// proxyBeanMethods = true SpringBoot总会检查这个组件是否在容器中有  保持单实例
// @Configuration(proxyBeanMethods = false)   不保持单实例


//@SpringBootConfiguration//这个和 @Configuration 一样的
@Import({User.class})//给容器中自动创建出这个类型的组件
//@ImportResource("classpath:beans.xml")//将 指定 xml 里的组件导入
//@EnableConfigurationProperties({Pet.class}) //1.开启Car的配置绑定功能 可以在 properties 中注册 //2.把这个Car的个组件自动注册到容器中
//@Controller       容器中添加此类这四个功能一致
//@Service          容器中添加此类这四个功能一致
//@Component        容器中添加此类这四个功能一致
//@Repository       容器中添加此类这四个功能一致
public class F1_组件注册Config {

    /**
     * 替代以前Bean的Bean标签
     *      优先 组件在容器中的名字是 @Bean(value = "user002") 的这个value值
     *      value为空 组件在容器中的名字是 方法名
     * @Scope
     *    singleton   单实例  默认     加载 spring 配置文件时候就会创建单实例对象
     *    prototype   多实例          不是在加载 spring 配置文件时候创建 对象，在调用时加载
     *    request     请求作用域
     *    session     会话作用域
     *    globalsession
     *
     * @return bean 对象
     */
    @Scope("singleton")
    @Bean(value = "user002")
    public User user001(){
        User user = new User();
        user.setName("张三");
        user.setAge(25);
        return user;
    }

}
