package com.zjl.SpringBoot.第01章_基础;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 *
 * @seeFile application-yml的使用.yml
 * @see Person
 * 在 这个文件存在 复杂的操作 application-属性绑定.yml
 *
 */
@SpringBootApplication(
        scanBasePackages = {"com.zjl.SpringBoot.第01章_基础"}//默认就是此包 就是 run中类的 package
)//告诉这是一个spring boot程序
public class H_yml的使用 {

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","yml的使用");//  同理 --spring.profiles.active=属性绑定
        ConfigurableApplicationContext run = SpringApplication.run(H_yml的使用.class, args);
        Person bean = run.getBean(Person.class);
        System.out.println(bean);
    }
}
