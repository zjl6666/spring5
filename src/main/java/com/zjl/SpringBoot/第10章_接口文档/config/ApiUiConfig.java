package com.zjl.SpringBoot.第10章_接口文档.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 如果大了，可以分组
 */
@Configuration
public class ApiUiConfig {
    @Bean
    public GroupedOpenApi api01(){
        return GroupedOpenApi.builder()
                .group("用户管理")
                .pathsToMatch("/employee/**","xxx")
                .build();
    }
    @Bean
    public GroupedOpenApi api02(){
        return GroupedOpenApi.builder()
                .group("静态")
                .pathsToMatch("/starter/**")
                .build();
    }

    //给文档页面添加 值
    @Bean
    public OpenAPI docsOpenApi(){
        Info info = new Info()
                .title("Swagger测试 文档")
                .description("测试的接口文档")
                .version("v1.0.0")
                .license(new License().name("Apache 2.0").url("http://springdoc.org"))
                ;
        ExternalDocumentation externalDocumentation = new ExternalDocumentation();
        externalDocumentation.description("哈哈，百度一下").url("https://www.baidu.com/");
        return new OpenAPI().info(info).externalDocs(externalDocumentation);
    }
}
