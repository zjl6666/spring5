package com.zjl.SpringBoot.第10章_接口文档;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.charset.Charset;

/**
 * Swagger可以快速生成实时接口文档，方便前后开发人员进行协调沟通。遵循OpenAPI规范。
 *
 * 详见
 * @see com.zjl.SpringBoot.第05章_数据访问
 *
 * 使用
 * @see com.zjl.SpringBoot.第05章_数据访问.SpringBoot数据访问Application 启动
 *
 * http://127.0.0.1:7777/swagger-ui/index.html
 * 会进入接口
 *
 *
 * 注解           标注位置                作用
 * @Tag         controller类             标识controller作用
 * @Parameter   参数                      标识参数作用
 * @Parameters  参数                      参数多重说明
 * @Schema      model层的JavaBean         描述模型作用及每个属性
 * @Operation       方法                  描述方法作用
 * @ApiResponse     方法                  描述响应状态等
 */
@SpringBootApplication(scanBasePackages={"com.zjl.SpringBoot.第05章_数据访问","com.zjl.SpringBoot.第10章_接口文档.config"})
public class OpenAPI3与Swagger {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","数据访问");
        SpringApplication.run(OpenAPI3与Swagger.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");
    }
}
