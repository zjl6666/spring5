package com.zjl.SpringBoot.第04章_模板引擎_thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;
import org.thymeleaf.spring6.templateresolver.SpringResourceTemplateResolver;

import java.nio.charset.Charset;

/**
 * ●于SpringBoot使用了嵌入式Servlet容器。所以JSP默认是不能使用的。
 * ●如果需要服务端页面渲染,优先考虑使用模板引擎。
 *
 * 本地使用 templates
 * {@link ThymeleafAutoConfiguration} 的 defaultTemplateResolver 方法
 * 里有自动注入  {@link SpringResourceTemplateResolver}  这个类
 * 发现默认路径  classpath:/templates/    默认后缀名：    .html
 *
 *
 *
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class A_简介 {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","thymeleaf");//  同理 --spring.profiles.active=xxx
        SpringApplication.run(A_简介.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");

    }
}
