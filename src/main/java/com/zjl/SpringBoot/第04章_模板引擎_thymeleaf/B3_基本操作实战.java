package com.zjl.SpringBoot.第04章_模板引擎_thymeleaf;

import com.zjl.SpringBoot.第04章_模板引擎_thymeleaf.bean.LoginUser;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

//import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * templates这个静态文件  都是经过处理的 html文件的访问不需要加   .html
 *
 * th:each="loginUser,stats:${loginUsers}"
 *
 * stats：状态字段
 * ● index:当前遍历元素的索引，从0开始
 * ● count: 当前遍历元素的索引，从1开始
 * ● size: 需要遍历元索的总数量
 * ● current: 当前正在遍历的元素对象
 * ● even/odd: 否偶数/奇数行
 * ● first:是否第一个元素
 * ● last:是否最后一个元素
 *
 *
 *
 *  三元运算符
 * ----a>b?a:b      ${loginUser.getAge()>=18?'成年':'未成年'}
 *
 *
 * ---switch   case  case
 *    <td th:switch="${loginUser.role}">
 *        <button th:case="'gly'">管理员</button>
 *        <button th:case="'rs'">人事</button>
 *        <button th:case="'jl'">经理</button>
 *        <button th:case="'nm'">牛马</button>
 *        <button th:case="'bj'">保洁</button>
 *    </td>
 * ----
 *  th:if
 *    <td th:if="${#strings.isEmpty(loginUser.email)}" th:text="'没有邮箱'"></td>
 *    <td th:if="${not #strings.isEmpty(loginUser.email)}" th:text="${loginUser.email}"></td>
 *
 * ----优先级
 *
 *Order                 Feature                 Attributes
 * 1                    片段包含                  th:insert th:replace
 * 2                    遍历                     th:each
 * 3                    判断                     th:if th:unless    th:switchth:case
 * 4                    定义本地变量               th:object th:with
 * 5                通用方式属性修改                th:attr  th:attrprepend  th:attrappend
 * 6                    指定属性修改                th:value th:href th:src ..
 * 7                    文本值                     th:text th:utext
 * 8                    片段指定                   th:fragment
 * 9                    片段移除                   th:remove
 *
 *
 */
@Controller
public class B3_基本操作实战 {

    @GetMapping("/login")
    public String loginDemo(Model model){

        model.addAttribute("msg","游客：" + System.currentTimeMillis());

        int i;
        //全部动态  th:attr
        i = new Random().nextInt(3) + 1;
        model.addAttribute("src","/img/" + i + ".webp");
        i = new Random().nextInt(3) + 1;
        model.addAttribute("style","width:" + (i*100) + "px;");

        return "login";
    }

    @PostMapping("/login")
    public String index(LoginUser loginUser, HttpSession session, Model model){


        if("张蛟龙".equals(loginUser.getUsername()) &&
                StringUtils.hasLength(loginUser.getPassword())){//判断是否有长度
            //登录成功的用户保存一下 以后使用方便
            session.setAttribute("loginUser",loginUser);
            return "redirect:/04_table";//重定向 防止表单重复提交//RedirectAttributes ra  // 重定向携带的数据
        }else {
            model.addAttribute("msg","账号密码错误");
            return "login";//回到登录界面
        }

    }


    @GetMapping("/04_table")
    public String table(Model model){
         List<LoginUser> loginUsers = Arrays.asList(
                 new LoginUser("张三", "333",8,"gly","xxx1@qq.com"),
                 new LoginUser("李四", "444",15,"rs","xxx2@qq.com"),
                 new LoginUser("王五", "555",22,"jl","xxx3@qq.com"),
                 new LoginUser("赵六", "666",80,"nm",null),
                 new LoginUser("钱七", "777",90,"bj","xxx5@qq.com"),
                 new LoginUser("宋八", "888",6,"bj",null)
         );
        model.addAttribute("loginUsers",loginUsers);
        return "04_table";
    }

}
