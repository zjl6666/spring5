package com.zjl.SpringBoot.第04章_模板引擎_thymeleaf;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Random;


/**
 * th:xxx :动态渲染指定的html标签属性值、或者th指令(遍历、判断等)
 * ●th:text :标签体内文本值渲染
 *      th:utext : 转义
 * ●th:属性:标签指定属性渲染.
 *      #让所有访问路径都加上指定路径 ，这样的话，会让几乎所有访问都失效，可有用
 *      server.servlet.context-path=/demo
 *      th:src="@{xlr.jpg}"   这样的话，他会自己动态将前缀给加上 @{} 在thymeleaf中专门取路径
 *      th:src="@{${jpgUrl}}"   这 样既可以动态，又可以 自动加 配置的路径
 *
 * ●th:attr :标签任意属性渲染
 * ●th:if th:each ... 其他th指令
 *
 *
 * http://127.0.0.1:8082
 */
@Controller
@Slf4j
public class B1_th基础语法 {

    @GetMapping("/")
    public String hello(){
        return "hello";
    }

    @GetMapping("/target")
    public String target(){
        return "target";
    }

    @RequestMapping(
            value = "/04_b1"
    )//http://127.0.0.1:8082/04_b1?name=张三
    public String loginDemo(@RequestParam(name = "name",required = false) String name, Model model){
        if(name == null){
            name = "游客：" + System.currentTimeMillis();
        }
        model.addAttribute("msg",name);


        String text = "<span style=\"color: red\">" + name + "</span>";

        //  th:text  和  th:utext 的转义和不转义
        model.addAttribute("text",text);

        int i = new Random().nextInt(3) + 1;
        //动态  替换 图片    th:src="${jpgUrl}"
        model.addAttribute("jpgUrl","/img/" + i + ".webp");

        //全部动态  th:attr
        i = new Random().nextInt(3) + 1;
        model.addAttribute("src","/img/" + i + ".webp");
        i = new Random().nextInt(3) + 1;
        model.addAttribute("style","width:" + (i*100) + "px;");

        model.addAttribute("show",new Random().nextBoolean());
        log.info("model的值：{}" ,model);
        return "04_b1";
    }

}
