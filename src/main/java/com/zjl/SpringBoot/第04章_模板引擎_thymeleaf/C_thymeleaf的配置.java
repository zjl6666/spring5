package com.zjl.SpringBoot.第04章_模板引擎_thymeleaf;

/**
 * #spring.thymeleaf
 * #默认 thymeleaf 的 html 路径
 * spring.thymeleaf.prefix=classpath:/templates/
 *
 * #默认 thymeleaf 后缀
 * spring.thymeleaf.suffix=.html
 * #缓存 默认 启动      建议开发期间关闭
 * spring.thymeleaf.cache=false
 *
 * #是否检查 @xxxMapping 的String 返回值所找的文件是否在 templates 文件下  未找到会抛错  不检查会提速  默认 true
 * #spring.thymeleaf.check-template=false
 */
public class C_thymeleaf的配置 {

}
