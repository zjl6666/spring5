package com.zjl.SpringBoot.第04章_模板引擎_thymeleaf.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginUser {
    private String username;
    private String password;
    private Integer age;
    private String role;//角色
    private String email;//邮箱
}
