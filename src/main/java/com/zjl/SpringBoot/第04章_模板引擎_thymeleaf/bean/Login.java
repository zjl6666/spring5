package com.zjl.SpringBoot.第04章_模板引擎_thymeleaf.bean;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("login")//指定绑定的 表名字  默认和文件名字一样（不区分大小写）
public class Login {
    @TableField(exist = false)//表示表中不存在这些数据
    private String username;


    private Long id;
    private String name;
    private Integer age;
    private String email;
}
