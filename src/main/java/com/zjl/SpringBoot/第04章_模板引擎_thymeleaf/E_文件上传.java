package com.zjl.SpringBoot.第04章_模板引擎_thymeleaf;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpSession;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * 已经自动配置好了文件上传解析器
 * 如果自己配制的有   优先使用用户配置的
 *
 * 执行流程  再doDispatch() 会判断是否是一个文件上传
 * 前端 enctype="multipart/form-data">  有这个  说明是文件上传
 * 只有一个文件上传解析器，直接解析
 *
 */
@Controller
public class E_文件上传 {


    @GetMapping("/filesc")
    public String filesc(){
        return "filesc";
    }

    /**
     * MultipartFile 自动封装上传过来的文件
     */
    @PostMapping("/upload")
    public String upload(@RequestParam("email") String email,
                         @RequestParam("username") String username,
                         @RequestPart("headerImg") MultipartFile headerImg,//封装文件
                         @RequestPart("photos") MultipartFile[] photos){

        System.out.print("    邮箱"+email);
        System.out.print("    名字"+username);
        System.out.print("    头像"+headerImg.getSize());
        System.out.println("    生活照个数"+photos.length);
        if(!headerImg.isEmpty()){
            //保存到文件服务器，OSS服务器
            headerImg.getOriginalFilename();//获取文件原始名字
            File file = new File("file/" + headerImg.getOriginalFilename());
            try {//相对路径   有问题
                //不知道为啥  直接  new File   不行   需要 再套一层  file.getAbsoluteFile()
                headerImg.transferTo(file.getAbsoluteFile());//file.getAbsoluteFile() 获取绝对路径的 fils
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(photos.length>0){
            for (MultipartFile m:photos) {
                if(!m.isEmpty()){
                    //保存到文件服务器，OSS服务器
                    try {
                        File file = new File("file/" + m.getOriginalFilename());
                        m.transferTo(file.getAbsoluteFile());
                    } catch (IOException e) { e.printStackTrace();}
                }
            }
        }
        return "redirect:/filesc";
    }




    @GetMapping("/files")
    public String files(){
        return "files";
    }

    @RequestMapping("/testDown")//文件下载
    public ResponseEntity<byte[]> testResponseEntity(HttpSession session) throws IOException {
        //获取ServletContext对象
        ServletContext servletContext = session.getServletContext();
        //获取服务器中文件的真实路径   但是由于 HttpSession在tomcat里，就会找 tomcat 路径
        String realPath = servletContext.getRealPath("static/img/1.webp");

        //创建输入流                                    相对服务的路径  不会从 resources 找
        InputStream is = new FileInputStream("file/img/1.webp");
        //创建字节数组
        byte[] bytes = new byte[is.available()];
        //将流读到字节数组中
        is.read(bytes);
        //创建HttpHeaders对象设置响应头信息
        MultiValueMap<String, String> headers = new HttpHeaders();
        //设置要下载方式以及下载文件的名字
        headers.add("Content-Disposition", "attachment;filename=1.webp");
        //设置响应状态码
        HttpStatus statusCode = HttpStatus.OK;
        //创建ResponseEntity对象
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(bytes, headers, statusCode);
        //关闭输入流
        is.close();
        return responseEntity;
    }

    @ResponseBody
    @RequestMapping("/testUp")//文件上传
    public String testUp(MultipartFile photo, HttpSession session) throws IOException {
        //获取上传的文件的文件名
        String fileName = photo.getOriginalFilename();
        //获取上传的文件的后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        //将UUID作为文件名  文件名字一样  会把原来的"文件的内容"覆盖掉(不是文件覆盖)
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        //将uuid和后缀名拼接后的结果作为最终的文件名
        fileName = uuid + suffixName;
        //通过ServletContext获取服务器中photo目录的路径   这里获取的是  tomcat 的路径
        ServletContext servletContext = session.getServletContext();
        String photoPath = servletContext.getRealPath("photo");

        File file = new File("file/photo");
        //判断对应路径是否存在
        if(!file.exists()){
            //若不存在，则创建目录
            file.mkdirs();
        }
        String finalPath = file.getAbsolutePath() + File.separator + fileName;
        //上传文件
        photo.transferTo(new File(finalPath));
        return "上传成功";
    }

}
