package com.zjl.SpringBoot.第04章_模板引擎_thymeleaf;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * messages.properties          默认
 * messages_en_GB.properties    英国
 * messages_en_US.properties    美国
 * messages_zh_CN.properties    中国
 *
 * 在配置文件中 添加文件
 * 默认 messages.properties
 *
 * th:text="#{a}  在thymeleaf
 * 中会去从国际化的文件中找替换的
 *
 *在报文头  Accept-Language = zh-CN
 *在报文头  Accept-Language = en-GB
 * 等等
 *
 * http://127.0.0.1:8082/getKey?key=b
 */
@RestController
public class D_国际化的使用 {

    @Autowired
    MessageSource messageSource;

    @GetMapping("/getKey")
    public String getKey(HttpServletRequest request, @RequestParam("key") String key){
        Locale locale = request.getLocale();

        //获取 国际化指定的值
        String a = messageSource.getMessage(key, null, locale);

        return a;
    }

}
