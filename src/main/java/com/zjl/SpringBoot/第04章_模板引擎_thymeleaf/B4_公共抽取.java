package com.zjl.SpringBoot.第04章_模板引擎_thymeleaf;

/**
 * 表示 <div>  标签页设置一个 片段名
 * <div th:fragment="shouye">
 *     xxxxx
 *     </div>
 *
 * 引用  片段           html名    片段fragment的名
 * <div th:include="hello :: shouye"></div>
 * <div th:insert="hello :: shouye"></div
 * <div th:replace="~{hello :: shouye}"></div>
 */
public class B4_公共抽取 {
    public static void main(String[] args) {

    }
}
