package com.zjl.SpringBoot.第04章_模板引擎_thymeleaf;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 表达式:用来动态取值
 * ● ${} :变量取值;使用model共享给页面的值都直接用${}
 * ● @{} : url路径;  可以动态配合 #server.servlet.context-path=/demo  设置所有路径的前置路径
 * ● #{}:国际化消息
 * ● ~{}:段引用
 * ● *{} :选择:要配合th:object绑定对象
 *
 *
 * https://www.thymeleaf.org/doc/tutorials/3.1/usingthymeleaf.html
 *
 * 系统工具&内置对象:详细文档
 * ● param :请求参数对象
 * ● session : session对象
 * ● application : application对象
 * ● #execInfo:模板执行信息
 * ● #messages :国际化消息
 * ● #uris : uri/url工I具
 * ● #conversions :类型转换工具
 * ● #dates :日期工具，是 java.util.Date对象的工具类
 * ● #calendars :类似#dates,只不过是java.util.calendar 对象的工具类
 * ● #temporals : JDK8+ java.time API工具类
 * ● #numbers :数字操作工具
 * ● #strings :字符串操作
 * ● #objects :对象操作
 * ● #bools : bool操作
 * ● #arrays : array工具
 * ● #lists : list工具
 * ● #sets : set工 具
 * ● #maps : map工具
 * ● #aggregates :集合聚合工具(sum、avg)
 * ● #ids : id生成工具
 *
 * 文本操作:
 *      ●拼串:.+
 *      ●文本替换:| The name is ${name}|
 * 布尔操作:
 *      ●二进制运算: and,or
 *      ●取反: !,not
 * 比较运算:
 *      ●比较: >, <, <=, >= (gt, lt, ge,le)
 *      ●等值运算: ==,!= (eq, ne)
 * 条件运算:
 *      ●if-then: (if)?(then)
 *      ●if-then-else: (if)?(then):(else)
 *      ●default: (value)?:(defaultValue)
 * 特殊语法:
 *      ●无操作: _
 *
 */
@Controller
@Slf4j
public class B2_动态取值和函数 {

    @RequestMapping(
            value = "/04_b2"
    )//http://127.0.0.1:8082/04_b1?name=张三
    public String demo(@RequestParam(name = "name",required = false) String name, Model model){
        if(name == null){
            name = "游客：" + System.currentTimeMillis();
        }
        model.addAttribute("name",name);

        model.addAttribute("函数","abc123XYZ");
        log.info("model的值：{}" ,model);
        return "04_b2";
    }

}
