package com.zjl.SpringBoot.第09章_中间件.消息队列Kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.nio.charset.Charset;

/**
 * ****单台 kafka 启动****************************************************************
 * source /etc/profile
 * cd /www/kafka/kafka_2.12-3.8.0/bin/
 * sh kafka-server-start.sh  ../config/server.properties
 * ***********************************************************************************
 * 消息队列场景
 *      1.异步        将消息放入 消息队列 ，我就可以结束了，其他异步处理
 *      2.解耦       系统之间需要交互时，如果一方的接口更改，另一方也需要更改，这样就改动太大，而放到消息队列中，就无需改动
 *      3.削峰       秒杀时，将用户消息存入消息队列，后台一条一条处理，不至于让系统垮掉
 *      4.缓冲       日志分析，从大量地方采集数据，放入消息队列，在进行订阅让某一系统进行处理
 *
 * ***********************************************************************************
 * Kafka原理
 *      分区:海量数据分散存储
 *          每个分区存入不同机器
 *      副本:每个数据区都有备份
 *          备份，防止主数据丢失
 * 如，三个分区，两个备份，
 *   就有九块数据
 *    其中有三个是主数据，构成完整的数据，
 *    其他六个构成两个完整数据
 *
 * 同一个消费者组里面的消费者是队列竞争模式（只有一个消费的数据不冲突）
 * 不同消费者组里面的消费者是发布/订阅模式
 *
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class A_场景 {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","kafka");
        SpringApplication.run(A_场景.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");

    }
}
