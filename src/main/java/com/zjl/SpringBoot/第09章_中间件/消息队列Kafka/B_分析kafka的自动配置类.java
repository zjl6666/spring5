package com.zjl.SpringBoot.第09章_中间件.消息队列Kafka;

import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * kafka的自动配置
 * @see KafkaAutoConfiguration
 *
 * 配置文件配置  {@link KafkaProperties}
 * 开启kafka注解驱动功能  {@link EnableKafka}
 * 收发消息  {@link KafkaTemplate}
 * 操作主题  {@link KafkaAdmin}
 *
 *
 */
public class B_分析kafka的自动配置类 {
    public static void main(String[] args) {

    }
}
