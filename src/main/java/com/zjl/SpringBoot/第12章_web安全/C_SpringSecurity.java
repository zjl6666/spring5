package com.zjl.SpringBoot.第12章_web安全;

import com.zjl.SpringBoot.SpringBootMainApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityDataConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityFilterAutoConfiguration;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.Charset;

/**
 * Security场景自动配置类 {@link SecurityAutoConfiguration}
 *              配置信息 {@link SecurityProperties}   spring.security 开头
 *               {@link org.springframework.boot.autoconfigure.security.servlet.SpringBootWebSecurityConfiguration}   spring.security 开头
 *                     {@link SecurityFilterChain}   ：默认的安全策略  1.所有请求需要认证  2.开启表单登录
 *                     {@link EnableWebSecurity}   web安全配置
 *                          {@link WebSecurityConfiguration}
 *                          {@link SpringWebMvcImportSelector}
 *                          {@link OAuth2ImportSelector}
 *                          {@link HttpSecurityConfiguration} http的安全规则
 *                          {@link EnableGlobalAuthentication} 全局认证
 *                               {@link AuthenticationConfiguration} 一堆东西
 *               {@link SecurityDataConfiguration}
 *
 *
 * Security场景自动配置类 {@link SecurityFilterAutoConfiguration}
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class C_SpringSecurity {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","security");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(C_SpringSecurity.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");

    }

}
