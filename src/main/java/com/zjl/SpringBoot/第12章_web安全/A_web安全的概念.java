package com.zjl.SpringBoot.第12章_web安全;

/**
 * ******************************************************************************************************
 * 认证: Authentication
 *
 * 授权: Authorization
 *
 * 攻击防护
 *      XSS  (Cross-site scripting)
 *          跨站脚本攻击
 *      CSRF (Cross-site request forgery)
 *          跨站请求伪造
 *      CORS (Cross-Origin Resource Sharing)
 *          跨域请求资源共享
 *      SQL  注入
 * ******************************************************************************************************
 *
 * 扩展.权限模型
 * 1.RBAC       用户->角色->权限
 *      ●用户(t_user)
 *      ●角色(t_role)
 *      ●权限(t_permission)
 * 2. ACL       用户->权限
 *      ●直接用户和权限挂钩
 *      ●用户(t_user)
 *      ●权限(t_permission)
 *
 */
public class A_web安全的概念 {

}
