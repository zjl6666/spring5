package com.zjl.SpringBoot.第12章_web安全;

/**
 * 1.过滤器链架构
 * Spring Security利用 FilterChainProxy封装一系列拦截器链， 实现各种安全拦截功能
 * Servlet三大组件: Servlet、 Filter、Listener
 * 2. FilterChainProxy
 *      对 访问路径进行权限管控
 * 3. SecurityFilterChain
 *      认证
 *
 * 1. HttpSecurity
 * 2. MethodSecurity
 *
 * {@link WebSecurityConfigurerAdapter}
 * {@link EnableGlobalMethodSecurity}: 开启全局方法安全配置
 *      {@link Secured}
 *      {@link PreAqthorize}
 *      {@link PostAuthorize}
 *
 * {@link UserDetailService}
 *      去数据据库查询用户详细信息的service用户基本信息、用户角色、用户权限)
 *
 */
public class B_SpringSecurity原理 {

}
