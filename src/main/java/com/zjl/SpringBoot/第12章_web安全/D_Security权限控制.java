package com.zjl.SpringBoot.第12章_web安全;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@EnableMethodSecurity//开启方法的权限控制
public class D_Security权限控制 {

    @RequestMapping("/login")
    public String login(){
        return "security";
    }
    @RequestMapping("/yes")
    @ResponseBody
    public String yes(){
        return "登陆成功";
    }

    @PreAuthorize("hasAuthority('hr')")
    @RequestMapping("/hr")
    @ResponseBody
    public String js(){
        return "我是角色hr";
    }
    @PreAuthorize("hasAuthority('admin')")
    @RequestMapping("/admin")
    @ResponseBody
    public String admin(){
        return "我是角色管理员";
    }

}
