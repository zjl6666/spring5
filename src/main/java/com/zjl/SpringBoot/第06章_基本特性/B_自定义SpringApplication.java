package com.zjl.SpringBoot.第06章_基本特性;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.nio.charset.Charset;

/**
 * ClassName: B_自定义SpringApplication
 * Package: com.zjl.SpringBoot.第06章_基本特性
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/8/19 18:46
 * @Version 1.0
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class B_自定义SpringApplication {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","06");

        //SpringApplication.run(B_自定义SpringApplication.class, args);//Boot 应用的核心 API 入口

        //自定义方式运行(args);


        //流式 创建
        new SpringApplicationBuilder()
                //.main(B_自定义SpringApplication.class)//没啥用
                .sources(B_自定义SpringApplication.class)
                .bannerMode(Banner.Mode.CONSOLE)
                //.listeners(null)//监听器  ...
                .run(args);


        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");
    }

    private static void 自定义方式运行(String[] args) {
        SpringApplication springApplication = new SpringApplication(B_自定义SpringApplication.class);

        springApplication.setBannerMode(Banner.Mode.CONSOLE);//设置 图标  但是优先级低于配置文件

        springApplication.run(args);
    }
}
