package com.zjl.SpringBoot.第07章_核心部分;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 1.导入 starter
 * 2.依赖导入  autoconfigure
 * 3.寻找类路径下 META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports 文件
 *      这个动作称为 SPI 机制
 *      ●Java中的SPI (Service Provider Interface) 是一种软件设计模式，用于在应用程序中 动态地发现和加载组件。
 *      SPI的思想是，定义一个接口或抽象类,然后通过在classpath中义实现该接口的类来实现对组件的动态发现和加载。
 *      SPI的主要目的是解决在应用程序中使用可插拔组件的问题。例如，一个应用程序可能需要使用不同的日志
 *      框架或数据库连接池，但是这些组件的选择可能取决于运行时的条件。通过使用SPI,应用程序可以在运行
 *      时发现并加载适当的组件，而无需在代码中硬编码这些组件的实现类。
 *      ●在Java中，SPI的实现方式是通过在 META-INF/services 目录下创建一个以服务接口全限定名为名字的文件,
 *      文件中包含实现该服务接口的类的全限定名。
 *      当应用程序启动时, Java的SPI机制会自动扫描classpath中的这些些件，并根据文件中指定的类名来加载实现类。
 *      ●通过使用SPI,应用程序可以实现更灵活、可扩展的架构，同时也可以避免硬编码依赖关系和增加代码的可维护性。
 * 4.启动,加载所有自动配置类 xxxAutoConfiguration
 *          给容器中配置功能组件
 *          组件参数绑定到属性类中。  xxxProperties
 *          属性类和配置文件 前缀项绑定
 *          @Contional 派生的条件注解进行判断是否组件生效
 * 5.效果:
 *          修改配置文件,修改底层参数
 *          所有场景自动配置好直接使用
 *          可以注入SpringBoot配置好的组件随时使用
 */
@EnableAsync//开启异步  内部会利用 @Import 注解加载类
//@EnableScheduling//开启定时
public class D_自动配置原理 {
    public static void main(String[] args) {

    }
}
