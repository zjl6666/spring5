package com.zjl.SpringBoot.第07章_核心部分.service;


import com.zjl.SpringBoot.第07章_核心部分.event.LoginEvent;
import com.zjl.util.OutColour;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class SysService {

    @EventListener //注解事件  优先级低于 继承接口
    public void onEvent(LoginEvent event){
        OutColour.out.printlnPurple("事件",event,"登陆了==========");
    }

    public void login(String username){
        OutColour.out.printlnPurple(username,"登陆了==========");

    }
}
