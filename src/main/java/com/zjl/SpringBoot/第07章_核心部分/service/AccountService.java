package com.zjl.SpringBoot.第07章_核心部分.service;

import com.zjl.SpringBoot.第07章_核心部分.event.LoginEvent;
import com.zjl.util.OutColour;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;


@Service
public class AccountService implements ApplicationListener<LoginEvent> {//只关注指定事件

    public void addAccount(String username){
        OutColour.out.printlnPurple(username,"的积分+1");

    }

    @Override//事件
    public void onApplicationEvent(LoginEvent event) {
        OutColour.out.printlnPurple("AccountService 收到事件========== " + event,"的积分+1");
        event.getSource();//返回的是   LoginEvent时间的入参
    }
}