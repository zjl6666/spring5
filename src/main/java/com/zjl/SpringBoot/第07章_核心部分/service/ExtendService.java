package com.zjl.SpringBoot.第07章_核心部分.service;

import com.zjl.SpringBoot.第07章_核心部分.event.LoginEvent;
import com.zjl.util.OutColour;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class ExtendService {
    @Order(1)//设置优先级  数字越小优先级越高
    @EventListener //注解事件
    public void onEvent(LoginEvent event){
        OutColour.out.printlnPurple(event,"的到了发放的优惠卷,价格", new Random().nextInt(500),"元");
    }

    public void extend(String username){
        OutColour.out.printlnPurple(username,"到了，发放优惠卷,价格", new Random().nextInt(500),"元");

    }
}
