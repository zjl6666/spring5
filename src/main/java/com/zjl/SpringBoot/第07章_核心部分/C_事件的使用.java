package com.zjl.SpringBoot.第07章_核心部分;

import com.zjl.SpringBoot.第07章_核心部分.event.EventPublisher;
import com.zjl.SpringBoot.第07章_核心部分.event.LoginEvent;
import com.zjl.SpringBoot.第07章_核心部分.service.AccountService;
import com.zjl.SpringBoot.第07章_核心部分.service.ExtendService;
import com.zjl.SpringBoot.第07章_核心部分.service.SysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 事件的使用
 * 1.{@link EventPublisher} 创建添加事件的类
 * 2.{@link LoginEvent} 创建指定事件
 * 3.    1）{@link AccountService}      implements ApplicationEventPublisherAware
 *                  对指定事件添加事件逻辑
 *       2）@EventListener //注解事件
 *           public void onEvent(LoginEvent event){xxx}  //给 指定事件 添加事件逻辑
 *
 */
@RestController
public class C_事件的使用 {

    @Autowired
    AccountService accountService;
    @Autowired
    SysService sysService;
    @Autowired
    ExtendService extendService;

    /**
     * 设计模式，对新增开发，对修改关闭
     *  如果再加动作，就会一直加
     */
    @RequestMapping("old")// http://127.0.0.1:8080/old?username=张蛟龙&password=123456
    public String event(@RequestParam("username") String username,
                        @RequestParam("password") String password){

        accountService.addAccount(username);//计数
        sysService.login(username);//登陆
        extendService.extend(username);//发放

        return username + "登录成功";
    }

    @Autowired
    EventPublisher eventPublisher;

    @RequestMapping("new")// http://127.0.0.1:8080/new?username=张蛟龙&password=123456
    public String event2(@RequestParam("username") String username,
                        @RequestParam("password") String password){

        LoginEvent loginEvent = new LoginEvent(username);//创建事件
        eventPublisher.sendEvent(loginEvent);//放入事件并调用

        return username + "登录成功";
    }
}
