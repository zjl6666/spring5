package com.zjl.SpringBoot.第07章_核心部分.event;

import org.springframework.context.ApplicationEvent;

/**
 * 模拟 登录成功事件
 * 所有事件 都推荐继承 {@link ApplicationEvent}
 * 创建 事件
 */
public class LoginEvent extends ApplicationEvent {
    public LoginEvent(String source) {//代表谁登录了
        super(source);
    }
}
