package com.zjl.SpringBoot.第07章_核心部分.event;


import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

/**
 * 创建事件接收
 */
@Service
public class EventPublisher implements ApplicationEventPublisherAware {
    ApplicationEventPublisher applicationEventPublisher;

    //事件   所有时间都可以发送
    public void sendEvent(ApplicationEvent event) {
        //调用底层发送事件
        applicationEventPublisher.publishEvent(event);//放入事件
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;//把事件
    }
}
