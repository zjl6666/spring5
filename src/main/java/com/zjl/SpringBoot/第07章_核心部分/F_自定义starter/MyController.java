package com.zjl.SpringBoot.第07章_核心部分.F_自定义starter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    @Autowired
    MyService myService;

    @GetMapping("starter/hello")
    public String hello(){
        return myService.hello();
    }
}
