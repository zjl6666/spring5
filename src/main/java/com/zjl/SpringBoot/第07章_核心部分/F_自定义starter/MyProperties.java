package com.zjl.SpringBoot.第07章_核心部分.F_自定义starter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "my.properties")
@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
@Component
public class MyProperties {
    private String name;
    private String age;
    private String email;
}
