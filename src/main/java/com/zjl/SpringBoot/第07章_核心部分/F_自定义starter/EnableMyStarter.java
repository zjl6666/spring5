package com.zjl.SpringBoot.第07章_核心部分.F_自定义starter;

import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.DelegatingWebMvcConfiguration;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(MyAutoConfiguration.class)
public @interface  EnableMyStarter {

}
