package com.zjl.SpringBoot.第07章_核心部分.F_自定义starter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MyService {

    @Autowired
    MyProperties myProperties;


    public String hello() {
        return "你好，我是" + myProperties.getName() + "年龄为：" + myProperties.getAge() + "邮件为：" + myProperties.getEmail();
    }
}
