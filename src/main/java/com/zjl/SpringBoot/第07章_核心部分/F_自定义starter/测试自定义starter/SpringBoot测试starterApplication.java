package com.zjl.SpringBoot.第07章_核心部分.F_自定义starter.测试自定义starter;

import com.zjl.SpringBoot.第07章_核心部分.F_自定义starter.EnableMyStarter;
import com.zjl.SpringBoot.第07章_核心部分.F_自定义starter.MyAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.nio.charset.Charset;

/**
 * 1.自定义的 starter 不能存在 main函数，否则会有问题
 * 2.引入自己的 cloud-api-commons  需要clean  和  install 打包到maven中
 *      且引入方式，如当前项目    需要这样引入
 *      <groupId>com.zjl</groupId>
 *      <artifactId>Spring</artifactId>
 *      <version>0.0.1-SNAPSHOT</version>
 * 3.由于只会扫描 自己程序的包及其子包
 *      如果存在需要扫描的数据
 *      {@link MyAutoConfiguration}
 *       1).通过 Import 注解来引入 配置  @Import(MyAutoConfiguration.class)
 *       2).通过 自定义注解  @EnableMyStarter
 *       3).直接写一个文件 在 resources/META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports  文件
 *          就会自动导入
 *
 *  模拟 ：
 *      由于当前扫描路径不存在其他的包，即扫描不到外面的包,使用
 *      3)  可以实现运行直接使用
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
//@Import(MyAutoConfiguration.class)//导入 自定义 starter的自动配置类
//@EnableMyStarter
public class SpringBoot测试starterApplication {

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","07");
        SpringApplication.run(SpringBoot测试starterApplication.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");
    }

}

