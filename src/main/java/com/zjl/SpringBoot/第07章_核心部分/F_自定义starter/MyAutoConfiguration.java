package com.zjl.SpringBoot.第07章_核心部分.F_自定义starter;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 给入其中导入了  需要用的组件
 * 当作为组件时，
 */
@Import({MyController.class,MyProperties.class,MyService.class})
@Configuration
public class MyAutoConfiguration {

}
