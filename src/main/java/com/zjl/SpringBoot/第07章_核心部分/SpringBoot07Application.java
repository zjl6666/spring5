package com.zjl.SpringBoot.第07章_核心部分;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.nio.charset.Charset;


@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class SpringBoot07Application {

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","07");
        SpringApplication.run(SpringBoot07Application.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");
    }

}

