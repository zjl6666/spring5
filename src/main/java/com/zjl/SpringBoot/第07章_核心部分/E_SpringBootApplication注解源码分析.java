package com.zjl.SpringBoot.第07章_核心部分;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 复合注解 {@link SpringBootApplication}
 *         1. {@link SpringBootConfiguration}  就是 {@link Configuration}
 *         2. {@link EnableAutoConfiguration} //开启自动配置
 *                  {@link AutoConfigurationPackage} //扫描主程序包
 *                        Import ：{@link AutoConfigurationPackages.Registrar}
 *                        利用 Import 只做了一件事，把主程序中的包的所有组件都导入进去，
 *                        这就是Spring Boot默认只扫描主程序所在的包及其子包
 *                  {@link Import}        @Import(AutoConfigurationImportSelector.class)
 *                        {@link AutoConfigurationImportSelector} //加载所有自动配置类
 *                              即扫描 META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports
 *                              加载 starter 的类
 *
 *
 *         3. {@link ComponentScan}//组件扫描
 *                  排除一些组件，排除前面已经扫描进来的配置类和自动配置类
 *
 *
 *
 */
public class E_SpringBootApplication注解源码分析 {
    public static void main(String[] args) {

    }
}
