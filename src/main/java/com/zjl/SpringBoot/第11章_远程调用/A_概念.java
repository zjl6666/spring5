package com.zjl.SpringBoot.第11章_远程调用;

import com.zjl.SpringBoot.SpringBootMainApplication;
import io.netty.handler.codec.http.HttpResponse;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * 远程过程调用:
 *      ●服务提供者:
 *      ●服务消费者:
 *      ●通过连接对方服务器进行请求\响应交互，实现调用效果
 * API/SDK的区别是什么?
 * ● api:接口    远程提供功能
 * ● sdk:工具包   导入jar，直接调用功能即可
 *
 * 例如 阿里的 官网 ->云市场-> 就有相关的api
 * https://market.aliyun.com/
 *
 *
 * 开发过程中,我们经常需要调用别人写的功能
 *      ●如果是内部微服务，可以通过依赖cloud、 注册中心、openfeign等进行调用
 *      ●如果是外部暴露的，可以发送http请求、或遵循外部协议进行调用
 * SpringBoot整合提供了很多方式进行远程调用
 *      ●轻量级客户端方式
 *          RestTemplate: 普通开发
 *          WebClient:响应式编程开发
 *          Http Interface: 声明式编程
 *      ●Spring Cloud分布式解决方案方式
 *          Spring Cloud OpenFeign
 *      ●第三方框架
 *          Dubbo
 *          gRPC
 *
 *
 *
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class A_概念 {

    public static void main(String[] args) {
//        System.setProperty("spring.profiles.active","web");//  同理 --spring.profiles.active=xxx
        SpringApplication.run(A_概念.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");

    }
}
