package com.zjl.SpringBoot.第11章_远程调用;

import io.netty.handler.codec.http.HttpResponse;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.HashMap;
import java.util.Map;

/**
 * 1. WebClient
 * 非阻塞、响应式HTTP喀户端
 *
 * 发请求:
 *      ●请求方式: GET\POST\DELETEXxxxx
 *      ●请求路径: /xxx
 *      ●请求参数: aa=1&bb=2
 *      ●请求头: aa=001,bb=002
 *      ●请求体: xxx
 * 创建WebClient非常简单:
 *      ●WebClient.create()
 *      ●WebClient.create(String baseUrl)
 * 还可以使用WebClient.builder()配置更多参数项:
 *      ●uriBuilderFactory:自定义UriBuilderFactory，定义baseurl.
 *      ●defaultUriVariables:默认uri量.
 *      ●defaultHeader:每个请求默认头.
 *      ●defaultCookie:每个请求默认cookie.
 *      ●defaultRequest: Consumer自定义每个请求.
 *      ●filter:过滤client发送的每个请求
 *      ●exchangeStrategies: HTTP消息reader/writer自定义.
 *      ●clientConnector: HTTP client库设置.
 *
 *
 */
public class B1_WebClient {
    public static void main(String[] args) {


        WebClient webClient = WebClient.create();//访问路径
        String block = webClient
                .get()//get请求
//                .post()//post请求
                .uri("http://127.0.0.1:7777/employee/selectAll")//访问路径
                .accept(MediaType.APPLICATION_JSON)//定义返回类型
                .header("Accept", "application/json")//定义报文头
                .retrieve()
                .bodyToMono(String.class)
                .block();//阻塞 访问

        System.out.println(block);


    }
}
