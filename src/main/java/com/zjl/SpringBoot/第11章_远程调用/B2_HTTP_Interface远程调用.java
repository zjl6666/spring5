package com.zjl.SpringBoot.第11章_远程调用;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;
import reactor.core.publisher.Mono;

/**
 * Spring 6新功能
 *
 * Spring允许我们通过定义接口的方式，给任意位置发送http请求,实现远程调用，可以用来简化
 * HTTP远程访问。
 * 需要 webflux 场景可
 *
 */
public interface B2_HTTP_Interface远程调用 {

    @GetExchange(url = "/employee/select",accept = {"application/json"})
    Mono<String> search(@RequestParam("id") Long id);

    @GetExchange(url = "http://127.0.0.1:7777/employee/select",accept = {"application/json"})
    Mono<String> search2(@RequestParam("id") Long id);
}
