package com.zjl.SpringBoot.第02章_日志;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 归档：每天的日志单独存到一个文档中
 * 分割：每个文件指定大小（如10MB）,超过大小切割成另一个文件
 * 1.每天的日志应该独立分割出来存档。如果使用logback (SpringBoot 默认整合)，
 *      可以过 application.properties/yaml文件指定日志滚动规则。
 *      也可以用在 resources 下 放一个 logback.xml 的xml配置，也会按照次配置的执行
 *       如果名字为 logback-spring.xml  则spring会完全掌握 此xml
 * 2.如果是其他日志系统，需要自行配置(添加log4j2.xml或log4j2-spring.xml)
 * 3.支持的滚动规则设置如下
 *
 * logging.logback.rollingpolicy.file-name-pattern
 *      日志存档的文件名格式  默认  ${LOG_FILE}.%d{yyyy-MM-dd}.%i.gz
 * logging.logback.rollingpolicy.clean-history-on-start
 *      应用启动时是否清除以前存档(默认值: false)
 * logging.logback.rollingpolicy.max-file-size
 *      存档前，每个日志文件的最大大小(默认值: 10MB)
 * logging.logback.rollingpolicy.tota-size-cap
 *      #日志文件被删除之前，可以容纳的最大大小(默认值: 0B)    设置1GB则磁盘存储超过1GB日后     就会删除旧日志文件
 * logging.logback.rollingpolicy.max-history
 *      日文件保存的最大天数(默认值: 7)
 *
 * 通常我们配置application.properties 就够了。当然也可以自定义。
 * 比如:
 *      Logback
 *           logback-spring.xml
 *           logback-spring.groovy
 *           logback.xml
 *           logback.groovy
 *      Log4j2
 *           log4j2-spring.xml
 *           log4j2.xml
 *      JDK (Java Util Logging)
 *           logging.properties
 * 如果可能,我们建议您在日志配置中使用 -spring 量
 * (例如， logback-spring.xml 不是 logback.xml )。
 * 如果您使用标准配置文件，spring 无法完全控制日志初始化。
 *
 * ----------------------------------------------
 * 1.导入任何第三方框架,先排除它的日志包，因为Boot底层控制好了日志
 * 2.修改application. properties配置文件，就可以调整日志的所有行为。
 *      如果不够，可以编写志框架自己的配置文件放在类路径下就行，
 *      比如logback-spring.xml log4j2-spring.xml
 * 3.如需对接专业日志系统，也只需要把logback记录的日志灌倒kafka之类的中间件,这和SpringBoot
 * 没关系，都是日志框架自己的配置，修改配置文件即可
 * 4.业务中使用slf4j-api记录日志。不要再 sout 了
 *
 *
 */
@RestController
@Slf4j
public class A6_文件归档与滚动切割 {

    @RequestMapping("hello")
    public String hello(){
        log.trace("******************************************************我是 {} 日志******************************************************","TRACE");
        log.debug("******************************************************我是 {} 日志******************************************************","DEBUG");

        //默认级别 INFO
        log.info("******************************************************我是 {} 日志******************************************************","INFO");
        log.warn("******************************************************我是 {} 日志******************************************************","WARN");
        log.error("******************************************************我是 {} 日志******************************************************","ERROR");
        return "hello";
    }
}
