package com.zjl.SpringBoot.第02章_日志;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  时间                        日志级别 进程ID 消息分割符     线程名       通常是产生日志的类名                              日志记录的内容
 * 2024-08-05T20:06:27.076+08:00  INFO 10660 --- [           main] c.z.S.第02章_日志.SpringBoot02Application    : No active profile set, falling back to 1 default profile: "default"
 *
 * 默认输出格式:
 * ●时间和日期:亳秒级精度
 * ●日志级别: ERROR, WARN, INFO, DEBUG, or TRACE.
 * ●进程ID
 * ● --- 消息分割符
 * ●线程名:使用包含[]
 * ●Logger名:通常是产生日志的类名
 * ●消息:日志记录的内容
 *
 * 注意: logback 没有FATAL级别，对应的是ERROR
 *
 * 默认的格式都在：
 * org.springframework.boot:spring-boot:版本
 * 的包里 的 META-INF 里的 additional-spring-configuration-metadata.json 文件中
 *
 */
@Slf4j
public class A2_日志格式 {
    //  @Slf4j 会定义一个常量 log  不能重名1
    private static final Logger log1 = LoggerFactory.getLogger(A2_日志格式.class);

    public static void main(String[] args) {
        log.info("xxxxxxxxxxxxxxxxxx");
    }
}
