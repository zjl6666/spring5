package com.zjl.SpringBoot.第02章_日志;

/**
 * 调整默认级别 为 debug
 * logging.level.root=debug
 *
 * # logging.level.包名  可以指定包的日志级别         不能出现汉语  会识别不到
 * logging.level.com.zjl.SpringBoot.第02章_日志=warn
 *
 * #日志分组      组名         每个包用 逗号隔开来  可以用汉语
 * logging.group.组名=com.zjl.SpringBoot.第02章_日志,com.zjl.SpringBoot.第01章_基础
 * #给指定组指定日志级别
 * logging.level.组名=日志级别
 *
 * SpringBoot 预定了两个组
 * logging.group.web
 *      org.springframework.core.codec,
 *      org.springframework.http,
 *      org.springframework.web,
 *      org.springframework.boot.actuate.endpoint.web,
 *      org.springframework.boot.web.servlet.ServletContextInitializerBeans
 * logging.group.sql
 *      org.springframework.jdbc.core,
 *      org.hibernate.SQL,
 *      org.jooq.tools.LoggerListener
 *
 */
public class A4_日志分组 {

}
