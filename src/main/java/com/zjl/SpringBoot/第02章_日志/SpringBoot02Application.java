package com.zjl.SpringBoot.第02章_日志;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(
        excludeName = {//不加载次类  即数据库
                "org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration"
        }
)
public class SpringBoot02Application {
    private static final Logger log = LoggerFactory.getLogger(SpringBoot02Application.class);

    public static void main(String[] args) {

        System.setProperty("spring.profiles.active","log");//  同理 --spring.profiles.active=xxx
        SpringApplication.run(SpringBoot02Application.class, args);
        log.info("************程序成功启动******************");

    }

}

