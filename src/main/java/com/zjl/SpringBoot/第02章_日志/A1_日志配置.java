package com.zjl.SpringBoot.第02章_日志;

/**
 * 日志规范：
 * 项目不要编写 System.out.println();，应该用日志记录信息
 *
 * 日志门面
 *      JCL ( Jakarta Commons Logging )
 *      SLF4j ( Simple Logging Facade for Java )
 *      jboss logging
 * 日志实现
 *      Log4j  //
 *      JUL ( java.util.logging )
 *      Log4j2
 *      Logback
 *
 * 1. Spring使用commons-logging作为内部日志，但底层日志实现是开放的。可对接其他日志框架。
 *      spring5及以后commons-logging被spring直接自己写了。
 * 2.支持jul, log4j2,logback。 SpringBoot 提供了默认的控制台输出配置，也可以配置 输出为文件。
 * 3. logback是默认使用的。
 * 4.虽然日志框架很多，但是我们不用担心，使用SpringBoot的默认配置就能工作的很好。
 *
 *
 * SpringBoot怎么把旧志默认配置好的
 * 1、每个starter 场景,都会导入一个核心场景spring boot -starter
 * 2、核心场景引入了日志的所用功能spring- boot - starter- logging
 * 3、默认使用了logback + slf4j组合作为默认底层日志
 * 4、日志是系统一启动就要用，xxxAutoConfiguration 縣统启动好了以后放好的组件,后来用的。
 * 5、日志是利用监听器机制配置好的。ApplicationListener 。
 * 6、日志所有的配置都可以通过修改配置文件实现。以logging开始的所有配置。
 *
 *
 */
public class A1_日志配置 {
}
