package com.zjl.SpringBoot.第02章_日志;

/**
 *
 * #指定文件的路径  日志默认名字为 spring.log   优先级低于 logging.file.name
 * logging.file.path=D:/work/gitee/Spring/logs/
 * #这两者不会并存  优先  logging.file.name  存在 logging.file.name 的情况下 logging.file.path 不生效
 * #指定日志文件的名
 * logging.file.name=demo.log
 *
 * SpringBoot默认只把日志写在控制台,如果想额外记录到文件,可以在application.properties中添加
 * logging.file.name or logging. file. path配置项。
 *
 * logging.file.name        logging.file.path       示例      效果
 * 未指定                          未指定                      仅控制台输出
 * 指定                           未指定             my.log   写入指定文件。可以加路径
 * 未指定                          指定              /var/log   写入指定目录，文件名为spring.log
 * 指定                             指定                       logging.file.name为准
 *
 */
public class A5_日志文件输出 {

}
