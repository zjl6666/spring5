package com.zjl.SpringBoot.第02章_日志;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


/**
 * ●由低到高:
 * ALL, TRACE，DEBUG, INFO， WARN， ERROR, FATAL,OFF;
 * -----只会打印指定级别及以上级别的日志---------
 *      ALL:打印所有日志
 *      ------只能使用中间的----
 *      TRACE:追踪框架详细流程日志，一般不使用
 *      DEBUG:开发调试细节日志
 *      INFO:关键、感兴趣信息日志
 *      WARN:警告但不是错误的信息日志，比如:版本过时
 *      ERROR:业务错误日志，比如出现各种异常
 *      -----------
 *      FATAL: 致命错误日志，比如jvm系统崩溃
 *      OFF:关闭所有日志记录
 * ●不指定级别的所有类， 都使用root指定的级别作为默认级别
 * ●SpringBoot日志默认级别是INFO
 *
 * 调整默认级别 为 debug
 * logging.level.root=debug
 *
 * # logging.level.包名  可以指定包的日志级别         不能出现汉语  会识别不到
 * logging.level.com.zjl.SpringBoot.第02章_日志=warn
 *
 */
@Slf4j
@SpringBootApplication(
        excludeName = {//不加载次类  即数据库
                "org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration"
        }
)
public class A3_日志级别 {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","log");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(SpringBoot02Application.class, args);
        log.info("---------------------------------------------------");
        log.trace("我是 TRACE 日志");
        log.debug("我是 DEBUG 日志");

        //默认级别 INFO
        log.info("我是 INFO 日志");
        log.warn("我是 WARN 日志");
        log.error("我是 ERROR 日志");
    }
    @Bean
    public A3_日志级别 get(){
        log.info("-------------Bean--------------------------------------");
        log.trace("我是 {} 日志","TRACE");
        log.debug("我是 {} 日志","DEBUG");

        //默认级别 INFO
        log.info("我是 {} 日志","INFO");
        log.warn("我是 {} 日志","WARN");
        log.error("我是 {} 日志","ERROR");
        return new A3_日志级别();
    }
}
