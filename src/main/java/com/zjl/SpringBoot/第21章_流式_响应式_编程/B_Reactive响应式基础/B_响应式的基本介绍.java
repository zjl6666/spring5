package com.zjl.SpringBoot.第21章_流式_响应式_编程.B_Reactive响应式基础;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * API Components:
 * 1.Publisher:发布者;     产生数据流
 * 2.Subscriber:订阅者;    消费数据流
 *
 * 3.Subscription:        订阅关系;
 *      订阅关系是发布者和订阅者之间的关键接口。
 *      订阅者通过订阅来表示对发布者产生的数据的兴趣。
 *      订阅者可以请求一定数量的元素，也可以取消订阅。
 * 4.Processor:           处理器;
 *      处理器是同时实现了发布者和订阅者接口的组件。
 *      它可以接收来自一个发布者的数据，进行处理，并将结果发布给下一个订阅者。
 *      处理器在Reactor中充当中间环节，代表一个处理阶段,
 *      允许你在数据流中进行转换、过滤和其他操作。
 *
 * 这种模型遵循Reactive Streams规范，确保了异步流的一致性和可靠性。
 *
 *
 * 以前的编程模型在干什么?怎么编码?编码的时候注意什么问题?
 * //命令式编程：全自定义
 * //响应式编程/声明式编程：说清楚要干什么、最终结果是要怎样
 * <pre> {@code
 *      function a(String arg[){
 *           //业务处理
 *           //数据返回
 *      }
 * }</pre>
 * 方法调用:
 * 1、给他什么样的数据; 传参
 * 2、怎么处理数据:    业务
 * 3、处理后返回结果:   结果
 *
 * 哲学角度:
 * 万物皆是数据数据处理;
 *      数据结构+算法=程序
 *  一个数据/一堆数据(流) === 流操作 === 新数据/新流
 *
 */
public class B_响应式的基本介绍 {
    public static void main(String[] args) {

        /**
         * 数据是自流动的，而不是靠迭代被动流动;
         * 推拉模型:
         * 推:流模式;上游有数据，自动推给下游;
         * 拉:迭代器;自己遍历，自己拉取;
         */
        List<String> list = new ArrayList<>();

        for (String li:list){//迭代器
            //1.迭代速度取决于数据量
            //2.数据还得有容器缓存
        }

        //流
        List<String> collect = list.stream()
                .filter(a -> a != null)
                .collect(Collectors.toList());


//        Flow
    }
}
