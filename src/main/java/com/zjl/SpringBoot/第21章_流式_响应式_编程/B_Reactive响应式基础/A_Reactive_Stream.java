package com.zjl.SpringBoot.第21章_流式_响应式_编程.B_Reactive响应式基础;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 响应式流
 * Reactive Streams是JVM面向流的库的标准和规范
 * 1、处理可能无限数量的元素
 * 2、有序
 * 3、在组件之间异步传递元素
 * 4、强制性非阻塞背压模式
 *
 * 如 tomcat <img src="A_1.png" alt="示意图" width="900"/>
 **    当有10000个请求访问后台，
 *     tomcat 会有一个  acceptor 监听8080 端口，将每个请求数据放入缓冲区
 *     再由其他工作线程 顺序拿数据，进行处理返回、
 *     （其他工作线程 如果需要访问外系统
 *             与外系统还存在 缓冲区，工作线程继续干别的活，直到，外系统返回后放入缓冲区中）
 *
 * 响应式：
 *      目的:通过全异步的方式、加缓存区构建一个实时的数据流系统。如 Kafka、MQ能构建出大型分布式的响应式系统。
 *          缺本地化的消息系统解决方案:
 *          让所有的异步线程能互相监听消息，处理消息，构建实时消息处理流
 *      消息传递是响应式核心
 *      引入一个缓存区，引入消息队列， 就能实现全系统、全异步不咀塞、不等待、实时响应
 *
 */
public class A_Reactive_Stream {
    public static void main(String[] args) {

    }
}
