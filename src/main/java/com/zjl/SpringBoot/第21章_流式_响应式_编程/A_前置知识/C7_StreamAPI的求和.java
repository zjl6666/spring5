package com.zjl.SpringBoot.第21章_流式_响应式_编程.A_前置知识;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class C7_StreamAPI的求和 {

    @Test
    public  void Stream求和() {
        int[] arr = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println(Arrays.stream(arr).reduce(0/*初始值没有的话结果OptionalInt[45]*/, Integer::sum));//45
        System.out.println(List数组().stream().map(User::getAge).reduce(Integer::sum));//Optional[273]
    }

    private static List<User> List数组() {
        List<User> list = new ArrayList<>();
        list.add(new User(1001, "马化腾", 34));
        list.add(new User(1002, "马云", 12));
        list.add(new User(1003, "刘强东", 33));
        list.add(new User(1004, "雷军", 26));
        list.add(new User(1005, "李彦宏", 65));
        list.add(new User(1006, "比尔盖茨", 42));
        list.add(new User(1007, "任正非", 26));
        list.add(new User(1008, "扎克伯格", 35));
        return list;
    }
}
