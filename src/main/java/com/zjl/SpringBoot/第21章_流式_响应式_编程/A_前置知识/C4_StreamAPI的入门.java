package com.zjl.SpringBoot.第21章_流式_响应式_编程.A_前置知识;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;


/**
 * ●Java8中有两大最为重要的改变。
 * 第一个是Lambda表达式;另外一个则是Stream API。
 * ●Stream API (java.util.stream)把真正的函数式编程风格引入到Java中。这
 * 是目前为止对Java类库最好的补充，因为Stream API可以极大提供Java程
 * 序员的生产力，让程序员写出高效率、干净、简洁的代码。
 * ●Stream是Java8中处理集合的关键抽象概念，它可以指定你希望对集合进
 * 行的操作，可以执行非常复杂的查找、过滤和映射数据等操作。使用
 * ●Stream API对集合数据进行操作，就类似于使用SQL执行的数据库查询。
 * 也可以使用Stream API来并行执行操作。简言之，StreamAPl 提供了- -种
 * 高效且易于使用的处理数据的方式。
 *
 * Stream不会改变原来的元素
 *
 * ●1- 创建 Stream
 * 一个数据源(如:集合、数组)，获取一个流
 * ●2- 中间操作
 * 一个中间操作链，对数据源的数据进行处理
 * ●3- 终止操作(终端操作)
 * 一旦执行终止操作，就执行中间操作链，并产生结果。之后，不会再被使用
 */
public class C4_StreamAPI的入门 {

    @Test
    public void Stream的创建() {
        User u1 = new User(16, "a", 23);
        User u2 = new User(17, "b", 24);
        User u3 = new User(19, "c", 22);
        User u4 = new User(15, "d", 28);
        User u5 = new User(17, "e", 26);

        List<User> list = Arrays.asList(u1, u2, u3, u4, u5);

        //创建一个顺序流
        Stream<User> stream1 = list.stream();
        //创建一个并行流
        Stream<User> stream2 = list.parallelStream();

        //合并流
        Stream<User> concat = Stream.concat(stream1, stream2);
        //创建流
        Stream<Object> build = Stream.builder().add("1").add("2").build();


        int[] arr = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        IntStream stream3 = Arrays.stream(arr);//基本数据类型   包装成特殊的流

        double[] d = {1.2,1.6,1.9};
        DoubleStream stream = Arrays.stream(d);

        //会自动识别成引用数据类型
        Stream<Integer> Stream4 = Stream.of(1, 2, 3, 4, 5, 6);

        //iterate迭代                     每次加2   遍历前十个       循环     输出
        Stream.iterate(0/*起始值*/, t -> t + 2).limit(10).forEach(System.out::println);

        //  生成
        Stream.generate(Math::random).limit(10).forEach(System.out::println);//生成




    }  //流式接口


    private static List<User> List数组() {
        List<User> list = new ArrayList<>();
        list.add(new User(1001, "马化腾", 34));
        list.add(new User(1002, "马云", 12));
        list.add(new User(1003, "刘强东", 33));
        list.add(new User(1004, "雷军", 26));
        list.add(new User(1005, "李彦宏", 65));
        list.add(new User(1006, "比尔盖茨", 42));
        list.add(new User(1007, "任正非", 26));
        list.add(new User(1008, "扎克伯格", 35));
        return list;
    }

}