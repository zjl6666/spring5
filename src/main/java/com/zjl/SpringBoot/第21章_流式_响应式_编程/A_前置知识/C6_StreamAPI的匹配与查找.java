package com.zjl.SpringBoot.第21章_流式_响应式_编程.A_前置知识;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * ClassName: C6_StreamAPI的匹配与查找
 * Package: java学习.第18章_JDK8_17新特性
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2023/4/28 20:25
 * @Version 1.0
 */
public class C6_StreamAPI的匹配与查找 {

    @Test
    public void Stream的匹配与查找方法01() {
        List<User> list = List数组();
        System.out.println("***allMatch(Predicate p)——检查是否匹配所有元素。  boolean类型");
        System.out.println(list.stream().allMatch(e -> e.getAge() > 18));//false 是否全部  age>18
        System.out.println("***anyMatch(Predicate p)——检查是否至少匹配一个元素。 boolean类型");
        System.out.println(list.stream().anyMatch(e -> e.getAge() > 18));//true 是否有   age>18
        System.out.println("***noneMatch(Predicate p)——检查是否'没有'匹配的元素。boolean类型");
        System.out.println(list.stream().noneMatch(e -> e.getAge() > 66));//false
        System.out.println("***findFirst******返回第一个元素");
        Optional<User> first = list.stream().findFirst();//Optional避免空指针造出的类
        System.out.println(first);
        System.out.println("***findAny——返回当前流中的任意元素  随机的");
        Optional<User> any = list.stream().findAny();//串行流  总是第一个
        Optional<User> any1 = list.parallelStream().findAny();//并行流  总是某一个(第六个)
        Stream<User> stream = any1.stream();//JDK9新特性  可以将 Optional-》Stream
        System.out.println("***串行流 总是第一个\t\t\t" + any);
        System.out.println("***并行流 总是某一个(第六个)\t" + any1);
//        System.out.println("*****parallelStream*并行流***自带多线程**用线程池**哪个线程抢到哪个运算*****");
//        list.parallelStream().forEach(num->System.out.println(Thread.currentThread().getName()+">>"+num));
        System.out.println("***count——返回流中元素的总个数  是个long类型");
        System.out.println(list.stream().count());
        System.out.println("***max(Comparator c)——返回流中最大值");
        System.out.println(list.stream().max((e, f) -> Integer.compare(e.getAge(), f.getAge())));
        //Optional[User(id=1005, username=李彦宏, age=65)]
        System.out.println("***min(Comparator c)——返回流中最小值同上");
        System.out.println(list.stream().map(e -> e.getAge()).min(Integer::compareTo));//Optional[12]
        System.out.println("***forEach(Consumer c)——内部迭代");
        list.stream().forEach(System.out::println);
    }
    private static List<User> List数组() {
        List<User> list = new ArrayList<>();
        list.add(new User(1001, "马化腾", 34));
        list.add(new User(1002, "马云", 12));
        list.add(new User(1003, "刘强东", 33));
        list.add(new User(1004, "雷军", 26));
        list.add(new User(1005, "李彦宏", 65));
        list.add(new User(1006, "比尔盖茨", 42));
        list.add(new User(1007, "任正非", 26));
        list.add(new User(1008, "扎克伯格", 35));
        return list;
    }
}
