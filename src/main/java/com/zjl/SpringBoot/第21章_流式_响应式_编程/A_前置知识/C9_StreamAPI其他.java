package com.zjl.SpringBoot.第21章_流式_响应式_编程.A_前置知识;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * ClassName: C9_StreamAPI其他
 * Package: java学习.第18章_JDK8_17新特性
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2023/4/29 13:19
 * @Version 1.0
 */
public class C9_StreamAPI其他 {
    @Test
    public void Stream的map和flatMap的区别() {
        System.out.println("****map*和*flatMap****区别********* ");
        List<String> list = Arrays.asList("aa", "bb", "cc", "dd");
        Stream<Stream<Character>> streamStream = list.stream().map(C9_StreamAPI其他::StringToCharArray);//正常执行
        streamStream.forEach(s -> s.forEach(System.out::print));//看返回值就知道

        System.out.println();
        Stream<Character> characterStream = list.stream().flatMap(C9_StreamAPI其他::StringToCharArray);//执行后 数组会自动拆分
        characterStream.forEach(System.out::print);//直接可以输出
    }

    private static Stream<Character> StringToCharArray(String str) {
        List<Character> list = new ArrayList<>();
        for (Character c : str.toCharArray()) {
            list.add(c);
        }
        return list.stream();
    }
    @Test
    public void Stream的并行流() {
        System.out.println("****map*和*flatMap****区别********* ");
        List<String> list = Arrays.asList("aa", "bb", "cc", "dd");
        list.stream().forEach(System.out::println);//顺序流
        System.out.println("-------------------------");
        list.stream().parallel().forEach(System.out::println);//并行流（多线程）
    }


}
