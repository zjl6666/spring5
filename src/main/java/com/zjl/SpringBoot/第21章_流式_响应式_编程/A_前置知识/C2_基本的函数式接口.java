package com.zjl.SpringBoot.第21章_流式_响应式_编程.A_前置知识;

import org.junit.Test;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class C2_基本的函数式接口 {

    @Test
    public void InterfaceDemo() {
        //函数型接口  有输入有输出
        Function<String,Integer> function=s->{return  s.length();};
        Function<String,Integer> function1=String::length; //方法引用的函数型接口
        System.out.println(function.apply("abc"));

        //bool 型接口 输入  返回 boolean 类型
        Predicate<String> predicate=s->{return s.isEmpty();};
        Predicate<String> predicate1=String::isEmpty; //方法引用的bool型接口
        System.out.println(predicate.test("hh"));

        //消费性接口
        Consumer<String> consumer=s->{System.out.println(s);};
        Consumer<String> consumer1= System.out::println; //方法引用的bool消费性接口
        consumer.accept("傻逼");

        //供给型接口 只有输出
        Supplier<String> supplier=()->{return "你猜";};
        supplier.get();
        Supplier<String> supplier1=()-> "你猜";//供给型接口


    }  //四大函数式接口  juf

}
