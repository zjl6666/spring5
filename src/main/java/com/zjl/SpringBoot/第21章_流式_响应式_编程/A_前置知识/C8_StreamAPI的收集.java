package com.zjl.SpringBoot.第21章_流式_响应式_编程.A_前置知识;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ClassName: C8_StreamAPI的收集
 * Package: java学习.第18章_JDK8_17新特性
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2023/4/29 13:11
 * @Version 1.0
 */
public class C8_StreamAPI的收集 {
    @Test
    public  void Stream筛选() {

        List<User> list = List数组();//获取数组
        Stream<User> stream = list.stream();//获取流
        //
        List<User> collect = stream.
                filter(e -> e.getAge() > 20).//筛选出年龄大于20的
                        collect(Collectors.toList());//收集 返成List
        //Stream<User>  把这种类型转换成Collectors.toList()指定的类型
        collect.forEach(System.out::println);
    }
    private static List<User> List数组() {
        List<User> list = new ArrayList<>();
        list.add(new User(1001, "马化腾", 34));
        list.add(new User(1002, "马云", 12));
        list.add(new User(1003, "刘强东", 33));
        list.add(new User(1004, "雷军", 26));
        list.add(new User(1005, "李彦宏", 65));
        list.add(new User(1006, "比尔盖茨", 42));
        list.add(new User(1007, "任正非", 26));
        list.add(new User(1008, "扎克伯格", 35));

        return list;
    }
}
