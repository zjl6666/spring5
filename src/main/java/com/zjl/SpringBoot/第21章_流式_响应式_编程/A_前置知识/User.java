package com.zjl.SpringBoot.第21章_流式_响应式_编程.A_前置知识;

import java.util.Objects;

public class User {
    private Integer id;
    private String username;
    private int age;

    public User() {
    }

    public User(Integer id, String username, int age) {
        this.id = id;
        this.username = username;
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", age=" + age +
                '}';
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {

//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        User user = (User) o;
//        return age == user.age && Objects.equals(id, user.id) && Objects.equals(username, user.username);

        //JDK 14-16 的 instanceof 新特性      o instanceof User user
        return o instanceof User user &&
                user.id.equals(this.id) &&
                user.username.equals(this.username) &&
                user.age == this.age;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, age);
    }
}
