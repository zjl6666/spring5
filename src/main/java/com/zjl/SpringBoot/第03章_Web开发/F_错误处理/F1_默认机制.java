package com.zjl.SpringBoot.第03章_Web开发.F_错误处理;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;

import java.nio.charset.Charset;

/**
 * 错误的自动配置
 * {@link ErrorMvcAutoConfiguration}
 * SpringBoot会自适应处理错误，根据内容协商响应页面或JSON数据
 * SpringMVC的错误处理机制依然保留，MVC处理不了才会交给boot进行处理
 *
 * 异常的视图解析器  会默认返回白页和json
 *
 * 否则返回自己指定的
 * 默认会再路径前加/error 解释了为啥放在 error 包下 会自动解析
 * 会根据状态码为视图页地址 找页面
 * 4xx  x:表示任意一个
 *
 * http://127.0.0.1:8082/erroradw
 * 这是没找到路径的默认错误:
 * {
 *     "timestamp": "2024-08-18T05:55:06.847+00:00",
 *     "status": 404,
 *     "error": "Not Found",
 *     "trace": "org.springframework.web.servlet.resource.NoResourceFoundException: XXX",
 *     "message": "No static resource erroradw.",
 *     "path": "/erroradw"
 * }
 * --------------------------------------------------------
 * 每个 spring boot 都会有一个默认的 错误路径  http://127.0.0.1:8082/error
 * 直接访问：
 * {
 *     "timestamp": "2024-08-18T05:56:32.390+00:00",
 *     "status": 999,
 *     "error": "None",
 *     "message": "No message available"
 * }
 *
 *
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class F1_默认机制 {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","thymeleaf");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(F1_默认机制.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");

    }
}
