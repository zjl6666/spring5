package com.zjl.SpringBoot.第03章_Web开发;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 * 1.整合 web 场景
 *     <dependency>
 *         <groupId>org.springframework.boot</groupId>
 *         <artifactId>spring-boot-starter-web</artifactId>
 *     </dependency>
 * 2.引入  autoconfigure  自动配置
 * 3. @EnableAutoConfiguration 注解使用了 @Import(AutoConfigurationImportSelector.class) 批量导入组件
 * 4.加载
 * @see org.springframework.boot.autoconfigure.AutoConfigurationImportSelector 的 getCandidateConfigurations 方法
 * 使用了  ImportCandidates.load(AutoConfiguration.class, getBeanClassLoader())
 * @see org.springframework.boot.context.annotation.ImportCandidates
 * 中使用了路径 META-INF/spring/%s.imports   是通过这个文件加载的
 * 在文件中的 类中 @EnableConfigurationProperties 这个注解就是加载配置文件中的值
 * 其中
 * @see org.springframework.boot.autoconfigure.web.reactive 是响应式编程的自动配置
 * 5.绑定了配置文件的一堆配置项  如：
 *      ●1、SpringMVC的所有配置 spring.mvc
 *      ●2、Web场景通用配置 spring.web
 *      ●3、文件上传配置 spring.servlet.multipart
 *      ●4、服务器的配置 server :比如:编码方式
 *
 *
 *
 */
@EnableAutoConfiguration
public class A1_自动配置 {
}
