package com.zjl.SpringBoot.第03章_Web开发.E_内容协商;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * ClassName: E1_多端内容适配
 * Package: com.zjl.SpringBoot.第03章_Web开发.E_内容协商
 * Description:
 *  自定义报文头  自定义返回值格式
 * @Author 张蛟龙
 * @Create 2024/8/13 18:58
 * @Version 1.0
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class E0_内容协商 {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","web");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(E0_内容协商.class, args);

    }
}
