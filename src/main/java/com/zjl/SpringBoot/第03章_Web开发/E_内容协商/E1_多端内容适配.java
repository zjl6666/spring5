package com.zjl.SpringBoot.第03章_Web开发.E_内容协商;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * SpringBoot 多端内容适配。
 *      基于请求头内容协商:
 *          客户端向服务端发送请求,携带HTTP标准的Accept请求头。（默认开启）
 *               Accept: application/json 、text/xml 、text/yaml
 *               服务端根据客户端请求头期望的数据类型进行动态返回
 *          基于请求参数内容协商:（需要开启    ）
 *              发送请求GET /projects/spring-boot?format=json
 *              匹配到 @GetMapping("/projects/spring-boot")
 *              根据参数协商，优先返回json类型数据[需要开启参数匹配设置]
 *              发送请求GET /projects/spring-boot?format=xml,优先返回xml类型数据
 *
 * 默认支持 把对象写为 JSON     jackson 配置
 *
 *
 # ip:端口?myparam=json     myparam的值为为啥，返回就是啥，要支持
 #使用参数进行内容协商
 spring.mvc.contentnegotiation.favor-parameter=true
 #自定义参数名  默认为 format
 spring.mvc.contentnegotiation.parameter-name=myparam
 *
 */
@Controller
@ResponseBody
public class E1_多端内容适配 {

    //http://127.0.0.1:8080/dd
    @GetMapping("/dd")//默认 返回 JSON
    public Object hello(){

        Map<String,Object> map = new HashMap<>();
        map.put("id",1);
        map.put("name","张三");
        map.put("age",66);
        return map;
    }
    //http://127.0.0.1:8080/person
    @GetMapping("/person")//默认 返回 JSON
    public Person person(){
        Person person = new Person();
        person.setAge(15);
        person.setUserName("李四");
        person.setBoss(true);
        person.setBirth(LocalDateTime.now());
        return person;
    }
}
