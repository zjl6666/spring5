package com.zjl.SpringBoot.第03章_Web开发.E_内容协商;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
//@JacksonXmlRootElement //可以写出 xml文档  不写也无所谓
public class Person {
    private String userName;
    private Boolean boss;
    private LocalDateTime birth;
    private Integer age;
}
