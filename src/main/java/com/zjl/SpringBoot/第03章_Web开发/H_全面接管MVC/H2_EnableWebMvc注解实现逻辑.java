package com.zjl.SpringBoot.第03章_Web开发.H_全面接管MVC;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.web.servlet.config.annotation.DelegatingWebMvcConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 *
 * {@link EnableWebMvc}  禁用所有 web的自动配置 实现逻辑
 *  @Import(DelegatingWebMvcConfiguration.class)
 *  导入了 {@link DelegatingWebMvcConfiguration} 这个类
 *          继承了 {@link WebMvcConfigurationSupport}
 *
 *  其中 {@link WebMvcAutoConfiguration}
 *  @ConditionalOnMissingBean(WebMvcConfigurationSupport.class)
 *  中 在没有 {@link WebMvcConfigurationSupport} 这个类才生效
 *      而
 * @EnableWebMvc  会导入 {@link WebMvcConfigurationSupport} 组将，所以不生效
 *
 *
 */
public class H2_EnableWebMvc注解实现逻辑 {

}
