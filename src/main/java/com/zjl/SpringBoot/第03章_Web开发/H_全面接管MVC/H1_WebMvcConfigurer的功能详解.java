package com.zjl.SpringBoot.第03章_Web开发.H_全面接管MVC;

import com.zjl.SpringBoot.第03章_Web开发.B_web实现逻辑;
import com.zjl.SpringBoot.第03章_Web开发.E_内容协商.MyConfig;
import org.springframework.boot.autoconfigure.task.TaskExecutionAutoConfiguration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.format.Formatter;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.lang.Nullable;
import org.springframework.validation.MessageCodesResolver;
import org.springframework.validation.Validator;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.mvc.annotation.ResponseStatusExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;
import org.springframework.web.servlet.view.ViewResolverComposite;

import java.util.List;

/**
 * {@link B_web实现逻辑}
 *
 * WebMvcConfigurer功能【了解】
 * WebMvcConfigurer定义和扩展SpringMVC底层功能
 * 
 * @EnableWebMvc禁用默认行为
 * @EnableWebMvc给容器中导入 DelegatingWebMvcConfiguration组件，该组件继承于WebMvcConfigurationSupport
 *
 * WebMvcAutoConfiguration有一个核心的条件注解，@ConditionalOnMissingBean(WebMvcConfigurationSupport.class)，容器中没有WebMvcConfigurationSupport，WebMvcAutoConfiguration才生效
 *
 * @EnableWebMvc 导入 WebMvcConfigurationSupport 导致 WebMvcAutoConfiguration 失效。导致禁用了默认行为
 *
 * 总结
 * @EnableWebMVC 禁用了Mvc的自动配置
 * WebMvcConfigurer定义SpringMVC底层组件的功能类
 */
public class H1_WebMvcConfigurer的功能详解 implements WebMvcConfigurer {

    /**
     * 格式化器：支持属性上 @NumberFormat 和 @DatetimeForma t的数据类型转换
     * {@link GenericConversionService}
     *
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {}
    /**
     * 数据校验：校验 Controller 上使用 @Valid 标注的参数合法性。需要导入starter-validator
     *
     */
    @Override
    public Validator getValidator() {
        return null;
    }

    /**
     * 拦截器：拦截收到的所有请求
     * 模拟拦截器的测试
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new 防登录拦截器())//添加拦截规则

                //需要拦截哪些 /**是所有请求都拦截  "包括静态"
                .addPathPatterns("/table","/main.html","/main","/filesc","/userLongs","/logins")
                .excludePathPatterns("/","/login","");//需要放行哪些

    }
    /**
     * 内容协商：支持多种数据格式返回。需要配合支持这种类型的 {@link HttpMessageConverter}
     * 支持 json
     */
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {}
    /**
     * 消息转换器：标注 @ResponseBody 的返回值会利用 {@link HttpMessageConverter} 直接写出去
     *      {@link com.zjl.SpringBoot.第03章_Web开发.E_内容协商.E2_自定义内容协商}
     * 8 个，支持 byte，string,multipart,resource，json
     * 案例{@link com.zjl.SpringBoot.第03章_Web开发.E_内容协商.MyConfig}
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {}
    /**
     * 视图映射：直接将请求路径与物理视图映射。用于无 java 业务逻辑的直接视图页渲染
     * 无 mvc:view-controller
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {}

    /**
     *视图解析器：逻辑视图转为物理视图
     * 默认 {@link ViewResolverComposite}
     */
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {}
    /**
     * 静态资源处理：静态资源路径映射、缓存控制
     *
     * 默认 {@link ResourceHandlerRegistry}
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {}
    /**
     * 默认 Servlet：可以覆盖 Tomcat 的DefaultServlet。让DispatcherServlet拦截/
     *
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {}
    /**
     * 路径匹配：自定义 URL 路径匹配。可以自动为所有路径加上指定前缀，比如 /api
     *
     */
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {}
    /**
     * 异步支持：
     * 默认 {@link TaskExecutionAutoConfiguration}
     *
     */
    @Override
    public void configureAsyncSupport(AsyncSupportConfigurer configurer) {}
    /**
     * 跨域：
     *
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {}
    /**
     *参数解析器：
     *mvc 默认提供
     */
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {}
    /**
     *
     * 返回值解析器：	 mvc 默认提供
     */
    @Override
    public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> handlers) {}

    /**
     * 异常处理器：
     * 默认 3 个
     * {@link ExceptionHandlerExceptionResolver}
     * {@link ResponseStatusExceptionResolver}
     * {@link DefaultHandlerExceptionResolver}
     */
    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> resolvers) {}
    /**
     * 消息码解析器：国际化使用
     * @return
     */
    @Override
    public MessageCodesResolver getMessageCodesResolver() {
        return null;
    }
    /**
     * 扩展：消息转换
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {}

    /**
     *  扩展：异常解析
     */
    @Override
    public void extendHandlerExceptionResolvers(List<HandlerExceptionResolver> resolvers) {}



}