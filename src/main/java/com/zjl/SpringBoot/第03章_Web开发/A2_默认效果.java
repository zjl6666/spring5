package com.zjl.SpringBoot.第03章_Web开发;

import com.zjl.SpringBoot.SpringBootMainApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 默认配置:
 * 1.包含了 ContentNegotiatingViewResolver 和 BeanNameViewResolver 组件,方便视图解析
 * 2.默认的静态资源处理机制: 静态资源放在 static 文件夹下即可直接访问
 * 3.自动注册了 Converter,GenericConverter,Formatter 组件,适配常见的 数据类型转换和格式化 需求。
 * 4.支持 HttpMessageConverters,可以方便返回json等数据类型
 * 5.注册 MessageCodesResolver,方便国际化及错误消息处理
 * 6.支持静态 index.html
 * 7.自动使用 ConfigurableWebBindingInitializer, 实现消息处理、数据绑定、类型转化、数据校验等功能
 * ********************************************************************************
 * ●如果想保持 boot mvc 的默认配置,并且自定义更多的mvc配置,
 *      如: interceptors, formatters,view controllers等。
 *   可以使用 @Configuration 注解添加一个 WebMvcConfigurer 类型的配置类,
 *   并不要标注 @EnableWebMvc
 * ●如果想保持boot mvc的默认配置，但要自定义核心组件实例，比如:
 *  RequestMappingHandlerMapping, RequestMappingHandlerAdapter或
 *  ExceptionHandlerExceptionResolver,给容器中放一个WebMvcRegistrations组件即可
 * ●如果想全面接管Spring MVC, @Configuration标注一 个配置类,
 *  并加上@EnableWebMvc注解,实现 WebMvcConfigurer 接口
 *
 */
@EnableWebMvc //禁用所有 web的自动配置
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class A2_默认效果 {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","web");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(A2_默认效果.class, args);

    }
}
