package com.zjl.SpringBoot.第03章_Web开发;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcRegistrations;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *| ----------------------------------------------------------------------------------------------
 *| 全自动      |      直接编写控制器逻辑         |                        |     全部使用自动配置默认效果  |
 *|-----------------------------------------------------------------------------------------------|
 *|            |       @Configuration +       |                       |          自动配置效果        |
 *| 手自一体    |     配置 WebMvcConfigurer     |   不标注 @EnableWebMvc  |         手动设置部分功能     |
 *|            |     配置 WebMvcRegistrations  |                       |         定义MVC底层组件      |
 *|------------------------------------------------------------------------------------------------|
 *|            |                              |                       |          禁用自动配置效果     |
 *| 全手动      |      @Configuration +        |   标注 @EnableWebMvc   |          全手动设置          |
 *|            |       配置 WebMvcConfigurer   |                       |                            |
 *|----------------------------------------------------------------------------------------------------
 *
 *
 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurer
 * @see org.springframework.boot.autoconfigure.web.servlet.WebMvcRegistrations
 *
 *
 *
 *
 */
@EnableWebMvc //禁用所有 web的自动配置
@Configuration
public class Z_最佳实战 {
    public static void main(String[] args) {

    }
}
