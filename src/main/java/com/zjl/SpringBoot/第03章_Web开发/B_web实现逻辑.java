package com.zjl.SpringBoot.第03章_Web开发;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.web.servlet.filter.OrderedHiddenHttpMethodFilter;
import org.springframework.boot.web.servlet.filter.OrderedFormContentFilter;
import org.springframework.web.filter.FormContentFilter;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 1.生效条件
 * WebMvcAutoConfiguration 原理  详看：
 * @see WebMvcAutoConfiguration
 *
 * @AutoConfiguration(after = { DispatcherServletAutoConfiguration.class, TaskExecutionAutoConfiguration.class,
 * 		ValidationAutoConfiguration.class })  在这些配置生效后生效
 * @ConditionalOnWebApplication(type = Type.SERVLET)  如果是web应用就生效
 * @ConditionalOnClass({ Servlet.class, DispatcherServlet.class, WebMvcConfigurer.class })
 * @ConditionalOnMissingBean(WebMvcConfigurationSupport.class)//容器没有这个bing 才生效
 * @AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE + 10)//优先级
 * @ImportRuntimeHints(WebResourcesRuntimeHints.class)
 *
 * 2.效果
 *  在 WebMvcAutoConfiguration 中注入了
 *      ①
 *      @see OrderedHiddenHttpMethodFilter  他又继承了
 *      @see HiddenHttpMethodFilter 这个方法实现了，页面提交Rest 请求（GET、POST、PUT、DELETE）
 *      ②
 *      @see OrderedFormContentFilter  他又继承了
 *      @see FormContentFilter   表单内容Filter ，GET(数据放URL后面)、 POST(数据放请求体)请求可以携带数据、PUT、DELETE（请求体数据会被忽略）
 *      ③ 静态内部类
 *      @see WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter 继承了
 *      @see WebMvcConfigurer 这个就是web 的接口，提供了SpringMVC 底层所有的组件入口
 *          configurePathMatch                      路径匹配
 *          configureContentNegotiation             内容协商
 *          configureAsyncSupport                   异步支持
 *          configureDefaultServletHandling         默认处理  默认接受  /
 *          addFormatters                           格式化器
 *          addInterceptors                         拦截器
 *          addResourceHandlers                     资源处理器  -处理静态资源规则
 *          addCorsMappings                         跨域
 *          addViewControllers                      视图控制器： /a 直接跳转 XXX.html 页面跳转
 *          configureViewResolvers                  视图解析
 *          addArgumentResolvers                    参数解析器
 *          addReturnValueHandlers                  返回值处理器
 *          configureMessageConverters              消息转换器
 *          extendMessageConverters                 扩展：消息转换
 *          configureHandlerExceptionResolvers      配置异常解析器
 *          extendHandlerExceptionResolvers         扩展：异常解析
 *          getValidator
 *          getMessageCodesResolver
 *
 */
public class B_web实现逻辑 {
    public static void main(String[] args) {

    }
}
