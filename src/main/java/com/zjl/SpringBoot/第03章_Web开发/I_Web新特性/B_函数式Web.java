package com.zjl.SpringBoot.第03章_Web开发.I_Web新特性;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.servlet.function.*;

import java.nio.charset.Charset;
import java.util.HashMap;

/**
 * 基础知识
 * SpringMVC 5.2 以后允许使用函数式的方式，定义Web的请求处理流程【函数式接口】
 * Web请求处理的方式
 * @Controller + @RequestMapping：耦合式 （路由、业务耦合）
 * 函数式Web：分离式（路由、业务分离）
 * 核心类
 *
 * RouterFunction：定义路由信息
 * RequestPredicate：定义请求规则，即请求方式、接收的请求参数等
 * ServerRequest：封装请求数据
 * ServerResponse：封装响应数据
 *
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class B_函数式Web {
    public static void main(String[] args) {
//        System.setProperty("spring.profiles.active","tomcat");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(B_函数式Web.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");
    }
}
@Configuration
class MyRoutingConfiguration {
    //定义请求的数据类型
    private static final RequestPredicate ACCEPT_JSON = RequestPredicates.accept(MediaType.APPLICATION_JSON);
    @Bean
    public RouterFunction<ServerResponse> routerFunction(MyHandlerFunctions myHandlerFunctions/*自动注入业务处理器*/) {
        //表示  入参必须是                           JSON
        RequestPredicates.accept(MediaType.APPLICATION_JSON).
                and(RequestPredicates.param("aa","bb"));//   存在参数 aa且值为 bb
        return RouterFunctions.route()
                //请求路径	  接收参数类型   交给谁处理，用什么方法处理【每个业务都准备自己的handler】
                .GET("/{user}", ACCEPT_JSON, myHandlerFunctions::getUser)
                .POST("/{user}/customers", ACCEPT_JSON, myHandlerFunctions::getUserCustomers)
                .DELETE("/{user}", ACCEPT_JSON, myHandlerFunctions::deleteUser)
                .PUT("/{user}", ACCEPT_JSON, myHandlerFunctions)
                .build();//构建出上述规则的RouterFunction
    }
}
@Component
class MyHandlerFunctions implements HandlerFunction<ServerResponse>{
    //     响应体                       请求体
    public ServerResponse getUser(ServerRequest serverRequest) {

        return ServerResponse.ok()
                .body(new HashMap<>(){{
                    put("xxx","xxx");
                    putAll(serverRequest.pathVariables());//获取所有路径变量
                    putAll(serverRequest.params());//获取所有 ？ 后
                }});
    }

    public ServerResponse getUserCustomers(ServerRequest serverRequest) {
        return null;
    }

    public ServerResponse deleteUser(ServerRequest serverRequest) {
        return null;
    }

    @Override
    public ServerResponse handle(ServerRequest request) throws Exception {
        return null;
    }
}
