package com.zjl.SpringBoot.第03章_Web开发.I_Web新特性;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * ProblemDetailsExceptionHandler 是一个 @ControllerAdvice，用于集中处理系统异常
 * {@link WebMvcAutoConfiguration.ProblemDetailsErrorHandlingConfiguration  }
 *  {@link org.springframework.boot.autoconfigure.web.servlet.ProblemDetailsExceptionHandler} // 有  @ControllerAdvice 处理异常
 * 继承了 {@link ResponseEntityExceptionHandler} 发现他可以处理一些指定的异常
 *  集中    处理一些对错误数据的处理和数据的返回  以 【RFC 9457】 规则返回 //https://www.rfc-editor.org/rfc/rfc7807
 *
 * 只有  spring.mvc.problemdetails.enabled=true  才会启用
 *
 * 启用前 错误信息
 *  返回报文头  Content-Type=application/json
 *  {
 *      "timestamp": "2024-08-18T05:55:06.847+00:00",
 *      "status": 404,
 *      "error": "Not Found",
 *      "trace": "org.springframework.web.servlet.resource.NoResourceFoundException: XXX",
 *      "message": "No static resource erroradw.",
 *      "path": "/erroradw"
 *  }
 * 启用后
 *  返回报文头  Content-Type=application/problem+json
 *  {
 *      "type": "about:blank",
 *      "title": "Not Found",
 *      "status": 404,
 *      "detail": "No static resource xxx.",
 *      "instance": "/xxx"
 *  }
 *
 *
 */
public class A_Web异常处理Problemdetails {
    public static void main(String[] args) {

    }
}
