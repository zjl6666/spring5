package com.zjl.SpringBoot.第03章_Web开发.G_嵌入式容器;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.embedded.EmbeddedWebServerFactoryCustomizerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.support.AbstractApplicationContext;

import java.nio.charset.Charset;

/**
 * Servlet容器：管理、运行Servlet组件（Servlet、Filter、Listener）的环境，一般指服务器
 *
 *
 * 自动配置原理
 * SpringBoot默认嵌入Tomcat作为Servlet容器
 * 根据自动配置类分析功能，嵌入式容器的自动配置类是
 * {@link ServletWebServerFactoryAutoConfiguration}
 *      Tomcat、Jetty、Undertow 都有条件注解，系统中有这个类才会生效（也就是导了包）
 *      springboot给容器中放了TomcatServletWebServerFactory，默认 Tomcat配置生效
 * 其中 {@link ServerProperties}  就是配置信息
 *   @Import 注解导入了 3大  服务器
 *      ServletWebServerFactoryConfiguration.EmbeddedTomcat.class,
 *              {@link org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryConfiguration.EmbeddedTomcat}
 *      ServletWebServerFactoryConfiguration.EmbeddedJetty.class,
 *              {@link org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryConfiguration.EmbeddedJetty}
 *      ServletWebServerFactoryConfiguration.EmbeddedUndertow.class
 *              {@link org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryConfiguration.EmbeddedUndertow}
 *
 *  都继承了  {@link ServletWebServerFactory}   web服务器工厂
 * {@link ServletWebServerApplicationContext} IOC容器 启动时 createWebServer 方法调用了 web服务器工厂 获取服务器
 *      而  createWebServer 是 onRefresh() 刷新时启动，也就是启动
 * 启动都是使用了 {@link AbstractApplicationContext}  的 refresh()
 *
 *
 *
 *
 * {@link EmbeddedWebServerFactoryCustomizerAutoConfiguration}
 *
 *
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class G0_自动配置 {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","tomcat");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(G0_自动配置.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");

    }
}
