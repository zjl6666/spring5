package com.zjl.SpringBoot.第03章_Web开发;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * 1. Ant风格路径用法
 * Ant风格的路径模式语法具有以下规则:
 * ●*:示任意数量的字符。
 * ●?:表示任意一个字符。
 * ●**:表示任意数量的目录。
 * ●{}:示一个命名的模式占位符。
 * ●[]:示字符集合,例如[a-z]表示小写字母。
 * 例如:
 *      ●*.html匹配任意名称,扩展名为.html的文件。
 *      ●/folder1/* /*.java匹配在folder1绿下的任意两级绿下的java文件。
 *      ●/folder2/** /*.jsp 匹配在folder2目录下任意目录深度的jsp文件。
 *      ●/{type}/{id}.html匹配任意文件名为{id}.html,在任意命名的{type}目录下的文件。
 *
 * 注意:Ant 风格的路径模式语法中的特殊字符需要转义，如:
 *      ●要匹配文件路径中的星号，则需要转义为 \\*
 *      ●要匹配文件路径中的问号，则需要转义为 \\?。
 *
 * 2.模式切换
 * AntPathMatcher 与 PathPatternParser(默认)
 *      ●PathPatternParser在jmh基准测试下，有6~8倍吞吐量提升，降低30%~ 40%空间分配率
 *      ●PathPatternParser 兼容AntPathMatcher语法,技持更多类型的路径模式
 *      ●PathPatternParser "**"多段匹配的支持仅允许在模式末尾使用
 *
 * 可以改变路径匹配策略
 *      #ant_path_matcher  老版策略
 *      #path_pattern_parser  新版版策略 也是默认  不支持  路径中间是 ** 但是效率高
 *      #spring.mvc.pathmatch.matching-strategy=ant_path_matcher
 *
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
@EnableWebMvc //禁用所有 web的自动配置
@RestController
public class D_Ant分割路径用法 {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","web");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(D_Ant分割路径用法.class, args);

    }
    //http://127.0.0.1:8080/axxx/b1/abcd/aewf/awetgf/ewfrt/eas
    @GetMapping("/a*/b?/{p1:[a-f]+}/**")
    public String hello(HttpServletRequest request, @PathVariable("p1") String path){
        System.err.println("p1的值为" + path);
        String requestURI = request.getRequestURI();
        return requestURI;
    }

}
