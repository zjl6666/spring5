package com.zjl.SpringBoot.第03章_Web开发.C_静态资源配置;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;


import java.util.concurrent.TimeUnit;

/**
 * 1.配置文件
 *      #设置静态资源访问固定前缀 路径   默认 /**
 *      #spring.mvc.static-path-pattern=/xxx/**
 * ********************************************
 * 原本不生效，是因为我引入了
 *         <dependency>
 *             <groupId>org.springframework.boot</groupId>
 *             <artifactId>spring-boot-starter-tomcat</artifactId>
 *             <scope>provided</scope>
 *         </dependency>
 *         <dependency>
 *             <groupId>org.springframework.boot</groupId>
 *             <artifactId>spring-boot-starter-test</artifactId>
 *             <scope>test</scope>
 *         </dependency>
 * 这俩，由于
 *  @see WebMvcConfigurationSupport  用到了 jakarta.servlet.ServletContext; 这个包
 *  这个包在 tomcat 10里，但是我又 <scope>provided</scope> 给不打入 jar 包中 排除了，导致
 *  jakarta.servlet 相关的包，看着有  实则，没有
 *  而这个   <scope>test</scope>  存在问题有些包在test 里有，但是不可使用，就是因为一 test引入，不可以在非test的情况下运行
 *
 * ***********************************
 *      详见 application-web.properties
 * 2.代码配置
 *      1.
 *
 *
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class}
)
public class C2_配置静态资源 {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","web");//  同理 --spring.profiles.active=xxx

        ConfigurableApplicationContext run = SpringApplication.run(C2_配置静态资源.class, args);
    }

}
//@EnableWebMvc //禁用所有 web的自动配置
@Configuration  //以下都可  给容器中 放入 WebMvcConfigurer 组件，就会自定义底层
//class MyConfig extends WebMvcConfigurationSupport {
class MyConfig implements WebMvcConfigurer {
    /**
     * @see WebMvcAutoConfiguration.EnableWebMvcConfiguration 继承了
     * @see DelegatingWebMvcConfiguration 类中
     * 这个方法，就是
     *      @Autowired
     *     public void setConfigurers(List<WebMvcConfigurer> configurers) {
     *         if (!CollectionUtils.isEmpty(configurers)) {
     *             this.configurers.addWebMvcConfigurers(configurers);
     *         }
     *
     *     }
     *  将所有    WebMvcConfigurer 的类注入 并实现
     *  这就是 继承 WebMvcConfigurer 能实现的原因
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //保留以前 配置
//         WebMvcConfigurer.super.addResourceHandlers(registry);
        //自己写 新加配置     优先级高于配置文件
        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/a/","classpath:/static/")
                .setCacheControl(CacheControl.maxAge(6666, TimeUnit.SECONDS));
    }
}