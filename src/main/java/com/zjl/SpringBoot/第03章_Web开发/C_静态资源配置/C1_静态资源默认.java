package com.zjl.SpringBoot.第03章_Web开发.C_静态资源配置;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.web.servlet.config.annotation.DelegatingWebMvcConfiguration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 *    @see WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter 继承了
 *    @see WebMvcConfigurer 的
 *        addResourceHandlers                     资源处理器  -处理静态资源规则
 *
 *      @Override
 *        public void addResourceHandlers(ResourceHandlerRegistry registry) {
 * 			if (!this.resourceProperties.isAddMappings()) {
 * 				logger.debug("Default resource handling disabled");
 * 				return;
 *            }
 * 			addResourceHandler(registry,
 *
 * 		        	//    /webjars/**  或spring.mvc.webjarsPathPattern  配置     目的是可以用 maven 引入js
 * 		            //会进入  	classpath:/META-INF/resources/webjars  下找资源
 * 		        	this.mvcProperties.getWebjarsPathPattern(),
 * 					"classpath:/META-INF/resources/webjars/");//默认路径
 *
 *
 * 			addResourceHandler(registry, this.mvcProperties.getStaticPathPattern(),//   /**   或 spring.mvc.staticPathPattern 配置
 * 		        	(registration) -> { //会进入  下面四个路径下
 * 		        // 默认 静态资源 位置 "classpath:/META-INF/resources/","classpath:/resources/", "classpath:/static/", "classpath:/public/"
 * 				// spring.web.Resources.staticLocations  配置
 * 				registration.addResourceLocations(this.resourceProperties.getStaticLocations());
 * 				if (this.servletContext != null) {
 * 					ServletContextResource resource = new ServletContextResource(this.servletContext, SERVLET_LOCATION);
 * 					registration.addResourceLocations(resource);
 *                }
 *            });
 *        }
 * 规则1：
 *      /webjars/**  或spring.mvc.webjarsPathPattern  配置     目的是可以用 maven 引入js
 *      会进入  	classpath:/META-INF/resources/webjars  下找资源
 *
 * 规则2：
 *       /**   或 spring.mvc.staticPathPattern 配置
 *       会进入
 *       classpath:/META-INF/resources/
 *       classpath:/resources/
 *       classpath:/static/
 *       classpath:/public/
 *
 * 规则3： 添加了缓存，静态资源缓存，如果服务资源没有变化，下次访问时候，就可以直接让浏览器用自己的 缓存中的数据，
 * 可以直接通过配置文件设置缓存设置， spring.web.XXX
 * 其中 addResourceHandler 方法内部
 * 			registration.setCachePeriod(getSeconds(this.resourceProperties.getCache().getPeriod()));//设置缓存周期，,默认没，以秒为单位
 * 			registration.setCacheControl(this.resourceProperties.getCache().getCachecontrol().toHttpCacheControl());//设置HTTP缓存控制-即私用缓存，共享缓存等等配置
 * 			registration.setUseLastModified(this.resourceProperties.getCache().isUseLastModified());//是否使用 lastModified 头  默认false
 * -*****************************************************************************
 *
 * @see WebMvcAutoConfiguration 定义了，
 *      默认页就是在 静态资源下找 index.html
 *     没有就在在 templates 下找 index.html
 * ****************************************************************************
 * @see WebMvcAutoConfiguration.EnableWebMvcConfiguration 继承了
 * @see DelegatingWebMvcConfiguration  （将所有 WebMvcConfigurer 的配置 都注入进去了）  又继承了
 * @see WebMvcConfigurationSupport
 *           而
 *          @see WebMvcAutoConfiguration 有这个注解 @ConditionalOnMissingBean(WebMvcConfigurationSupport.class)
 *          不存在 WebMvcConfigurationSupport 这个类才生效，
 *          当自己注入了 WebMvcConfigurationSupport 这个类。就会不加载 WebMvcAutoConfiguration
 *
 *  @see org.springframework.boot.autoconfigure.web.servlet.WelcomePageHandlerMapping
 *      是欢迎页的处理规则 在 WebMvcAutoConfiguration 的 getIndexHtmlResource 方法中
 *     favicon.ico   网站的 默认图标，有的浏览器不认
 *
 *
 */
public class C1_静态资源默认 {
    public static void main(String[] args) {

    }
}
