package com.zjl.SpringBoot.config;

import com.zjl.util.OutColour;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

//                                  应用程序侦听器         应用程序事件
public class MyListener implements ApplicationListener<ApplicationEvent> {
    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        OutColour.out.printlnYellow("=======事件到达====================",event);
    }

    @Override
    public boolean supportsAsyncExecution() {
        return ApplicationListener.super.supportsAsyncExecution();
    }
}
