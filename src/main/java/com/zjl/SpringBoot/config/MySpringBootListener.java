package com.zjl.SpringBoot.config;

import org.springframework.boot.ConfigurableBootstrapContext;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.stereotype.Component;

import java.time.Duration;

/**
 * 自定义的 监听器
 * package 的路径  --不能存在中文
 * 会在 META-INF/spring.factories   配置此路径
 */
public class MySpringBootListener implements SpringApplicationRunListener {
    @Override//ConfigurableBootstrapContext   可配置的 Boot 启动 上下文
    public void starting(ConfigurableBootstrapContext bootstrapContext) {
        System.err.println("============正在启动============");
    }
    @Override//ConfigurableBootstrapContext   可配置的 Boot 启动 上下文
    public void environmentPrepared(ConfigurableBootstrapContext bootstrapContext, ConfigurableEnvironment environment) {
        System.err.println("============环境准备完成============");
    }
    @Override//ConfigurableApplicationContext  可配置的应用程序上下文
    public void contextPrepared(ConfigurableApplicationContext context) {
        System.err.println("============上下文【ioc容器】准备完成============");
    }
    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
        System.err.println("============上下文【ioc容器】加载完成============");
    }
    @Override
    public void started(ConfigurableApplicationContext context, Duration timeTaken) {
        System.err.println("============启动完成============");
    }
    @Override
    public void ready(ConfigurableApplicationContext context, Duration timeTaken) {
        System.err.println("============应用准备就绪============");
    }
    @Override
    public void failed(ConfigurableApplicationContext context, Throwable exception) {
        System.err.println("============启动失败才生效============");
    }
}
