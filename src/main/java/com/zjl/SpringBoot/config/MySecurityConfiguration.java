package com.zjl.SpringBoot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class MySecurityConfiguration {

    @Bean //排除其他程序受到  Spring Security 的影响
    SecurityFilterChain mySecurityFilterChain(HttpSecurity http) throws Exception {

        http.authorizeHttpRequests((registry) -> {
            registry.requestMatchers("/**").permitAll().anyRequest().permitAll()
            ;
        });
        http.csrf().disable();// disable()  关闭 CSRF防御
        return http.build();
    }
}
