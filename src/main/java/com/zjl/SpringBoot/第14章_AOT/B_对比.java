package com.zjl.SpringBoot.第14章_AOT;

/**
 * |---------------|----------------------------------------------------------------------------------------
 * |对比项           |              编译器              |       解释器                               |
 * |---------------|----------------------------------------------------------------------------------------
 * |机器执行速度      |  快，因为源代码只需被转换一次        |      慢，因为每行代码都需要被解释执行            |
 * |---------------|----------------------------------------------------------------------------------------
 * |开发效率         |  慢，因为需要耗费大量时间编译        |      快，无需花费时间生成目标代码，更快的开发和测试 |
 * |---------------|----------------------------------------------------------------------------------------
 * |调试            |      难以调试编译器生成的目标代码     |        容易调试源代码，因为解释器行行地执行      |
 * |---------------|----------------------------------------------------------------------------------------
 * |可移植性(跨平台)  |  不同平台需要重新编译目标平台代码      |      同一份源码可以跨平台执行，因为每个平       |
 * |               |                                  |       台会开发对应的解释器                    |
 * |---------------|----------------------------------------------------------------------------------------
 * |学习难度        |  相对较高，要了解源代码、编译器以       |    相对较低，无需了解机器的细节               |
 * |               |      及目标机器的知识                |                                         |
 * |---------------|----------------------------------------------------------------------------------------
 * |错误检查         |  编译器可以在编译代码时检查错误        |     解释器只能在执行代码时检查错误            |
 * |---------------|----------------------------------------------------------------------------------------
 * |运行时增强       |  无                               |    可以动态增强                            |
 * |---------------|----------------------------------------------------------------------------------------
 *
 *
 *
 * |-------------------------------------------------------------------------------------------------------
 * |               |              JIT                 |     AOT                                 |
 * |-------------------------------------------------------------------------------------------------------
 * |优点            |      1.具备实时调整能力             |     1.速度快，优化了运行时编译时间和内存消耗   |
 * |               |      2.生成最优机器指令             |     2.程序初期就能达最高性能                |
 * |               |      3.根据代码运行情况优化内存占用   |     3.加快程序启动速度                     |
 * |------------------------------------------------------------------------------------------------------
 * |缺点            |      1.运行期边编译速度慢.           |     1.程序第一次编译占用时间长             |
 * |               |      2.初始编译不能达到最高性能       |     2.牺牲高级语言一些特性                 |
 * |------------------------------------------------------------------------------------------------------
 *
 *
 *
 */
public class B_对比 {
}
