package com.zjl.SpringBoot.第14章_AOT;

/**
 * 打包成本地镜像:
 * windows:
 *      1、打成jar包:注意修改 jar包内的 MANIFEST.MF 文件,指定 Main-Class 的全类名
 *           - java -jar xxx.jar 就可以执行。
 *           - 切换机器，安装java环境。默认解释执行，启动速度慢,运行速度慢
 *      2、打成本地镜像(可执行文件)
 *           windows 中 查找：这个程序，以管理员身份执行（需要先执行{@link F1_GraalVM环境配置}）
 *                x64 Native Tools Command Prompt for VS 2022
 *            执行命令  (在)target 目录下
 *                           制定操作的文件  主程序                                          生成执行文件名
 *            1.native-image -cp xxx.jar 主程序(如：com.zjl.SpringBoot.第14章_AOT.F2_打包执行) -o haha
 *            2.native-image -cp classes 主程序(如：com.zjl.SpringBoot.第14章_AOT.F2_打包执行) -o haha
 *  linux
 *      1.sudo yum install gcc glibc-devel zlib-devel  安装 gcc 初始环境
 *      2.{@link F1_GraalVM环境配置} 下载 linux的环境  配置graalvm的 JDK 并配置环境变量
 *          #修改环境变量
 *              vim /etc/profile
 *          #添加环境变量内容
 *              export JAVA_HOME=/opt/java/graalvm-ce-java17-22.3.2
 *              export PATH=$PATH:$JAVA_HOME/bin
 *          #刷新环境变量内容
 *              source /etc/profile
 *          #查看jdk版本
 *              java -version
 *      3.安装 native-image
 *          gu install --file native-image-installable-svm-java17-windows-amd64-22.3.2.jar
 *      4.生成程序           制定操作的文件  主程序                                          生成执行文件名
 *          native-image -cp xxx.jar 主程序(如：com.zjl.SpringBoot.第14章_AOT.F2_打包执行) -o haha
 *
 *
 */
public class F2_打包执行 {
    public static void main(String[] args) {

    }
}
