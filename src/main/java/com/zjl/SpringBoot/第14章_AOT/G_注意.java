package com.zjl.SpringBoot.第14章_AOT;

/**
 * 并不是所有的Java代码都能支持本地打包
 *
 *   动态能力损失:反射的代码: (动态获取构造器， 反射创建对象，反射调用一些方法)
 *       额外处理(SpringBoot提供了一些注解) :提前告知graalvm反射会用到哪些方法、构造器
 *   配置文件损失:
 *       解决方案:额外处理(配置中心) :提前告知graalvm 配置文件怎么处理
 *
 * 等二进制不能包含的，不能动态的都需要提前处理
 *
 * 不是所有框架都适配了AOT特性; Spring 全系列栈适配0K
 *
 * 如果需要加载的，可以仿照
 *      spring-boot-actuator-autoconfigure 的jar包里的
 *          META-INF/spring/aot.factories 写入自己的
 *
 *  ● mvn -Pnative native:build -f pom.xml
 *  ● 在 Plugins 下回有 native 下的 native:build  即可
 *      1.添加 maven的打包程序
 *        <plugin>
 *              <groupId>org.graalvm.buildtools</groupId>
 *              <artifactId>native-maven-plugin</artifactId>
 *        </plugin>
 *      2.需要的准备 见G_需要的准备.png
 *      3.重启 idea
 *      4.settings 搜索 Terminal
 *          使用cmd 的窗口 就可以使用了
 *
 */
public class G_注意 {

}
