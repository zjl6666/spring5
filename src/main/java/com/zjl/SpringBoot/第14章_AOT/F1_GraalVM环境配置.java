package com.zjl.SpringBoot.第14章_AOT;

/**
 * https://www.graalvm.org/
 * 原生镜像: native-image (机器码、本地镜像)
 *      ●把应用打包成能适配本机平台的可执行文件(机器码、本地镜像)
 *
 * GraalVM是一个高性能的JDK， 旨在加速用Java和其他JVM语言编写的应用程序的执行，同时还提供JavaScript、Python和许多其他流行语言的运行时。
 * GraalVM提供了两种运行Java应用程序的方式:
 *      ●1.在HotSpot JVM.上使用Graal即时 (JIT) 编译器
 *      ●2.作为预先编译(AOT) 的本机可执行文件运行(本地镜像)。
 * GraalVM的多语言能力使得在单个应用程序中混合多种编程语言成为可能，同时消除了外部语言调用的成本。
 *
 * 1.编译 Windows 下的执行文件
 *      下载路径：
 *      https://visualstudio.microsoft.com/zh-hans/free-developer-offers/
 *
 *      要记住安装路径  默认：C:\Program Files\Microsoft Visual Studio\2022\Community
 *      x64 Native Tools Command Prompt for VS 2022
 *
 *  第一种  2  3
 *       2.下载  GraalVM 的 jdk https://www.graalvm.org/downloads/
 *
 *       3.下载  GraalVM 的运行器
 *               cmd   联网下载 GraalVM 的运行器  前提是搞好 graalvm 的  jdk 环境变量
 *                      gu install native-image
 *  第二种  2  3
 *          2/3. https://github.com/graalvm/graalvm-ce-builds/releases/tag/vm-22.3.2
 *                  graalvm 的 java的jdk
 *                       graalvm-ce-java17-windows-amd64-22.3.2.zip
 *                  GraalVM 的运行器
 *                       native-image-installable-svm-java17-windows-amd64-22.3.2.jar
 *  4.将 graalvm 的 java的jdk 的解压并修改环境变量
 *  5.cmd
 *      安装
 *              gu install --file native-image-installable-svm-java17-windows-amd64-22.3.2.jar
 *      //测试 出现 Please specify options for native-image building or use help for more info.  表示成功
 *              native-image
 *
 */
public class F1_GraalVM环境配置 {

}
