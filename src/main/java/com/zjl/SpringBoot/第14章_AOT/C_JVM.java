package com.zjl.SpringBoot.第14章_AOT;

/**
 * JVM:既有解释器,又有编辑器(JIT: 即时编译)
 *
 * .java  -->javac(词法分析、语法分析、语义分析) --> .class
 *      -> 方法调用
 *
 *                |-> 是 ->  编译执行    -----------------> CodeCache  <--
 *      ->是否编译 |                                                     |
 *                |-> 否 ->  解释执行调用次数统计 -> 是否到达阈值  ->(是) 触发编译
 *
 *
 *  JVM编译器
 * JVM中集成了两种编译器，Client Compiler和Server Compiler;
 *      ●Client Compiler注重启动速度和局部的优化
 *      ●Server Compiler更加关注全局优化,性能更好，但由于会进行更多的全局分析，所以启动速度会慢。
 * Client Compiler:
 *      ●HotSpot VM 带有一个Client Compiler C1编译器
 *      ●这种编译器启动速度快,但是性能比较Server Compiler来说会差一些。
 * Server Compiler:
 *      ●Hotspot虚拟机中使用的Server Compiler有两种: C2和 Graal。
 *      ●在Hotspot VM中，默认的Server Compiler是C2编译器。
 *
 *
 */
public class C_JVM {

}
