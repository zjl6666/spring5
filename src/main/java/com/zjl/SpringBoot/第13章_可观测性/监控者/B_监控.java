package com.zjl.SpringBoot.第13章_可观测性.监控者;

import com.zjl.SpringBoot.config.MySecurityConfiguration;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.nio.charset.Charset;

/**
 * 需要
 * @see MySecurityConfiguration
 * 和maven
 * <dependency>
 *     <groupId>de.codecentric</groupId>
 *     <artifactId>spring-boot-admin-starter-server</artifactId>
 *     <version>${spring.boot.admin.version}</version>
 * </dependency>
 *
 * 只有所有实例都正常才会是正常状态     如有redis的包，但是没有配置redis就是部分失败
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class, RedisAutoConfiguration.class}
)
@EnableAdminServer //开启监控页面
public class B_监控 {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","监控");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(B_监控.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");

    }
}
