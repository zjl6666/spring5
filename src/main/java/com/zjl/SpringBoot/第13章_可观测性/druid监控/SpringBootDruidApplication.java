package com.zjl.SpringBoot.第13章_可观测性.druid监控;

import com.zjl.SpringBoot.config.MySecurityConfiguration;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;

import java.nio.charset.Charset;

/**
 * http://127.0.0.1:7777/druid
 * 需要数据库
 */
@SpringBootApplication(
        exclude = {
//                DataSourceAutoConfiguration.class,
                RedisAutoConfiguration.class}
)
@EnableAdminServer //开启监控页面
public class SpringBootDruidApplication {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","druid");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(SpringBootDruidApplication.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");

    }
}
