package com.zjl.SpringBoot.第13章_可观测性.被监控着;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.nio.charset.Charset;

/**
 *  maven
 *  <dependency>
 *     <groupId>org.springframework.boot</groupId>
 *     <artifactId>spring-boot-starter-actuator</artifactId>
 * </dependency>
 *  <dependency>
 *      <groupId>de.codecentric</groupId>
 *      <artifactId>spring-boot-admin-starter-client</artifactId>
 *      <version>${spring.boot.admin.version}</version>
 *  </dependency>
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class, RedisAutoConfiguration.class}
)
public class B_被监控1 {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","被监控1");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(B_被监控1.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");

    }
}
