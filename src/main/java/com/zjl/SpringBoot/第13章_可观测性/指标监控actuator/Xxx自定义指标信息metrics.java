package com.zjl.SpringBoot.第13章_可观测性.指标监控actuator;


import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 指标信息：
 *      http://localhost:9999/actuator/metrics
 * 自定义指标信息，如 myActuator.xxx
 * http://localhost:9999/actuator/metrics/myActuator.xxx
 *
 */
@Component
@RestController
public class Xxx自定义指标信息metrics {
    Counter counter;
    //注入 MeterRegistry 来保存和统计所有指标
    public Xxx自定义指标信息metrics(MeterRegistry meterRegistry){
        counter = meterRegistry.counter("myActuator.xxx");//自定义指标信息
    }
    @RequestMapping("/xxx")
    public void xxx(){
        counter.increment();// +1
        System.out.println("第" + counter.count() + "次");
    }
}
