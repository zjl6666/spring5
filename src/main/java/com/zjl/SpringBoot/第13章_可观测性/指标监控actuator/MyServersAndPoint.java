package com.zjl.SpringBoot.第13章_可观测性.指标监控actuator;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;

@Component
@Endpoint(id = "myServers") //自定义监控
//http://localhost:9999/actuator/myServers
public class MyServersAndPoint {

    @ReadOperation//端点读操作
    public Map<String, Object> getDockerInfo() {
        return Collections.singletonMap("我的", "自定义");
    }

    @WriteOperation//端点写操作
    public void stopDocker() {
        System.out.println("docker stopped");
    }
}
