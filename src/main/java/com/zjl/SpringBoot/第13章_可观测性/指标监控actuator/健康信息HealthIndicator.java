package com.zjl.SpringBoot.第13章_可观测性.指标监控actuator;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Health Endpoint
 * 健康检查端点，我们一般用于在云平台，平台会定时的检查应用的健康状况，
 *  我们就需要Health Endpoint可以为平台返回当前应用的一系列组件健康状况的集合。
 * 重要的几点：
 * ● health endpoint返回的结果，应该是一系列健康检查后的一个汇总报告
 * ● 很多的健康检查默认已经自动配置好了，比如：数据库、redis等
 * ● 可以很容易的添加自定义的健康检查机制
 * ● 只要存在一个是 down  掉的，状态就是 down 掉的
 * //监控信息配置
 * http://localhost:9999/actuator/health  配置这个页面信息
 *
 */
@Component//表示这是个配置文件
public class 健康信息HealthIndicator extends AbstractHealthIndicator {
    //前端会把 HealthIndicator 自动去掉
    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        Map<String,Object> reData = new HashMap<>();
        if(1 == 1){ //业务代码
//            builder.up();//表示健康
            builder.status(Status.UP);//表示健康
            reData.put("count",1);
            reData.put("ms",100);
        } else {
//            builder.down();//表示不健康
            //其他不健康的各种情况
            builder.status(Status.OUT_OF_SERVICE);
            builder.status(Status.DOWN);
            builder.status(Status.UNKNOWN);
        }

        builder.withDetails(reData);//放入
        builder.withDetail("K","V");
    }
}
