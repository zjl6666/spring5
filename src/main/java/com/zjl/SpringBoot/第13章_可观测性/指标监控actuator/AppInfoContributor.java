package com.zjl.SpringBoot.第13章_可观测性.指标监控actuator;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
//http://localhost:9999/actuator/info 定制这个页面的信息第二种   最终结果是合并后的
public class AppInfoContributor implements InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("msg0","你好")
                .withDetails(new HashMap<>())
                .withDetail("k","v");
    }
}
