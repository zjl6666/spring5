package com.zjl.SpringBoot.第13章_可观测性.指标监控actuator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.nio.charset.Charset;

/**
 * 对线上应用进行观测、监控、预警...
 * ●健康状况[组件状态存活状态] Health
 * ●运行指标[cpu. 陈垃圾回收、吐量响应成功率] Metrics
 * ●链路追踪
 *
 * 引入
 *         <dependency>
 *             <groupId>org.springframework.boot</groupId>
 *             <artifactId>spring-boot-starter-actuator</artifactId>
 *         </dependency>
 *
 * 访问地址：
 *      http://127.0.0.1:9999/actuator
 * 显示有哪些组件 bean
 *      http://127.0.0.1:9999/actuator/beans
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class, RedisAutoConfiguration.class}
)
public class A_指标监控acutuator {
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","actuator");//  同理 --spring.profiles.active=xxx

        SpringApplication.run(A_指标监控acutuator.class, args);
        System.err.println("***********启动完毕**当前编码:" + Charset.defaultCharset().name() + "****************");

    }
}
