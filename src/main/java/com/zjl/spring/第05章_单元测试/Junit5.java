package com.zjl.spring.第05章_单元测试;


import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;


//@ExtendWith(SpringExtension.class)//JUnit5单元测试框架
//@ContextConfiguration("classpath:测试类.xml")//加载配置文件  45都一样
@SpringJUnitConfig(locations = "classpath:测试类.xml")//JUnit5单元测试框架 一个当上两个
public class Junit5 {
    @Autowired
    private JUnitService jUnitService;

    @org.junit.jupiter.api.Test
    public void JUnit5测试(){
        jUnitService.add();
    }
}
