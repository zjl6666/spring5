package com.zjl.spring.第05章_单元测试;

/**
 * ClassName: JUnit4Service
 * Package: com.zjl.spring.第05章_单元测试
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/20 18:15
 * @Version 1.0
 */
public class JUnitService {
    public void add() {
        System.out.println("JUnitService");
    }
}
