package com.zjl.spring.第05章_单元测试;


import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)//指定JUnit4单元测试框架
@ContextConfiguration("classpath:测试类.xml")//加载配置文件  45都一样
public class Junit4 {
    @Autowired
    private JUnitService jUnitService;

    @org.junit.Test
    public void JUnit4测试(){
        jUnitService.add();
    }

}
