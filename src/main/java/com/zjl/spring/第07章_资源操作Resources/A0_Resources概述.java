package com.zjl.spring.第07章_资源操作Resources;

/**
 * Java的标准java.net.URL类和各种URL前缀的标准处理程序无法满足所有对low-level资源的访问，
 *      比如:没有标准化的URL实现可用于访问需要从类路径或相对于ServletContext获取的资源。
 *      并且缺少某些Spring所需要的功能，例如检测某资源是否存在等。
 *      而Spring的Resource声明了访问low-level资源的能力。
 *
 *
 *  UrlResource访问网络资源
 *      @see A1_UrlResource
 *  ClassPathResource 访问类路径下资源
 *      @see A2_ClassPathResource
 *  FileSystemResource 访问文件资源系统
 *      @see A3_FileSystemResource
 *  ServletContextResource
 *          这是ServletContext资源的Resource实现，它解释相关Web应用程序根目录中的相对路径。
 *          它始终支持流(stream)访问和URL访问，
 *          但只有在扩展Web应用程序存档且资源实际位于文件系统上时才允许java.io.File访问。
 *          无论它是在文件系统上扩展还是直接从AR或其他地方(如数据库)访问，实际上都依赖于Servlet容器。
 *  InputStreamResource
 *          InputStreamResource是给定的输入流(InputStream)的Resource实现。
 *          它的使用场景在没有特定的资源实现的 时候使用(感觉和@Component的适用场景很相似)。
 *          与其他Resource实现相比，这是已打开资源的描述符。
 *          因此，它的isOpen()方法返回true。 如果需要将资源描述符保留在某处或者需要多次读取流，请不要使用它。
 *  ByteArrayResource
 *          字节数组的Resource实现类。通过给定的数组创建了一个ByteArrayInputStream。
 *          它对于从任何给定的字节数组加载内容非常有用，而无需求助于单次使用的 InputStreamResource。
 *
 */
public class A0_Resources概述 {


}
