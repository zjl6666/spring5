package com.zjl.spring.第07章_资源操作Resources;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

/**
 * Resource的一个实现类,用来访问网络资源，它支持URL的绝对路径。
 * http-----该前缀用于访问基于HTTP协议的网络资源。
 * fp-----该前缀用于访问基于FTP协议的网络资源
 * file: ----该前缀用于从文件系统中读取资源
 *
 * 实验:访问基于HTTP协议的网络资源
 *
 */
public class A1_UrlResource {
    @Test
    public void UrlDemo() throws Exception {
        Resource url= new UrlResource("https://www.baidu.com/");

        System.out.println(url.contentLength());
        System.out.println(url.getDescription());
        System.out.println(new String(url.getContentAsByteArray()));
    }
    @Test
    public void fileDemo() throws Exception {
        Resource file= new UrlResource("file:logs/api_his_info.log");
        int length = file.getContentAsByteArray().length;
        System.out.println(length);
        System.out.println(file.getDescription());
        System.out.println(file.getFilename());
    }
}
