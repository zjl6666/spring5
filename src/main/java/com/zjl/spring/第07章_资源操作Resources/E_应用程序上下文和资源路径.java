package com.zjl.spring.第07章_资源操作Resources;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 * 不管以怎样的方式创建ApplicationContext实例，都需要为ApplicationContext指定配置文件，
 * Spring允许使用一份或多分XML配置文件。
 * 当程序创建ApplicationContext实例时,通常也是以Resource的方式来访问配置文件的，
 * 所以ApplicationContext完全支持ClassPathResource、FileSystemResource、 ServletContextResource等资源访问方式。
 *
 * ApplicationContext确定资源访问策略通常有两种方法:
 *      (1)使用ApplicationContext实现类指定访问策略。
 *      (2)使用前缀指定访问策略。
 */
public class E_应用程序上下文和资源路径 {
    public static void main(String[] args) {
        //classpath* :前缀提供了加载多个XML配置文件的能力，当使用classpath* :前缀来指定XML配置文件时，系统将搜
        //索类加载路径，找到所有与文件名匹配的文件,分别加载文件中的配置定义,最后合并成一个ApplicationContext。
        //IOC*.xml 加载IOC开头的所有xml文件
        ApplicationContext classPath = new ClassPathXmlApplicationContext("classpath:IOC*.xml");
        Object ioc1 = classPath.getBean("userDao");
        System.out.println(ioc1.getClass());
    }
}
