package com.zjl.spring.第07章_资源操作Resources;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

/**
 * 前面介绍了Spring提供的资源访问策略，但这些依赖访问策略要么需要使用Resource实现类，
 * 要么需要使用ApplicationContext来获取资源。
 * 实际上，当应用程序中的Bean实例需要访问资源时，Spring 有更好的解决方法:
 *      直接利用依赖注入。从这个意义上来看, Spring 框架不仅充分利用了策略模式来简化资源访问，
 *      而且还将策略模式和IOC进行充分地结合，最大程度地简化了Spring 资源访问。
 *
 * 归纳起来，如果Bean实例需要访问资源，有如下两种解决方案:
 *      ●代码中获取Resource实例。
 *      ●使用依赖注入。
 *
 * 对于第一种方式，当程序获取Resource实例时，总需要提供Resource所在的位置，
 * 不管通过FileSystemResource创建实例，还是通过ClassPathResource创建实例，
 * 或者通过ApplicationContext的getResource()方法获取实例，都需要提供资源位置。
 * 这意味着:资源所在的物理位置将被耦合到代码中，如果资源位置发生改变，则必须改写程序。
 * 因此，通常建议采用第二种方法，让Spring为Bean实例依赖注入资源。
 *
 * 实验:让Spring为Bean实例依赖注入资源
 */
public class D_Resources作为属性 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("Resources.xml");
//        ResourcesBean bean = context.getBean(ResourcesBean.class);
        ResourcesBean bean = context.getBean("resourceBean", ResourcesBean.class);
        bean.parse();


    }
}
class ResourcesBean{
    private Resource resource;

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }
    public void parse(){
        System.out.println(resource.getDescription());
        System.out.println(resource.getFilename());
    }
}