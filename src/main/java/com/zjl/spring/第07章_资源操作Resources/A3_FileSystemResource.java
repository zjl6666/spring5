package com.zjl.spring.第07章_资源操作Resources;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

/**
 * ClassName: B3_FileSystemResource
 * Package: com.zjl.spring.第07章_资源操作Resources
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/21 15:29
 * @Version 1.0
 */
public class A3_FileSystemResource {
    @Test
    public void ClassPathDemo() throws Exception {
//        Resource fileSystem = new FileSystemResource("static/banner.txt");//默认是 系统目录，此方法是错的
        Resource fileSystem = new FileSystemResource("src/main/resources/static/banner.txt");//默认是 系统目录
//        Resource fileSystem = new FileSystemResource("D:\\work\\gitee\\Spring\\src\\main\\resources\\static\\banner.txt");

        System.out.println(fileSystem.contentLength());
        System.out.println(fileSystem.getDescription());
        System.out.println(new String(fileSystem.getContentAsByteArray()));
    }
}
