package com.zjl.spring.第07章_资源操作Resources;

import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;

import java.io.IOException;

/**
 * Spring提供如下两个标志性接口:
 *      (1) ResourceLoader :该接口实现类的实例可以获得一个Resource实例。
 *      (2) ResourceLoaderAware :该接口实现类的实例将获得一个ResourceLoader的引用。
 * 在ResourceLoader接口里有如下方法:
 *      (1) Resource getResource (String location) : 该接口仅有这个方法，用于返回一个Resource实例。
 * ApplicationContext实现类都实现ResourceLoader接口，因此ApplicationContext可直接获取Resource实例。
 *
 * Spring 将采用和 ApplicationContext 相同的策略来访问资源。
 * 也就是说，
 * ●如果 ApplicationContext 是 FileSystemXmlApplicationContext,
 *  res就是 FileSystemResource 实例;
 * ●如果 ApplicationContext 是 ClassPathXmlApplicationContext，
 *  res就是 ClassPathResource 实例
 * ●如果 ApplicationContext 是 XmlWebApplicationContext
 *  res就是 ServletContextResource进 行资源访问。
 *
 * 当Spring应用需要进行资源访问时，实际上并不需要直接使用Resource实现类,而是调用ResourceLoader实例的
 * getResource()方法来获得资源，ReosurceLoader将 会负责选择Reosurce实现类，也就是确定具体的资源访问策
 * 略,从而将应用程序和具体的资源访问策略分离开来
 * 另外，使用ApplicationContext访问资源时,可通过不同前缀指定强制使用指定的ClassPathResource.
 * FileSystemResource等实现类
 * Resource res = ctx.getResource("classpath:bean.xml");
 * Resource res = ctx.getResource("file:bean.xml");
 * Resource res = ctx.getResource("http://localhost:8080/beans.xml");
 */
public class B_ResourceLoader {
    public static void main(String[] args) throws IOException {
        ApplicationContext context = new ClassPathXmlApplicationContext();
        System.out.println("-------------------------------------------");
        Resource resource = context.getResource("application.yml");
        System.out.println(new String(resource.getContentAsByteArray()));
    }
    @Test
    public void FileSystemDemo() throws Exception {
        ApplicationContext context = new FileSystemXmlApplicationContext();
        Resource resource = context.getResource("src/main/resources/application.yml");
        System.out.println(resource.contentLength());
        System.out.println(resource.getDescription());
        System.out.println(new String(resource.getContentAsByteArray()));
    }
    @Test
    public void ClassPathDemo() throws Exception {
        ApplicationContext context = new FileSystemXmlApplicationContext();
        //指定获取方式
        Resource resource = context.getResource("classpath:application.yml");
        System.out.println(resource.contentLength());
        System.out.println(resource.getDescription());
        System.out.println(new String(resource.getContentAsByteArray()));
    }
}
