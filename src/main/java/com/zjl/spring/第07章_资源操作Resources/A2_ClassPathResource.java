package com.zjl.spring.第07章_资源操作Resources;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * ClassName: B2_ClassPathResource
 * Package: com.zjl.spring.第07章_资源操作Resources
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/21 15:22
 * @Version 1.0
 */
public class A2_ClassPathResource {
    @Test
    public void ClassPathDemo() throws Exception {
        Resource classPath= new ClassPathResource("static/banner.txt");

        System.out.println(classPath.contentLength());
        System.out.println(classPath.getDescription());
        System.out.println(new String(classPath.getContentAsByteArray()));
    }
}
