package com.zjl.spring.第06章_事务;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 1、在 service 类上面添加注解@Transactional，在这个注解里面可以配置事务相关参数
 *      1.propagation：事务传播行为    a方法调用b方法
 *          多事务方法直接进行调用，这个过程中事务 是如何进行管理的
 *        a有 的意思是   有前面的事务传播行为
 *          REQUIRED(0),  如果a有   共用一个事务  a没  a没事务
 *              默认的    异常被处理  相当与没发生异常  不会滚
 *          REQUIRES_NEW(3),当前的方法必须启动新事务，并在它自己的事务内运行.如果有事
 *                          务正在运行，应该将它挂起
 *          SUPPORTS(1),如果有事务在运行，当前的方法就在这个事务内运行，否则它可以不
 *                      运行在事务中，
 *          MANDATORY(2),当前的方法必须运行在事务内部，
 *                      如果没有正在运行的事务，就抛出异常
 *          NOT_SUPPORTED(4),当前的方法不应该运行在事务中，如果有运行的事务， 将它挂起
 *          NEVER(5),当前的方法不应该运行在事务中，如果有运行的事务，就抛出异常
 *          NESTED(6);如果有事务在运行，当前的方法就应该在这个事务的嵌套事务内运
 *                  行，否则，就启动一个新的事务，并在它自己的事务内运行，
 *      2.isolation：事务隔离级别
 *          （1）事务有特性成为隔离性，多事务操作之间不会产生影响。不考虑隔离性产生很多问题
 *          （2）有三个读问题：脏读、不可重复读、虚（幻）读
 *                 脏读(Drity Read)：
 *                    某个事务已更新一份数据，另一个事务在此时读取了同一份数据，
 *                    由于某些原因，前一个RollBack(回滚)了操作，则后一个事务所读取的数据就会是不正确的。
 *                不可重复读(Non-repeatable read):不可重复读是读异常
 *                    在一个事务的两次查询之中数据不一致，
 *                    这可能是两次查询过程中间插入了一个事务更新的原有的数据。
 *                幻读(Phantom Read):幻读则是写异常
 *                    在一个事务的两次查询中数据笔数不一致，
 *                    例如有一个事务查询了几列(Row)数据，
 *                    而另一个事务却在此时插入了新的几列数据，
 *                    先前的事务在接下来的查询中，就会发现有几列数据是它先前所没有的。
 *          READ_UNCOMMITTED   读未提交   废物
 *          READ_COMMITTED     读已提交    可以解决脏读
 *          REPEATABLE_READ    可重复读   可以解决脏读和不可重复读
 *          SERIALIZABLE       串行化      可以解决脏读和不可重复读和幻读
 *          DEFAULT            默认     使用数据库默认的事物隔离级别
 *
 *      3.timeout：超时时间
 *          事务在一定时间内进行提交   否者回滚
 *          默认值时-1(永不超时)  可以设置参数  以秒为单位
 *      4.readOnly：是否只读
 *          默认false(可以增删改查)    true(只能查询操作)
 *      5.rollbackFor：回滚
 *          设置那些 异常进行回滚
 *      6.noRollbackFor：不回滚
 *          设置那些 异常不进行回滚
 *
 */
@Transactional(
        propagation = Propagation.REQUIRED,//事物的传播行为
        isolation = Isolation.REPEATABLE_READ,//事务隔离级别
        timeout = 5,//默认-1   永不超时   超时时间 以秒为单位，超时直接回滚
        readOnly = false,//是否只读  默认false  可修改     true只能查询
        rollbackFor = {OutOfMemoryError.class},//哪些异常回滚
        rollbackForClassName = {"java.lang.OutOfMemoryError"},//哪些异常回滚
        noRollbackFor = {/*Throwable.class*/},//哪些异常不回滚
        noRollbackForClassName = {"java.lang.ArithmeticException"}//哪些异常不回滚
)
public class C_Transactional事务注解详情 {
}
