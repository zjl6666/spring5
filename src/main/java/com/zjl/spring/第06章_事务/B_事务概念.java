package com.zjl.spring.第06章_事务;

/**
 * ①什么是事务  ACID
 *      数据库事务(transaction)是访问并可能操作各种数据项的一个数据库操作序列，
 *      这些操作要么全部执行,要么全部不执行，是一个不可分割的工作单位。
 *      事务由事务开始与事务结束之间执行的全部数据库操作组成。
 * ②事务的特性
 *      A:原子性(Atomicity)
 *      一个事务(transaction)中的所有操作，要么全部完成，要么全部不完成,不会结束在中间某个环节。
 *      事务在执行程中发生错误，会被回滚(Rollback) 到事务开始前的状态，就像这个事务从来没有执行过一样。
 *      C:一致性(Consistency)
 *      事务的一致性指的是在一个事务执行之前和执行之后数据库都必须处于一致性状态。
 *      如果事务成功地完成，那么系统中所有变化将正确地应用，系统处于有效状态。
 *      如果在事务中出现错误，那么系统中的所有变化将自动地回滚，系统返回到原始状态。
 *      I:隔离性(Isolation)
 *      指的是在并发环境中，当不同的事务同时操纵相同的数据时，每个事务都有各自的完整数据空间。
 *      由并发事务所做的修改必须与任何其他并发事务所做的修改隔离。
 *      事务查看数据更新时，数据所处的状态要么是另一事务修改它之前的状态，
 *      要么是另一事务修改它之后的状态,事务不会查看到中间状态的数据。
 *      D:持久性(Durability)
 *      指的是只要事务成功结束，它对数据库所做的更新就必须保存下来。
 *      即使发生系统崩溃，重新启动数据库系统后,数据库还能恢复到事务成功结束时的状态。
 *
 * ③事务隔离级别
 *      （1）事务有特性成为隔离性，多事务操作之间不会产生影响。不考虑隔离性产生很多问题
 *      （2）有三个读问题：脏读、不可重复读、虚（幻）读
 *             脏读(Drity Read)：
 *                某个事务已更新一份数据，另一个事务在此时读取了同一份数据，
 *                由于某些原因，前一个RollBack(回滚)了操作，则后一个事务所读取的数据就会是不正确的。
 *            不可重复读(Non-repeatable read):不可重复读是读异常
 *                在一个事务的两次查询之中数据不一致，
 *                这可能是两次查询过程中间插入了一个事务更新的原有的数据。
 *            幻读(Phantom Read):幻读则是写异常
 *                在一个事务的两次查询中数据笔数不一致，
 *                例如有一个事务查询了几列(Row)数据，
 *                而另一个事务却在此时插入了新的几列数据，
 *                先前的事务在接下来的查询中，就会发现有几列数据是它先前所没有的。

 */
public class B_事务概念 {
}
