package com.zjl.spring.第06章_事务;

import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 1、什么是 JdbcTemplate
 * （1）Spring 框架对 JDBC 进行封装，使用 JdbcTemplate 方便实现对数据库操作
 * 2、准备工作
 *  （1）引入相关 jar 包
 *  （2）在 spring 配置文件配置数据库连接池          JdbcTemplate.xml 里
 *  （3）配置 JdbcTemplate 对象，注入 DataSource   JdbcTemplate.xml 里
 *  （4）创建 service 类，创建 dao 类，在 dao 注入  jdbcTemplate 对象
 *
 * JavaEE
 *    WEB层      接口 和前端连接
 *    Service层  业务操作    事务一般加在这
 *    Dao层      数据库操作
 *    见 JdbcTemplateDemo这个包
 *
 *
 * 后续会使用 Mybatis
 *
 *
 */
public class A_JdbcTemplate概述 {
}
