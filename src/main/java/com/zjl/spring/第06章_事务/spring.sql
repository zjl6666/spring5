CREATE table IF NOT EXISTS spring.bank (
	username varchar(100) NOT NULL COMMENT '名字',
	money DECIMAL(18,2) unsigned NOT NULL COMMENT '金额',
	CONSTRAINT bank_pk PRIMARY KEY (username)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci
COMMENT='spring事务测试';


INSERT INTO spring.bank(username,money)
values('张三',10000.00),('李四',10000.00);
