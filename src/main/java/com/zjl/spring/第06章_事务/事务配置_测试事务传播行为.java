package com.zjl.spring.第06章_事务;

import com.zjl.spring.第06章_事务.spring.BankService;
import com.zjl.spring.第06章_事务.spring.BankServiceX;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.math.BigDecimal;

public class 事务配置_测试事务传播行为 {
    static ApplicationContext context =
            new ClassPathXmlApplicationContext("注解事务.xml");//注解方式  完成事务  需要BankService有事务注解

    public static void main(String[] args) {

        BankServiceX bankServicex = context.getBean("bankServiceX", BankServiceX.class);
        BankService bankService = context.getBean("bankService", BankService.class);
        try {
            System.out.println(bankService.queryAll());
            BigDecimal[] bigDecimals ={BigDecimal.valueOf(500),BigDecimal.valueOf(600)};
            //false  正常执行   true:模拟异常回滚
            bankServicex.共用一个事物(bigDecimals,false);//共用一个事物，一个失败全失败
//            bankServicex.新创建事务(bigDecimals,false);//新创建事务  我的事务是一个新的事务,不受外部事物影响
//            bankServicex.我无事务(bigDecimals,false);//我无事务   结果同上,因为外部无事务

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            System.out.println(bankService.queryAll());
        }
    }

}



