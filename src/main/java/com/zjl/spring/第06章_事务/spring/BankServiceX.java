package com.zjl.spring.第06章_事务.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 *
 * 如果
 *
 */
@Service
public class BankServiceX {
    @Autowired
    BankService bankService;
    @Transactional
    public void 共用一个事物(BigDecimal[] ranges, boolean b) {
        for (BigDecimal range:ranges){
            bankService.accountMoney(range,b);
        }
    }

    @Transactional//由于调用的 accountMoney2 为 启动新事务  所以导致无法保证次事务的完整性,因为他自带事务的完整
    public void 新创建事务(BigDecimal[] ranges, boolean b) {
        for (BigDecimal range:ranges){
            bankService.accountMoney2(range,b);
        }
    }

    public void 我无事务(BigDecimal[] ranges, boolean b) {
        for (BigDecimal range:ranges){
            bankService.accountMoney2(range,b);
        }
    }
}
