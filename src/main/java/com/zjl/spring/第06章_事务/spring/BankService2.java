package com.zjl.spring.第06章_事务.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service
public class BankService2 {
    @Autowired
    BankDao bankDao;

    public void accountMoney(BigDecimal range, boolean b) {
        //张三 少 100
        bankDao.reduceMoney("张三",range);
        if(b){
            int a=10/0;
        }
        //李四 多 100
        bankDao.plusMoney("李四",range);
    }
    public List<Map<String, Object>> queryAll() {
        return bankDao.query();
    }
    public List<Bank> queryBank() {
        //张三 少 100
        return bankDao.queryBank();
    }
}