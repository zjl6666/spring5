package com.zjl.spring.第06章_事务.spring;


import lombok.*;

import java.math.BigDecimal;

@ToString//toString方法
@Data//get set方法
@NoArgsConstructor//无参构造器
@AllArgsConstructor//全参构造器
@EqualsAndHashCode
//EqualsAndHashCode方法
public class Bank {
    private String username;
    private BigDecimal money;

}

