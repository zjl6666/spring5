package com.zjl.spring.第06章_事务.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service
@Transactional //事务注解 加在类上，表示此类的全部方法都是事务的
public class BankService{
    @Autowired
    BankDao bankDao;

    @Transactional//事务注解 加在方法上，表示此方法是事务的
    public void accountMoney(BigDecimal range, boolean b) {
        //张三 少 100
        bankDao.reduceMoney("张三",range);
        if(b){
            int a=10/0;
        }
        //李四 多 100
        bankDao.plusMoney("李四",range);
    }
    @Transactional(
            propagation = Propagation.REQUIRES_NEW//事物的传播行为 表示
    ) //启动新事务    我自己独立一个事务,外部调我,我只要成功了,就会提交事务,不受调用者的事务
    public void accountMoney2(BigDecimal range, boolean b) {
        //张三 少 100
        bankDao.reduceMoney("张三",range);
        if(b){
            int a=10/0;
        }
        //李四 多 100
        bankDao.plusMoney("李四",range);
    }
    public List<Map<String, Object>> queryAll() {
        //张三 少 100
        return bankDao.query();
    }
    public List<Bank> queryBank() {
        //张三 少 100
        return bankDao.queryBank();
    }
}