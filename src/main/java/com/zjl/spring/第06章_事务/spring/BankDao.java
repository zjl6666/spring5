package com.zjl.spring.第06章_事务.spring;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

interface BankDao{
    void plusMoney(String username,BigDecimal range);
    void reduceMoney(String username, BigDecimal range);
    List<Map<String, Object>> query();
    List<Bank> queryBank();
}
