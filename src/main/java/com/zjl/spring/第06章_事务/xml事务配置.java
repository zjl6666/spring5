package com.zjl.spring.第06章_事务;

import com.zjl.spring.第06章_事务.spring.BankService;
import com.zjl.spring.第06章_事务.spring.BankService2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.math.BigDecimal;

/**
 * 1、在 spring 配置文件中进行配置
 * 第一步 配置 事务管理器
 * 第二步 配置 通知
 * 第三步 配置 切入点和切面
 *
 *  详见    注解事务.xml
 *
 *
 *
 */
public class xml事务配置 {
    //    开启事务注解 以注解形式做事务      会与注解方式开始事务  产生冲突！！！！！！！！！！！！！！！！！！！！！！
//    即@EnableTransactionManagement//开启事务！！！！！！！！！！！！！！！！！！！！！！！！！
    static ApplicationContext context =
            new ClassPathXmlApplicationContext("xml事务.xml");//xml方式 完成事务  需要把冲突的去掉

    public static void main(String[] args) {//myIsam  不支持事务  用InnoDB 引擎

        //可能出现  时区问题 serverTimezone=UTC  添加时区
        BankService2 bankService = context.getBean("bankService2", BankService2.class);
        System.out.println(bankService.getClass());
        System.out.println(bankService.queryAll());
        bankService.accountMoney(new BigDecimal(100.0),false);//false  正常执行   true:模拟异常回滚
        System.out.println(bankService.queryAll());

    }
}
