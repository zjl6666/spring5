package com.zjl.spring.第06章_事务;

import com.zjl.spring.第06章_事务.spring.BankService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.math.BigDecimal;

/**
 * 1、什么事务
 *   （1）事务是数据库操作最基本单元，逻辑上一组操作，
 *       要么都成功，如果有一个失败所有操作都失败
 *  2、事务四个特性（ACID）
 *      （1）原子性
 *      （2）一致性
 *      （3）隔离性
 *      （4）持久性
 *
 *  JavaEE
 *     WEB层
 *     Service层  业务操作   一般事务加在这里
 *     Dao层      数据库操作
 *
 *     见 JdbcTemplateDemo这个包
 *
 * 1、事务添加到 JavaEE 三层结构里面 Service 层（业务逻辑层）
 * 2、在 Spring 进行事务管理操作
 * （1）有两种方式：编程式事务管理和声明式事务管理（使用）
 * 3、声明式事务管理
 * （1）基于注解方式（使用）
 * （2）基于 xml 配置文件方式
 * 4、在 Spring 进行声明式事务管理，底层使用 AOP 原理  AOP(不改变源代码增加功能)
 * 5、Spring 事务管理 API
 * （1）提供一个接口，代表事务管理器，这个接口针对不同的框架提供不同的实现类
 *      PlatformTransactionManager
 *
 */

/**
 * 事务操作
 * 1.在 spring 配置文件配置事务管理器 注解事务.xml  创建事务管理器
 * 2、在 spring 配置文件，开启事务注解
 *   （1）在 spring 配置文件引入名称空间 tx
 *   （2）开启事务注解
 * 3、在 service 类上面（或者 service 类里面方法上面）添加事务注解
 * （1）@Transactional，这个注解添加到类上面，也可以添加方法上面
 * （2）如果把这个注解添加类上面，这个类里面所有的方法都添加事务
 * （3）如果把这个注解添加方法上面，为这个方法添加事务
 *
 */
public class 事务配置 {
//    开启事务注解 以注解形式做事务      会与注解方式开始事务  产生冲突！！！！！！！！！！！！！！！！！！！！！！
//    即@EnableTransactionManagement//开启事务！！！！！！！！！！！！！！！！！！！！！！！！！
    static ApplicationContext context =
//          new FileSystemXmlApplicationContext("src/main/resources/注解事务.xml");//起始目录是本工程的目录
            new ClassPathXmlApplicationContext("注解事务.xml");//注解方式  完成事务  需要BankService有事务注解
//            new ClassPathXmlApplicationContext("xml事务.xml");//xml方式 完成事务  需要把冲突的去掉

    public static void main(String[] args) {//myIsam  不支持事务  用InnoDB 引擎

        //可能出现  时区问题 serverTimezone=UTC  添加时区
        BankService bankService = context.getBean("bankService", BankService.class);
        System.out.println(bankService.queryAll());
        bankService.accountMoney(new BigDecimal(100.0),false);//false  正常执行   true:模拟异常回滚
        System.out.println(bankService.queryAll());
        System.out.println("********************************************");
        System.out.println(bankService.queryBank());
    }

}



