package com.zjl.spring.第04章_AOP;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class D_AOP操作_基于AspectJ配置文件 {
    //注解的增强与配置文件的增强   顺序不一样
    static ApplicationContext context = new ClassPathXmlApplicationContext("AOP.xml");

    public static void main(String[] args) {
        AspectJ_xml主 aspectJXml主 = context.getBean("AspectJ_xml主", AspectJ_xml主.class);
        aspectJXml主.get();
        System.out.println("****************接口继承了被代理的接口******");
        Class<?>[] interfaces = aspectJXml主.getClass().getInterfaces();//获取实现的接口
        for (Class<?> in:interfaces){
            //interface org.springframework.aop.SpringProxy
            //interface org.springframework.aop.framework.Advised
            //interface org.springframework.cglib.proxy.Factory
            System.out.println(in);
        }
        System.out.println("********获取的已不是原来单纯的类****************");
        //查看class  发现这个类是被改变过得，不是单纯的 AspectJ_主
        System.out.println(aspectJXml主.getClass());//class com.zjl.spring.第04章_AOP.AspectJ_xml主$$SpringCGLIB$$0
    }
}

class AspectJ_xml主{
    public void get(){
        System.out.println("主方法");
    }

}

class AspectJ_xml副{
    public void getA(){
        System.out.println("getA");
    }
    public void getB(){
        System.out.println("getB");
    }
    public void getC(){
        System.out.println("getC");
    }
    public void getD(){
        System.out.println("getD");
    }
    public void getE(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("getE1");
        proceedingJoinPoint.proceed();//主方法(包括给他增强的方法)
        System.out.println("getE2");
    }

}
