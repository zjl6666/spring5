package com.zjl.spring.第04章_AOP;

/**
 *
 * 什么是AOP？
 * 面向切面编程（AOP）完善spring的依赖注入（DI），面向切面编程在spring中主要表现为两个方面
 * 1.面向切面编程提供声明式事务管理
 * 2.spring支持用户自定义的切面
 *
 * AOP 可以对业务逻辑的各个部分进行隔离，从而使得业务逻辑各部分之间的耦合度降低，
 * 提高程序的可重用性，同时提高了开发的效率。
 * 通俗描述：不通过修改源代码方式，在主干功能里面添加新功能  类似加一个配置
 *
 * 面向切面编程（aop）是对面向对象编程（oop）的补充，
 *  面向对象编程将程序分解成各个层次的对象，面向切面编程将程序运行过程分解成各个切面。
 *  AOP从程序运行角度考虑程序的结构，提取业务处理过程的切面，oop是静态的抽象，aop是动态的抽象，
 *  是对应用执行过程中的步骤进行抽象，，从而获得步骤之间的逻辑划分。
 *
 * aop框架具有的两个特征：
 * 1.各个步骤之间的良好隔离性
 * 2.源代码无关性
 *
 * Spring的事务管理机制实现的原理，就是通过这样一个动态代理对所有需要事务管理的Bean进行加载，
 * 并根据配置在invoke方法中对当前调用的 方法名进行判定，
 * 并在method.invoke方法前后为其加上合适的事务管理代码，
 * 这样就实现了Spring式的事务管理。
 * Spring中的AOP实 现更为复杂和灵活，不过基本原理是一致的。
 *
 * AOP解析
 * AOP(Aspect-Oriented Programming:面向切面编程)
 * 能够将那些与业务无关，却为业务模块所共同调用的逻辑或责任
 * （例如事务处理、日志管理、权限控制等）封装起来，
 * 便于减少系统的重复代码，降低模块间的耦合度，并有利于未来的可拓展性和可维护性。
 *
 *
 * 使用 AOP 之后我们可以把一些通用功能抽象出来，在需要用到的地方直接使用即可，
 * 这样大大简化了代码量。我们需要增加新功能时也方便，这样也提高了系统扩展性。
 * 日志功能、事务管理等等场景都用到了 AOP 。
 *
 */
public class A_概念 {
}
