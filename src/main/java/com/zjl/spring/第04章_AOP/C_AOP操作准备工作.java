package com.zjl.spring.第04章_AOP;
/**
 * AOP术语:
 *  1.连接点
 *      类里面哪些方法可以被增强，这些方法成为连接点
 *  2.切入点
 *      实际被增强的方法叫切入点
 *  3.通知(增强)
 *      增强的逻辑部分成为通知(增强)
 *      通知(增强)的多种类型
 *          前置通知，后置通知，环绕通知，异常通知，最终通知(finally)
 *  4.切面   是一个动作
 *      把通知应用到切入点的过程
 *
 *
 * ●动态代理分为JDK动态代理和cglib动态代理
 * ●当目标类有接口的情况使用JDK动态代理和cglib动态代理,没有接口时只能使用cglib动态代理(JDK动态代理 必须有接口)
 * ●JDK动态代理动态生成的代理类会在com.sun.proxy包下，类名为$proxy1, 和目标类实现相同的接口
 * ●cglib动态代理动态生成的代理类会和目标在在相同的包下，会继承目标类
 * ●动态代理(InvocationHandler) : JDK原生的实现方式,需要被代理的目标类必须实现接口。
 *  因为这个技术要求代理对象和目标对象实现同样的接口(兄弟两个拜把子模式)。
 * ●cglib: 通过继承被代理的目标类(认干爹模式)实现代理，所以不需要目标类实现接口。
 * ●Aspecty: 是AOP思想的一种实现。本质上是静态代理，
 *  将代理逻辑”织入”被代理的目标类编译得到的字节码文件，所以最终效果是动态的。
 *  weaver就是织入器。Spring只是借用 了AspectJ中的注解。
 *
 */
/**
 * AOP 操作（准备工作）
 * 1、Spring 框架一般都是基于 AspectJ 实现 AOP 操作 （AspectJ 本质是静态代理吗）
 *      （1）AspectJ 不是 Spring 组成部分，独立 AOP 框架，
 *      一般把 AspectJ 和 Spring 框架一起使用，进行 AOP 操作
 * 2、基于 AspectJ 实现 AOP 操作
 *      （1）基于 xml 配置文件实现
 *      （2）基于注解方式实现（使用）
 * 3、在项目工程里面引入 AOP 相关依赖
 * 4、切入点表达式
 * （1）切入点表达式作用：知道对哪个类里面的哪个方法进行增强
 * （2）语法结构： execution([权限修饰符] [返回类型] [类全路径] [方法名称]([参数列表]) )
 * 举例 1：对 com.atguigu.dao.BookDao 类里面的 add 进行增强
 * execution(* com.atguigu.dao.BookDao.add(..))
 * 举例 2：对 com.atguigu.dao.BookDao 类里面的所有的方法进行增强
 * execution(* com.atguigu.dao.BookDao.* (..))
 * 举例 3：对 com.atguigu.dao 包里面所有类，类里面所有方法进行增强
 * execution(* com.atguigu.dao.*.* (..))
 *
 */
public class C_AOP操作准备工作 {
}
