package com.zjl.spring.第01章_概述;

/**
 * Spring地址 https://spring.io/
 * Spring是一款主流的Java EE轻量级开源框架,
 * Spring 由"Spring之父"Rod Johnson提出并创立，
 * 其目的是用于简化Java企业级应用的开发难度和开发周期。
 * Spring的用途不仅限于服务器端的开发。
 * 从简单性、可测试性和松耦合的角度而言，任何ava应用都可以从Spring中受益。
 * Spring 框架除了自己提供功能外,还提供整合其他技术和框架的能力。
 *
 * Spring自诞生以来备受青睐，一直被广大开发人员作为Java企业级应用程序开发的首选。
 * 时至今日，Spring 俨然成为了Java EE代名词，成为了构建Java EE应用的事实标准。
 * 自2004年4月, Spring 1.0版本正式发布以来,
 * Spring 已经步入到了第6个大版本,也就是Spring 6。
 * 本课程采用Spring当前最新发布的正式版本6.0.2。
 *
 */
public class A_Spring是什么 {
}
