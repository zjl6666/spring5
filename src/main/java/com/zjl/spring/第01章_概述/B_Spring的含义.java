package com.zjl.spring.第01章_概述;

/**
 * 在不同的语境中，Spring 所代表的含义是不同的。
 * 下面我们就分别从"广义”和”狭义”两个角度，对Spring进行介绍。
 *
 * 广义的Spring: Spring 技术栈
 * 广义上的Spring泛指以Spring Framework为核心的Spring技术栈。
 * 经过十多年的发展，Spring 已经不再是一个单纯的应用框架，而是逐渐发展成为一个由多个不同子项目(模块)
 * 组成的成熟技术，例如
 * Spring Framework、Spring MVC、SpringBoot。Spring Cloud、Spring Data、Spring Security等，
 * 其中Spring Framework是其他子项目的基础。
 *
 * 这些子项目涵盖了从企业级应用开发到云计算等各方面的内容,能够帮助开发人员解决软件发展过程中不断产生的
 * 各种实际问题，给开发人员带来了更好的开发体验。
 *
 *
 * 狭义的Spring: Spring Framework
 * 狭义的Spring特指Spring Framework,通常我们将它称为Spring框架。
 * Spring框架是一个分层的、面向切面的Java应用程序的一站式轻量级解决方案，
 * 它是Spring技术栈的核心和基础，是为了解决企业级应用开发的复杂性而创建的。
 * Spring有两个最核心模块: IOC 和AOP。
 * IOC: Inverse of Control的简写,译为"控制反转”，指把创建对象过程交给Spring进行管理。
 * AOP: Aspect Oriented Programming的简写，译为"面向切面编程”。
 *      AOP用来封装多个类的公共行为,将那些与业务无关，
 *      却为业务模块所共同调用的逻辑封装起来,减少系统的重复代码，降低模块间的耦合度。
 *      另外,AOP还解决一些系统层面上的问题，比如日志、事务、权限等。
 *
 *
 *
 *
 */
public class B_Spring的含义 {
}
