package com.zjl.spring.第10章_提前编译AOT;

/**
 * 目前业界除了这种在JVM中进行AOT的方案，还有另外一种实现Java AOT的思路，
 * 那就是直接摒弃JVM,和C/C++一样通过编译器直接将代码编译成机器代码，然后运行。
 * 这无疑是一种直接颠覆Java语言 设计的思路,那就是GraalVM Native Image。
 * 它通过C语言实现了一个超微缩的运行时组件    Substrate VM ,
 * 基本实现了JVM的各种特性，但足够轻量、可以被轻松内嵌，这就让Java语言和工程摆脱JVM的限制，
 * 能够真正意义上实现和C/C++一样的AOT编译。
 * 这一方案在经过长时间的优化和积累后，已经拥有非常不错的效果，
 * 基本上成为Oracle官方首推的Java AOT解决方案。
 *
 * Native Image是一项创新技术，可将Java代码编译成独立的本机可执行文件或本机共享库。
 * 在构建本机可执行文件期间处理的Java字节码包括所有应用程序类、依赖项、第三方依赖库和任何所需的JDK类。
 * 生成的自包含本机可执行文件特定于不需要JVM的每个单独的操作系统和机器体系结构。
 *
 */
public class C_Native_Image {
    public static void main(String[] args) {

    }
}
