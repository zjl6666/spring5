package com.zjl.spring.第10章_提前编译AOT;

/**
 * java  是 Java Hotspot 编译器
 * 新出的 GraalVM 编译器
 *
 * Spring6支持的AOT技术，这个GraalVM就是底层的支持，Spring 也对GraalVM本机映像提供了一流的支持。
 * GraalVM是一种高性能JDK，旨在加速用Java和其他JVM语言编写的应用程序的执行，
 * 同时还为JavaScript、Python和许多其他流行语言提供运行时。
 * GraalVM 提供两种运行Java应用程序的方法:
 *          在HotSpot jVM 上使用Graal即时(JIT)编译器或作为提前(AOT)编译的本机可执行文件。
 *          GraalVM 的多语言能力使得在单个应用程序中混合多种编程语言成为可能，同时消除了外语调用成本。
 *          GraalVM 向HotSpot Java虚拟机添加了一个用Java编写的高级即时(JIT) 优化编译器。
 *
 * GraalVM具有以下特性:
 * (1)一种高级优化编译器，它生成更快、更精简的代码，需要更少的计算资源
 * (2) AOT本机图像编译提前将Java应用程序编译为本机二进制文件，立即启动，无需预热即可实现最高性能
 * (3) Polyglot 编程在单个应用程序中利用流行语言的最佳功能和库，无需额外开销
 * (4) 高级工具在Java和多种语言中调试、监视、分析和优化资源消耗
 *     总的来说对云原生的要求不算高短期内可以继续使用2.7.X的版本和JDK8,
 *     不过Spring官方已经对Spring6进行了正式版发布。
 *
 */
public class B_GraalVM编译器 {
}
