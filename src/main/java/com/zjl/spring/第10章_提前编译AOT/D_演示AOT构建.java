package com.zjl.spring.第10章_提前编译AOT;

/**
 * 要提前有 JDK
 * Windows：
 * 1.安装  GraalVM 编译器
 *      下载地址 ：https://www.graalvm.org/downloads
 *      环境变量：GRAALVN_HOME     解压的地址如  D:\work\graalvm\graalvm-jdk-17.0.12+8.1
 *      膝盖 JAVA_HOME 为 同上地址
 *      在Path  中的 jdk\bin  改为GraalVM 编译器 如 D:\work\graalvm\graalvm-jdk-17.0.12+8.1\bin
 *
 *      这时候 java -version 的编译器就改变了
 * 2.给GraalVM安装  Native Image 插件
 *      安装命令 ： gu install native-image
 *      查看命令 : gu list
 *
 * 2.安装c++ 环境
 * https://www.bilibili.com/video/BV1kR4y1b7Qc/?p=86
 */
public class D_演示AOT构建 {
    public static void main(String[] args) {

    }
}
