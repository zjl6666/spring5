package com.zjl.spring.第09章_数据校验Validation.E_自定义校验;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

/**
 * ClassName: MyValidator
 * Package: com.zjl.spring.第09章_数据校验Validation.E_自定义校验
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/21 20:04
 * @Version 1.0
 */
@Constraint(
        //MyValidatorAchieve  这个类  必须是 public 的类
        validatedBy = {MyValidatorAchieve.class}
)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(MyValidator.List.class)
@Documented
public @interface MyValidator {
    //默认错误信息
//    String message() default "{不能包含指定字符}";
    //可以再 ValidationMessages.properties 文件中指定映射关系
    String message() default "{com.Validator.xxx.message}";

    //分组  可以指定分组，指定后，只有指定的分组生效
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface List {
        MyValidator[] value();
    }
}
