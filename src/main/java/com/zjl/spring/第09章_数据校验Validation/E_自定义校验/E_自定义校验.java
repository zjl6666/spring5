package com.zjl.spring.第09章_数据校验Validation.E_自定义校验;

import jakarta.validation.*;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName: E_自定义校验
 * Package: com.zjl.spring.第09章_数据校验Validation
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/21 19:00
 * @Version 1.0
 */
@Configuration//配置类
@ComponentScan(basePackages = {
        "com.zjl.spring.第09章_数据校验Validation.E_自定义校验",
})//组件扫描
public class E_自定义校验 {
    public static   void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(E_自定义校验.class);

        MyValidatorBean methodValidator = new MyValidatorBean();
        methodValidator.setTest("8548 54856");
        //获取校验器
        MyValidator3 bean = context.getBean(MyValidator3.class);
//        xxx x = context.getBean(xxx.class);
//        x.setx(methodValidator);
        //会直接抛错  内部有自己的校验 错误，就不会了
        String s = bean.MyValidator3(methodValidator);//执行校验


        Map<String,Object> m = new HashMap<>();
        m.containsKey("xx");
        List<Object> objects = new ArrayList<>();
        objects.contains("xx");
        System.out.println(s);
    }

//    @Bean //自动注入  基于方法实现校验类   spring boot 运行时，需要注释掉，否则会
    //is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying).
    // The currently created BeanPostProcessor [getMethodValidationAdapter] is declared through a non-static factory method on that class; consider declaring it as static instead.
    public MethodValidationPostProcessor getMethodValidationAdapter(){
        return new MethodValidationPostProcessor();
    }

}
