package com.zjl.spring.第09章_数据校验Validation.E_自定义校验;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

/**
 * ClassName: MyValidator3
 * Package: com.zjl.spring.第09章_数据校验Validation.E_自定义校验
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/21 20:04
 * @Version 1.0
 */
@Service
@Validated
public class MyValidator3 {
    public String MyValidator3(@Valid @NotNull MyValidatorBean myValidatorBean) {
        return myValidatorBean.toString();
    }
}
