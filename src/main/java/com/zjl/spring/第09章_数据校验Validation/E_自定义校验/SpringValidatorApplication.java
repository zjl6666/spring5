package com.zjl.spring.第09章_数据校验Validation.E_自定义校验;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

//exclude = DataSourceAutoConfiguration.class   和关闭数据库的配置加载
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
@SpringBootConfiguration
@ComponentScan("com.zjl.spring.第09章_数据校验Validation.E_自定义校验")
//@SpringBootApplication()
public class SpringValidatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringValidatorApplication.class, args);
    }

}
