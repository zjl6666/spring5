package com.zjl.spring.第09章_数据校验Validation.E_自定义校验;

import jakarta.validation.Valid;
import org.springframework.core.MethodParameter;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
public class RequestValidatorDemo {
    @RequestMapping("ValidatorDemo1")
    public String validatorDemo1(@Validated @RequestBody MyValidatorBean myValidatorBean)  {// @Validated 面向接口 抛错

        System.out.println(myValidatorBean);
        return myValidatorBean.toString();
    }
    @RequestMapping("ValidatorDemo2")
    public String validatorDemo2(@Valid @RequestBody MyValidatorBean myValidatorBean)  {// @Validated 面向接口 抛错

        System.out.println(myValidatorBean);
        return myValidatorBean.toString();
    }
}
