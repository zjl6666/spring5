package com.zjl.spring.第09章_数据校验Validation.E_自定义校验;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * 必须在 是 public
 * 否则 会抛错
 *
 * HV000064: Unable to instantiate ConstraintValidator: com.zjl.spring.第09章_数据校验Validation.E_自定义校验.MyValidatorAchieve.
 */
public class MyValidatorAchieve implements ConstraintValidator<MyValidator, String> {

//    @Override
//    public void initialize(MyValidator myValidator) {
//        ConstraintValidator.super.initialize(myValidator);
//    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value != null && value.contains(" ")) {
            //获取默认提示信息
            String defaultConstraintMessageTemplate = context.getDefaultConstraintMessageTemplate();
            System.out.println("原提示语" + defaultConstraintMessageTemplate);
//            context.disableDefaultConstraintViolation();//禁用默认提示信息
//
//            //设置自己的提示信息
//            context.buildConstraintViolationWithTemplate("不能出现空格1")
//                    .addConstraintViolation();

            return false;
        }
        return true;
    }
}
