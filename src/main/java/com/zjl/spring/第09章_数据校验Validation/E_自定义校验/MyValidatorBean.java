package com.zjl.spring.第09章_数据校验Validation.E_自定义校验;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ClassName: MyValidatorBean
 * Package: com.zjl.spring.第09章_数据校验Validation.E_自定义校验
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/21 20:04
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MyValidatorBean {

    @MyValidator//自定义校验
    private String test;

    @MyValidator//自定义校验
    private String test2;

    @Valid//嵌套对象也需要加  @Valid    集合对象，同样需要添加@Valid注解，
    private MyValidatorBean bean;
}
