package com.zjl.spring.第09章_数据校验Validation;

/**
 * ClassName: ValidatorBean
 * Package: com.zjl.spring.第09章_数据校验Validation
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/21 17:34
 * @Version 1.0
 */
public class ValidatorBean {
    private String name;

    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


}
