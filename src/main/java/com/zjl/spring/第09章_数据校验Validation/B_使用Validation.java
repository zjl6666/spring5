package com.zjl.spring.第09章_数据校验Validation;

import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;

/**
 * ClassName: B_使用Validation
 * Package: com.zjl.spring.第09章_数据校验Validation
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/21 17:44
 * @Version 1.0
 */
public class B_使用Validation {
    public static void main(String[] args) {
        //创建对象
        ValidatorBean validatorBean = new ValidatorBean();
        validatorBean.setName(null);
        validatorBean.setAge(201);

        //常见此对象的校验类
        DataBinder binder = new DataBinder(validatorBean);

        //放入对应的校验器
        binder.setValidator(new ValidatorBeanValidator());

        binder.validate();//执行校验

        //获取校验结果
        BindingResult bindingResult = binder.getBindingResult();
        System.out.println(bindingResult);

    }
}
