package com.zjl.spring.第09章_数据校验Validation;

/**
 * 在开发中，我们经常遇到参数校验的需求,比如用户注册的时候，
 * 要校验用户名不能为空、用户名长度不超过20个字符、手机号是合法的手机号格式等等。
 * 如果使用普通方式，我们会把校验的代码和真正的业务处理逻辑耦合在一起，
 * 而且如果未来要新增一种校验逻辑也需要在修改多个地方。
 * 而spring validation允许通过注解的方式来定义对象校验规则，把校验和业务逻辑分离开,让代码编写更加方便。
 * Spring Validation其实就是对Hibernate Validator 进一步的封装，方便在Spring中使用。
 *
 * 在Spring中有多种校验的方式
 * 第一种是通过实现 org.springframework.validation.Validator 接口,然后在代码中调用这个类
 * 第二种是按照 Bean Validation 方式来进行校验，即通过注解的方式。
 * 第三种是基于方法实现校验
 * 除此之外，还可以实现自定义校验
 *
 * 需要引入 校验依赖
 *
 *         <dependency>
 *             <groupId>org.hibernate.validator</groupId>
 *             <artifactId>hibernate-validator</artifactId>
 *             <version>8.0.1.Final</version>
 *         </dependency>
 *         <dependency>
 *             <groupId>org.glassfish</groupId>
 *             <artifactId>jakarta.el</artifactId>
 *             <version>4.0.1</version>
 *         </dependency>
 *
 */
public class A_概述 {
}
