package com.zjl.spring.第02章_log4j2;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * logback.xml
 */
public class A_log4j2 {
    private static final Logger log = LoggerFactory.getLogger(A_log4j2.class);

    @Test
    public void Log(){
        log.info("xxx{}",5);
        System.out.println("--------------");
    }
}
