package com.zjl.spring.第03章_IOC.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

//@ConfigurationProperties(prefix = "person")
@Component
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class Person {

    private String userName;
    private Boolean boss;
    private Date birth;
    private Integer age;
    private Pet pet;
    private String[] interests;
    private List<String> animal;
    private Map<String, Object> score;
    private Set<Double> salary;
    private Map<String, List<Pet>> allPets;

    public Person(String[] interests, List<String> animal, Map<String, Object> score, Set<Double> salary) {
        this.interests = interests;
        this.animal = animal;
        this.score = score;
        this.salary = salary;
    }
}