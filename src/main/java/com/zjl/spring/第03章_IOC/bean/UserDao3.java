package com.zjl.spring.第03章_IOC.bean;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository//为 开启组件解扫描  的使用
@Component//为 开启组件解扫描  的使用
@Service//为 开启组件解扫描  的使用
@Controller//为 开启组件解扫描  的使用
public class UserDao3 {

    private String userName;

    public UserDao3(String userName) {
        this.userName = userName;
    }

    public UserDao3() {}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "UserDao3{" +
                "userName='" + userName + '\'' +
                '}';
    }

}
