package com.zjl.spring.第03章_IOC.bean;

import lombok.Data;
import lombok.ToString;

@Data
//@NoArgsConstructor
@ToString
//@AllArgsConstructor
public class User {
    private String name;
    private Integer age;
    private Pet pet;

}
