package com.zjl.spring.第03章_IOC.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository//为 开启组件解扫描  的使用,也可以自定义名称  默认类第一个字母小写
@Component//为 开启组件解扫描  的使用
@Service//为 开启组件解扫描  的使用
@Controller//为 开启组件解扫描  的使用
public class UserDao {

    private String userName;

    public UserDao(String userName) {
        this.userName = userName;
    }

    public UserDao() {}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "UserDao{" +
                "userName='" + userName + '\'' +
                '}';
    }

    public void add() {
        System.out.println(this.userName);
    }
}
