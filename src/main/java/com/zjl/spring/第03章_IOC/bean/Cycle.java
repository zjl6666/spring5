package com.zjl.spring.第03章_IOC.bean;

public class Cycle {
    private String name;
    public Cycle() {
        System.out.println("1.通过构造器创建 bean 实例（无参数构造）");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println("2.为 bean 的属性设置值和对其他 bean 引用（调用 set 方法）");
        this.name = name;
    }
    public void initMethod(){
        System.out.println("3.调用 bean 的初始化的方法（需要进行配置初始化的方法）");

    }
    public void destroyMethod(){
        System.out.println("5.当容器关闭时候，调用 bean 的销毁的方法（需要进行配置销毁的方法）");
    }

    @Override
    public String toString() {
        return "Cycle{" +
                "name='" + name + '\'' +
                '}';
    }
}
