package com.zjl.spring.第03章_IOC;

import com.zjl.spring.第03章_IOC.bean.UserDao;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 1、什么是注解
 * （1）注解是代码特殊标记，格式：@注解名称(属性名称=属性值, 属性名称=属性值..)
 *
 * （3）使用注解目的：简化 xml 配置
 * 2、Spring 针对 Bean 管理中创建对象提供注解
 * （1）@Component    组成部分   开启控制器（必须） 在工具类上
 * （2）@Service      服务                     接口的实现类上
 * （3）@Controller   控制器                   在控制器(请求路径上)
 * （4）@Repository   存储库     已不用（原来是写在有sql语句里的）
 *                             现在用@Mapper替代  来接受 xml的 sql
 * * 上面四个注解功能是一样的，都可以用来创建 bean 实例
 *
 * 有个AOP的包
 * 1.注解需要依赖aop的包
 * 2.需要在xml里面引入名称空间
 *               xmlns:context="http://www.springframework.org/schema/context"
 *               http://www.springframework.org/schema/context
 *               http://www.springframework.org/schema/context/spring-context.xsd
 * 3.开启组件扫描    扫描所有注解
 *           1.
 *      <context:component-scan base-package="com.zjl.spring"/>
 *                 表示扫描这个包  可以  ,  隔开   也可以写大点
 *           2.
 *       <context:component-scan base-package="包" use-default-filters="false">  //不全部扫描
 *              <context:include-filter type="annotation"
 *          expression="org.springframework.stereotype.Controller"/>
 *             只扫描指定的 Controller这个注解
 *      </context:component-scan>
 *
 *          3.
 *      <context:component-scan base-package="包">
 *               <context:exclude-filter type="annotation"  //不扫描指定的
 *              expression="org.springframework.stereotype.Controller"/>
 *              只不扫描指定的 Controller这个注解
 *      </context:component-scan>
 *
 * 之后才能用
 *
 *
 */
public class C1_注解创建Bean {


    @Test
    public void  IOC注解注入() {
        /**
         * @see UserDao
         */
        //根据 IOC注解配置文件混用.xml 的配置的包路径，自己找 某些注解，自动注入（必须有空惨构造器）
        ApplicationContext a = new ClassPathXmlApplicationContext("IOC注解配置文件混用.xml");
        UserDao userDao = a.getBean("userDao", UserDao.class);
        System.out.println(userDao);


    }
}
