package com.zjl.spring.第03章_IOC;

import com.zjl.spring.第03章_IOC.bean.UserDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class B3_工厂Bean {
    @Test
    public void 工厂Bean() {
        /**
         * IOC 操作 Bean 管理（FactoryBean）
         * 1、Spring 有两种类型 bean，一种普通 bean，另外一种工厂 bean（FactoryBean）
         * 2、普通 bean：在配置文件中定义 bean 类型就是返回类型
         * 3、工厂 bean：在配置文件定义 bean 类型可以和返回类型不一样
         *   第一步 创建类，让这个类作为工厂 bean，实现接口 FactoryBean
         *   第二步 实现接口里面的方法，在实现的方法中定义返回的 bean 类型
         *
         * 工厂 bean
         */
        ApplicationContext a2 = new ClassPathXmlApplicationContext("IOC1.xml");

        UserDao mybean = a2.getBean("myBean", UserDao.class);//工厂 bean
        mybean.add();

    }

}

//工厂bean
class MyBean implements FactoryBean<UserDao> { //设置了MyBean返回的对象是UserDao对象

    @Override //定义返回类型
    public UserDao getObject() throws Exception {
        return new UserDao("工厂bean注入");
    }

    @Override
    public Class<?> getObjectType() {
        return null;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}
