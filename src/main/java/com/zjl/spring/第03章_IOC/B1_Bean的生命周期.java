package com.zjl.spring.第03章_IOC;

import com.zjl.spring.第03章_IOC.bean.Cycle;
import org.junit.jupiter.api.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 1、生命周期
 * （1）从对象创建到对象销毁的过程
 * 2、bean 生命周期
 * （1）通过构造器创建 bean 实例（无参数构造）
 * （2）为 bean 的属性设置值和对其他 bean 引用（调用 set 方法）
 *
 * （2.5）把bean实例传递bean后置处理器的方法。
 *       postProcessBeforeInitialization
 *
 * （3）调用 bean 的初始化的方法（需要进行配置初始化的方法 放入容器中）
 *
 * （3.5）把bean实例传递bean后置处理器的方法，
 *       postProcessAfterInitialization
 *
 * （4）bean 可以使用了（对象获取到了）
 * （5）当容器关闭时候，调用 bean 的销毁的方法（需要进行配置销毁的方法）
 *
 * 1.Spring启动，查找并加载需要被Spring管理的bean，进行Bean的实例化
 * 2.Bean实例化后对将Bean的引入和值注入到Bean的属性中
 * 3.如果Bean实现了BeanNameAware接口的话，
 *      Spring将Bean的Id传递给setBeanName()方法
 * 4.如果Bean实现了BeanFactoryAware接口的话，
 *      Spring将调用setBeanFactory()方法，将BeanFactory容器实例传入
 * 5.如果Bean实现了ApplicationContextAware接口的话，
 *      Spring将调用Bean的setApplicationContext()方法，将bean所在应用上下文引用传入进来。
 * 6.如果Bean实现了BeanPostProcessor接口，
 *      Spring就将调用他们的postProcessBeforeInitialization()方法。
 * 7.如果Bean 实现了InitializingBean接口，
 *      Spring将调用他们的afterPropertiesSet()方法。
 *      类似的，如果bean使用init-method声明了初始化方法，该方法也会被调用
 * 8.如果Bean 实现了BeanPostProcessor接口，
 *      Spring就将调用他们的postProcessAfterInitialization()方法。
 * 9.此时，Bean已经准备就绪，可以被应用程序使用了。
 *      他们将一直驻留在应用上下文中，直到应用上下文被销毁。
 * 10.如果bean实现了DisposableBean接口，Spring将调用它的destory()接口方法，
 *      同样，如果bean使用了destory-method 声明销毁方法，该方法也会被调用。
 *
 */
public class B1_Bean的生命周期 {

    @Test
    public void Bean的生命周期() {
        //        ApplicationContext context = new ClassPathXmlApplicationContext("IOC2.xml");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("IOC之Bean的生命周期.xml");
        Cycle cycle = context.getBean("cycle", Cycle.class);
        System.out.println("4.bean 可以使用了（对象获取到了）");
        System.out.println(cycle);

        context.close();
        //只有ClassPathXmlApplicationContext  有销毁的方法
    }
}
