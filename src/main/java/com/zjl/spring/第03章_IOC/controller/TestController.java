package com.zjl.spring.第03章_IOC.controller;

import com.zjl.spring.第03章_IOC.C1_注解创建Bean;
import com.zjl.spring.第03章_IOC.bean.UserDao;
import com.zjl.spring.第03章_IOC.bean.UserDao2;
import com.zjl.spring.第03章_IOC.bean.UserDao3;
import com.zjl.spring.第03章_IOC.bean.UserDao4;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Controller
/**
 * @Scope 注解  同 IOC2.xml 的一样
 *  scope= "singleton" 默认  单实例
 *          prototype       多实例
 *          request     请求作用域
 *          session     会话作用域
 */
//@Scope("prototype")
public class TestController {

    public UserDao4 userDao4;

    public UserDao3 userDao3;
//    @Autowired//构造方法注入   会自动注入，不写 @Autowired 也会注入,
//    因为只有一个构造器，默认不用加
    public TestController(UserDao3 userDao3,@Autowired UserDao4 userDao4){ //形参方法注入

        this.userDao3 = userDao3;
        this.userDao4 = userDao4;
    }

    /**
     * @Resource 注解也可以完成属性注入。那它和@Autowired注解有什么区别?
     * ●@Resource注解是IDK扩展包中的,也就是说属于JDK的一部分。所以该注解是标准注解，更加具有通用性。
     * USR-250标准中制定的注解类型。JSR是Java规范提案。
     * ●@Autowired注解是Spring框架自己的。
     * ●@Resource注解默认根据名称装配byName,未指定name时，使用属性名作为name。通过name找不到
     * 的话会自动启动通过类型byType装配。
     * ●@Autowired注解默认根据类型装配byType,如果想根据名称装配，需要配合@Qualifier注解- 起用。
     * ●@Resource注解用在属性上、setter方法. 上。
     * ●@Autowired注解用在属性上、setter方法上、 构造方法.上、构造方法参数上。
     * @Resource 注解属于JDK扩展包，所以不在JDK当中，需要额外引入以下依赖:
     *      [如果是JDK8的话不需要额外引入依赖。高于JDK11或低于JDK8需要引入以下依赖。]
     */
//    @Autowired //根据属性类型进行自动装配  不用new  默认 类的第一个字母小写
//    @Qualifier("userDao") // 比如这是个接口  没法找具体实现类（他有多个实现类） 可以指定名称 必须与上一个一起使用
    @Resource  //Autowired和Qualifier的结合  加括号 也可以写名称 他是javax包里的 jdk17在jakarta中，因为他需要额外引入了
    UserDao userDao;


    @Value("666")
    private Integer integer;
    @Value(value = "999")
    private String string;

    public UserDao getUserDao() {
        return userDao;
    }
    public Integer getInteger() {
        return integer;
    }
    public String getString() {
        return string;
    }

    public UserDao2 userDao2;

    @Autowired //set 方法注入
    public void setUserDao2(UserDao2 userDao2) {
        this.userDao2 = userDao2;
    }
    public UserDao2 getUserDao2() {
        return userDao2;
    }
}
