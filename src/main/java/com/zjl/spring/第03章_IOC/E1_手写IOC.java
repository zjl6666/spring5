package com.zjl.spring.第03章_IOC;

import org.springframework.context.annotation.ComponentScan;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;


/**
 * 使用反射手写IOC
 *
 */
public class E1_手写IOC {
    public static void main(String[] args) throws Exception {

        //获取 对象class
        Class<?> class4 = ClassLoader.getSystemClassLoader().loadClass("com.zjl.spring.第03章_IOC.D_完全注解管理Bean");
        Constructor<?> constructor = class4.getDeclaredConstructor();//获取构造器
        Object o1 = constructor.newInstance();//调用无参构造器

        Annotation[] annotations = class4.getAnnotations();//获取对象 class 的所有注解
        for (Annotation a:annotations){
            Class<? extends Annotation> x = a.annotationType();//获取注解类型
            Method value = x.getDeclaredMethod("value");//获取指定注解方法
            Object invoke = value.invoke(a);//调用制定方法
            if(invoke instanceof String[] ss){
                for (String s:ss){
                    System.out.println(s);
                }
            }else{
                System.out.println(invoke);
            }

        }//遍历他的注释
        System.out.println("----------------------------------------------");

        Class<?> class3 = Class.forName("com.zjl.spring.第03章_IOC.bean.MyBeanPost");
        //获取指定class 的构造器  getDeclaredConstructor()  所有包括私有            getConstructors()  只获取  public
        Constructor<?> declaredConstructor = class3.getDeclaredConstructor();

        //根据无参构造器获取类
        Object o = declaredConstructor.newInstance();

        //获取指定名字的方法
        Method postProcessBeforeInitialization = class3.getDeclaredMethod("postProcessBeforeInitialization", Object.class, String.class);
        //调用方法
        Object invoke = postProcessBeforeInitialization.invoke(o, "66", "99");

        //返回值
        System.out.println(invoke);

    }

}
