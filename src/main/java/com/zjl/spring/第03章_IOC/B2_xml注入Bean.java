package com.zjl.spring.第03章_IOC;

import com.zjl.spring.第03章_IOC.bean.UserDao;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 1、在 Spring 里面，设置创建 bean 实例是单实例还是多实例
 * 2、在 Spring 里面，默认情况下，bean 是单实例对象
 *
 */
public class B2_xml注入Bean {


    @Test
    public void  IOC依赖注入调用() {

        /**
         * 2、Bean 管理操作有两种方式
         *  （1）基于 xml 配置文件方式实现
         *  （2）基于注解方式实现
         * 普通 bean
         *
         * 基于 xml 方式注入属性
         *   什么是DI机制？
         *   依赖注入（Dependency Injection）和控制反转（Inversion of Control）是同一个概念，
         *   具体的讲：当某个角色需要另外一个角色协助的时候，在传统的程序设计过程中，
         *   通常由调用者来创建被调用者的实例。但在spring中创建被调用者的工作不再由调用者来完成，
         *   因此称为控制反转。创建被调用者的工作由spring来完成，然后注入调用者因此也称为依赖注入。
         *   spring以动态灵活的方式来管理对象 ，
         *        注入的两种方式，设置注入和构造注入。
         *            设置注入的优点：直观，自然
         *            构造注入的优点：可以在构造器中决定依赖关系的顺序。
         *
         */
        UserDao u = new UserDao();
        u.setUserName("666");//1.set注入
        UserDao u1 = new UserDao("555");//2.有参构造注入

        ApplicationContext a1 = new ClassPathXmlApplicationContext("IOC1.xml");

        System.out.println("----------通过xml的set注入---------------------");
        UserDao userDao = a1.getBean("userDao", UserDao.class);
        System.out.println(userDao);//

        System.out.println("----------通过xml的有参构造注入---------------------");
        UserDao userDao1 = a1.getBean("userDao01", UserDao.class);
        System.out.println(userDao1);//
        System.out.println("----------p名称空间的xml的set获取---------------------");
        UserDao userDao2 = a1.getBean("userDao02", UserDao.class);
        System.out.println(userDao2);//
        System.out.println("----------p名称空间的xml的有参构造注入---------------------");
        UserDao userDao3 = a1.getBean("userDao03", UserDao.class);
        System.out.println(userDao3);//

    }

}
