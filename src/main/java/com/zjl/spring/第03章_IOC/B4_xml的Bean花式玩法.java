package com.zjl.spring.第03章_IOC;

import com.zjl.spring.第03章_IOC.bean.Person;
import com.zjl.spring.第03章_IOC.bean.Pet;
import com.zjl.spring.第03章_IOC.bean.User;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class B4_xml的Bean花式玩法 {
    @Test
    public void 工厂Bean() {

        ApplicationContext IOC2 = new ClassPathXmlApplicationContext("IOC2.xml");

        System.out.println("-----------------骚操作-------------------------------------");
        //等等 看    "IOC2.xml"
        User user = IOC2.getBean("user05", User.class);
        System.out.println(user.toString());//.

        System.out.println("*************特殊符号编写*****************************************");
        Pet p = IOC2.getBean("pet03",Pet.class);
        System.out.println(p);//特殊符号编写
        System.out.println("*************其他类型*****************************************");
        Person person = IOC2.getBean("person01", Person.class);
        System.out.println(person);
    }

}

