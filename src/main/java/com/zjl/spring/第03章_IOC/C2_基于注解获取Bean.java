package com.zjl.spring.第03章_IOC;

import com.zjl.spring.第03章_IOC.bean.UserDao;
import com.zjl.spring.第03章_IOC.controller.TestController;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;


public class C2_基于注解获取Bean {

    /**
     * @Autowired
     * @Qualifier
     * @Resource
     * 这三个注解的使用,详见
     * @see TestController
     */
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("IOC注解配置文件混用.xml");

        TestController testController = context.getBean(TestController.class);
        System.out.println(testController.getUserDao());//属性注入

        System.out.println(testController.getUserDao2());//set方法注入
        System.out.println(testController.userDao2);//set方法注入

        System.out.println(testController.userDao3);//构造方法注入
        System.out.println(testController.userDao4);//构造方法注入

    }

}
