package com.zjl.spring.第03章_IOC;

import com.zjl.spring.第03章_IOC.controller.TestController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

//可以代替很多xml的配置
@Configuration//(proxyBeanMethods = false)//这是一个配置类 == 配置文件告诉spring boot这是一个配置类 主要
//组件解扫描 指定包
@ComponentScan({"com.zjl.spring.第03章_IOC.controller","com.zjl.spring.第03章_IOC.bean"})
public class D_完全注解管理Bean {
    public static void main(String[] args) {

        ApplicationContext context =
                new AnnotationConfigApplicationContext(D_完全注解管理Bean.class);
        TestController bean = context.getBean(TestController.class);
        System.out.println(bean.getString());
        System.out.println(bean);
        bean = context.getBean(TestController.class);
        System.out.println(bean);

    }
}
