package com.zjl.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * spring6
 *      jdk要求最低 17
 *      maven 3.6+
 *      spring 6.0.2
 *      idea:2022.1.2
 *
 * --spring.profiles.active=spring
 */
//exclude = DataSourceAutoConfiguration.class   和关闭数据库的配置加载
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
@SpringBootConfiguration
@ComponentScan("com.zjl.spring.第09章_数据校验Validation.E_自定义校验")
//@SpringBootApplication()
public class SpringNApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringNApplication.class, args);
    }

}
/**
 * spring是轻量级的开源JavaEE框架
 *
 * 解决企业应用来发的复杂性
 *
 * Spring核心:
 *      IOC:
 *          控制反转，把创建对象的过程交给Spring进行管理
 *          降低了组件之间的耦合性
 *      AOP:
 *          面向切面，不修改源代码的情况下进行功能的增强
 *          使得业务逻辑各部分之间的耦合度降低
 *
 * 4、Spring 特点
 * （1）方便解耦，简化开发
 * （2）Aop 编程支持
 * （3）方便程序测试
 * （4）方便和其他框架进行整合
 * （5）方便进行事务操作
 * （6）降低 API 开发难度
 *
 *
 * 4个核心包
 *    1.beans3
 *    2.context
 *    3.core
 *    4.expression
 *
 * spring 的优点？
 * 1.降低了组件之间的耦合性 ，实现了软件各层之间的解耦
 * 2.可以使用容易提供的众多服务，如事务管理，消息服务等
 * 3.容器提供单例模式支持
 * 4.容器提供了AOP技术，利用它很容易实现如权限拦截，运行期监控等功能
 * 5.容器提供了众多的辅助类，能加快应用的开发
 * 6.spring对于主流的应用框架提供了集成支持，如hibernate，JPA，Struts等
 * 7.spring属于低侵入式设计，代码的污染极低
 * 8.独立于各种应用服务器
 * 9.spring的DI机制降低了业务对象替换的复杂性
 * 10.Spring的高度开放性，并不强制应用完全依赖于Spring，开发者可以自由选择spring的部分或全部
 *
 *1、Spring 框架概述
 * （1）轻量级开源 JavaEE 框架，为了解决企业复杂性，两个核心组成：IOC 和 AOP
 * （2）Spring5.2.6 版本
 * 2、IOC 容器
 * （1）IOC 底层原理（工厂、反射等）
 * （2）IOC 接口（BeanFactory）
 * （3）IOC 操作 Bean 管理（基于 xml）
 * （4）IOC 操作 Bean 管理（基于注解）
 * 3、Aop
 * （1）AOP 底层原理：动态代理，有接口（JDK 动态代理），没有接口（CGLIB 动态代理）
 * （2）术语：切入点、增强（通知）、切面
 * （3）基于 AspectJ 实现 AOP 操作
 * 4、JdbcTemplate
 * （1）使用 JdbcTemplate 实现数据库 curd 操作
 * （2）使用 JdbcTemplate 实现数据库批量操作
 * 5、事务管理
 * （1）事务概念
 * （2）重要概念（传播行为和隔离级别）
 * （3）基于注解实现声明式事务管理
 * （4）完全注解方式实现声明式事务管理
 * 6、Spring5 新功能
 * （1）整合日志框架
 * （2）@Nullable 注解
 * （3）函数式注册对象
 * （4）整合 JUnit5 单元测试框架
 * （5）SpringWebflux 使用
 *
 */
