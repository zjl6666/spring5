package com.zjl.spring.Spring5新特性;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 2、Spring 5.0 框架自带了通用的日志封装
 * （1）Spring5 已经移除 Log4jConfigListener，官方建议使用 Log4j2
 * （2）Spring5 框架整合 Log4j2
 *
 * 1.引入jar包
 * 2.配置 log4j2.xml
 *
 */
public class 日志 {
    private static final Logger log = LoggerFactory.getLogger(日志.class);

    public static void main(String[] args) {
        log.info("hello Log4j2");
        log.warn("hello Log4j2");
    }
}
