package com.zjl.spring.Spring5新特性.Webflux新功能.函数式编程模型;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserLongServiceImpl implements UserLongService {

    private static  Map<Long, UserLong> users = new HashMap<>();
    public UserLongServiceImpl(){
        this.users.put(1L,new UserLong(10L,"飞飞1","111"));
        this.users.put(2L,new UserLong(11L,"飞飞2","222"));
        this.users.put(3L,new UserLong(12L,"飞飞3","333"));

    }
    @Override
    public Mono<UserLong> getUserLongById(Long id) {
        return Mono.justOrEmpty(this.users.get(id));
    }

    @Override
    public Flux<UserLong> getAllUserLong() {
        return Flux.fromIterable(this.users.values());
    }

    @Override
    public Mono<Void> setUserLong(Mono<UserLong> userLong) {
        return userLong.doOnNext(p->{
            users.put(users.size()+1L,p);
        }).thenEmpty(Mono.empty());//清空
    }
}

