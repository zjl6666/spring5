package com.zjl.spring.Spring5新特性.Webflux新功能.响应式编程;

import java.util.Observable;

/**
 * 2、响应式编程（Java 实现）
 * （1）什么是响应式编程  ： 当我变化是  依赖我的也会变化
 *     响应式编程是一种面向数据流和变化传播的编程范式。
 *     这意味着可以在编程语言中很方便地表达静态或动态的数据流，
 *     而相关的计算模型会自动将变化的值通过数据流进行传播。
 *     比如  a=1  b=2   c=a+b
 *         当a改为 3 时     c 自动改为 3+2 = 5
 *
 * Java8 及其之前版本
 *   * 提供的观察者模式两个类 Observer 和 Observable
 *
 */
public class ObserverDemo extends Observable {
    public static void main(String[] args) {
        //JDK9 有个新的
        JDK8响应式编程();//伪编程
    }

    private static void JDK8响应式编程() {
        ObserverDemo observer = new ObserverDemo();
        //添加观察者
        observer.addObserver((o,arg)->{
            System.out.println("发生了变化");
        });
        observer.addObserver((o,arg)->{
            System.out.println("收到被观察者通知  准备改变");
        });
        System.out.println("48584");

       //输出      收到被观察者通知  准备改变     发生了变化
        observer.setChanged();//监控数据变化
        observer.notifyObservers();//通知
        //一变化就执行   这就叫响应式编程
    }
}
