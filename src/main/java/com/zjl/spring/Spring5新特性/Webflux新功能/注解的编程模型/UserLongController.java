package com.zjl.spring.Spring5新特性.Webflux新功能.注解的编程模型;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class UserLongController{

    @Autowired
    private UserLongService userLongService;

    @GetMapping("/UserLong/{id}")
    public Mono<UserLong> getUserLongById(@PathVariable Long id){
        return userLongService.getUserLongById(id);
    }
    @GetMapping("/UserLong")
    public Flux<UserLong> getAllUserLong(){
        return userLongService.getAllUserLong();
    }
    @PostMapping("/setUserLong")
    public Mono<Void> setUserLong(@RequestBody UserLong userLong){
        Mono<UserLong> userLongMono = Mono.just(userLong);
        return userLongService.setUserLong(userLongMono);
    }

}