package com.zjl.spring.Spring5新特性.Webflux新功能.响应式编程;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * 3、响应式编程（Reactor 实现）     Reactor基于JDK9实现的
 * （1）响应式编程操作中，Reactor 是满足 Reactive 规范框架
 * （2）Reactor 有两个核心类，Mono 和 Flux，这两个类实现接口 Publisher，
 *      提供丰富操作符。
 *     Flux 对象实现发布者，返回 N 个元素；
 *     Mono 实现发布者，返回 0 或者 1 个元素
 * （3）Flux 和 Mono 都是数据流的发布者，
 *     使用 Flux 和 Mono 都可以发出三种数据信号：
 *      元素值，错误信号，完成信号，
 *      错误信号和完成信号都代表终止信号，终止信号用于告诉订阅者数据流结束了，
 *      错误信号终止数据流同时把错误信息传递给订阅者
 *  (4)代码演示 Flux 和 Mono
 *   第一步 引入依赖
 *   第二步 编程代码
 * （5）三种信号特点
 *     错误信号和完成信号都是终止信号，不能共存的
 *     如果没有发送任何元素值，而是直接发送错误或者完成信号，表示是空数据流
 *     如果没有错误信号，没有完成信号，表示是无限数据流
 * （6）调用 just 或者其他方法只是声明数据流，数据流并没有发出，
 *     只有进行订阅之后才会触发数据流，不订阅什么都不会发生的
 * （7）操作符  和Stream的一样
 * * 对数据流进行一道道操作，成为操作符，比如工厂流水线
 *   第一 map            元素映射为新元素
 *   第二 flatMap        可以多合一
 */
public class Reactor实现 {
    public static void main(String[] args) {
        //just 方法直接声明   订阅
        Flux.just(1,2,3,4).map(s->s*=2).subscribe(System.out::print);
        System.out.println();
        Mono.just(1).subscribe(System.out::print);
        //其他的方法
        Integer[] array = {1,2,3,4};
        Flux.fromArray(array);

        List<Integer> list = Arrays.asList(array);
        Flux.fromIterable(list);

        Stream<Integer> stream = list.stream();
        Flux.fromStream(stream);

    }
}
