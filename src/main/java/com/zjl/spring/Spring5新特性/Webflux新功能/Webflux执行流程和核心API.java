package com.zjl.spring.Spring5新特性.Webflux新功能;

import org.springframework.web.server.WebHandler;
import org.springframework.web.server.handler.WebHandlerDecorator;

/**
 * SpringWebflux 基于 Reactor，默认使用容器是 Netty，
 *
 * Netty 是高性能的 NIO 框架，异步非阻塞的框架
 * （1）Netty
 *     BIO
 *        一个Socket对一个输入流对一个Thread
 *     NIO
 *        多个个Channel受一个Selector控制对一个线程
 *（2）SpringWebflux 执行过程和 SpringMVC 相似的
 *   SpringWebflux 核心控制器 DispatcherHandler，
 *   实现接口 WebHandler 接口 WebHandler 有一个方法
 *（3）SpringWebflux 里面 DispatcherHandler，负责请求的处理
 *    HandlerMapping：请求查询到处理的方法
 *    HandlerAdapter：真正负责请求处理
 *    HandlerResultHandler：响应结果处理
 * （4）SpringWebflux 实现函数式编程，两个接口：
 *     RouterFunction（路由处理） 和 HandlerFunction（处理函数）
 *
 *
 */
public class Webflux执行流程和核心API {
    public static void main(String[] args) {
        WebHandler webHandler = new WebHandlerDecorator(null);
    }
}
