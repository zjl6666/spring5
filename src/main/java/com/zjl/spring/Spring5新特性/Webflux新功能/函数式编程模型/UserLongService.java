package com.zjl.spring.Spring5新特性.Webflux新功能.函数式编程模型;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserLongService {

    Mono<UserLong> getUserLongById(Long id);//根据id查询

    Flux<UserLong> getAllUserLong();//查询所有

    Mono<Void> setUserLong(Mono<UserLong> userLong);//添加


}
