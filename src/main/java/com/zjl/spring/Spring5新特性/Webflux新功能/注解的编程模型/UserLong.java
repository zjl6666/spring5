package com.zjl.spring.Spring5新特性.Webflux新功能.注解的编程模型;

import lombok.*;

@ToString//toString方法
@Data//get set方法
@NoArgsConstructor//无参构造器
@AllArgsConstructor//全参构造器
@EqualsAndHashCode//EqualsAndHashCode方法
public class UserLong {
    private Long id;
    private String username;
    private String password;
}
