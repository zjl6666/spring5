package com.zjl.spring.Spring5新特性.Webflux新功能;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 *SpringWebflux（基于注解编程模型）
 * SpringWebflux 实现方式有两种：  注解编程模型和函数式编程模型
 *   使用注解编程模型方式，和之前 SpringMVC 使用相似的，
 *   只需要把相关依赖配置到项目中，
 * SpringBoot 自动配置相关运行容器，默认情况下使用 Netty 服务器
 * 第一步 创建 SpringBoot 工程，引入 Webflux 依赖
 * 第二步 配置启动端口号
 * 第三步 创建包和相关类
 *
 * ⚫ 说明
 * SpringMVC 方式实现，同步阻塞的方式，基于 SpringMVC+Servlet+Tomcat
 * SpringWebflux 方式实现，异步非阻塞 方式，基于 SpringWebflux+Reactor+Netty
 *
 *
 */
@EnableAutoConfiguration
@SpringBootConfiguration
@ComponentScan("com.zjl.spring5.Spring5新特性.Webflux新功能.注解的编程模型")//spring5测试
/**
 * 必须在    主启动类里配置   SpringDemoApplication  否则无用
 * 这里面存放的是  启动类的配置应该为什么
 */
//@SpringBootApplication(scanBasePackages = "com.zjl.spring.Spring5新特性.Webflux新功能.注解的编程模型")
public class 基于注解的编程模型 {//启动类最低要在controller目录，service层，和dao层的类 的上一层就
    //简单来说  就是要在有其他java文件的上一层
    // 路径的返回   用到UserLongController
    public static void main(String[] args) { //
        ConfigurableApplicationContext run = SpringApplication.run(基于注解的编程模型.class, args);

    }


}
