package com.zjl.spring.Spring5新特性;

import org.springframework.context.support.GenericApplicationContext;

/**
 * 函数式风格GenericApplicationContext/AnnotationConfigApplicationContext
 *
 */
public class 支持函数式编程 {
    public static void main(String[] args) {
        GenericApplicationContext context = new GenericApplicationContext();
        context.refresh();
        context.registerBean(Main.class,() -> new Main());
        Main bean = (Main)context.getBean("com.zjl.spring.Spring5新特性.Main");

        context.registerBean("user1",Main.class,() -> new Main());
        Main bean1 = (Main)context.getBean("user1");

        System.out.println(bean);
        System.out.println(bean1);
    }
}

