package com.zjl.spring.Spring5新特性;

import org.springframework.lang.Nullable;

/**
 *（1）@Nullable 注解可以使用在方法上面，属性上面，参数上面，
 *    表示方法返回可以为空，属性值可以为空，参数值可以为空
 *      1.注解用在方法上面，方法返回值可以为空
 *      2.注解使用在方法参数里面，方法参数可以为空
 *      3.注解使用在属性上面，属性值可以为空
 *
 */

public class Nullable注解 {

    @Nullable
    private String name;
    public static void main(String[] args) {
        add(1,2);
    }
    @Nullable
    public static int add(int a,int b){
        return a+b;
    }
}
