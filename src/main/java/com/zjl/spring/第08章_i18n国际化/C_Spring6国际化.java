package com.zjl.spring.第08章_i18n国际化;

import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.time.LocalDateTime;
import java.util.Locale;

/**
 * spring中国际化是通过 MessageSource 这个接口来支持的
 * 常见实现类
 * ResourceBundleMessageSource
 *      这个是基于Java的ResourceBundle基础类实现，允许仅通过资源名加载国际化资源
 * ReloadableResourceBundleMessageSource
 *      这个功能和第一个类的功能类似，多了定时刷新功能，允许在不重启系统的情况下，更新资源的信息
 * StaticMessageSource
 *      它允许通过编程的方式提供国际化信息，一会我们可以通过这 个来实现db中存储国际化信息的功能。
 */
public class C_Spring6国际化 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("i18n.xml");

        Object[] objects = {"xxx", LocalDateTime.now()};
        //code的 值中的  {0} {1}   对应着入参
        String cn = context.getMessage("spring.internationalization", objects, Locale.CHINA);
        String us = context.getMessage("spring.internationalization", objects, Locale.US);
        String gb = context.getMessage("spring.internationalization", objects, Locale.UK);
        System.out.println(cn);
        System.out.println(us);
        System.out.println(gb);
    }
}
