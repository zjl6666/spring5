package com.zjl.spring.第08章_i18n国际化;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Java自身是支持国际化的，java.util.Locale用于指定当前用户所属的语言环境等信息,
 * java.til.ResourceBundle 用于查找绑定对应的资源文件。
 * Locale包含了language信息和country信息，Locale创建默认locale对象时使用的静态方法:
 *
 *   基本名字    国家    语言
 * **basename_language_country.properties
 *
 * 必须遵循以上的命名规则，java才会识别。其中，basename 是必须的，语言和国家是可选的。这里存在一个优先
 * 级概念，如果同时提供了messages.properties和 messages_zh_CN.properties 两个配置文件,
 * 如果提供的locale符合en_CN,那么优先查找messages_en_CN.properties 配置文件，
 * 如果没查找到，再查找 messages.properties
 * 配置文件。最后，提示下，所有的配置文件必须放在 classpath 中，一般放在resources目录下
 *
 * 最外层的文件是自己出来的
 *
 */
public class B_Java国际化 {
    @Test//乱码问题可以查看配置文件的编码格式
    public void zhCN() throws Exception {
        Locale locale = new Locale("zh", "CN");

        ResourceBundle messages = ResourceBundle.getBundle("messages", locale);
        String name = messages.getString("name");
        String s = new String(name.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
        System.out.println(name);
    }
    @Test
    public void enGB() throws Exception {
        Locale locale = new Locale("en", "GB");

        ResourceBundle messages = ResourceBundle.getBundle("messages", locale);
        String name = messages.getString("name");
        System.out.println(name);
    }
}
