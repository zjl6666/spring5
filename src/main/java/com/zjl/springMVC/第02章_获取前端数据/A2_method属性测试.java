package com.zjl.springMVC.第02章_获取前端数据;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * ClassName: A2_method属性测试
 * Package: com.zjl.springMVC.第02章_常用注解
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/22 21:52
 * @Version 1.0
 * http://127.0.0.1:8080/methodTest
 * http://127.0.0.1:8080/method
 */
@Controller
public class A2_method属性测试 {


    @RequestMapping("/methodTest")
    public String method(){
        return "restful";
    }

    /**
     * 	请求在  DispatcherServlet这个类的doDispatch方法
     * 	protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
     * ● SpringBoot自动配置欢迎页的 WelcomePageHandlerMapping 。访问 /能访问到index.html；
     * ● SpringBoot自动配置了默认 的 RequestMappingHandlerMapping
     * ● 请求进来，挨个尝试所有的HandlerMapping（处理器映射）看是否有请求信息。
     *   ○ 如果有就找到这个请求对应的handler
     *   ○ 如果没有就是下一个HandlerMapping
     *
     */

    //@RequestMapping(value = "/method",method = RequestMethod.GET)  //同下
    @GetMapping("/method/{id}")
    @ResponseBody
    public String getUser(
            @PathVariable("id") Integer id//达到指定的路径变量的值
    ){
        return "id的值"+id+"传递方式GET-张三";
    }

    //@RequestMapping(value = "/method",method = RequestMethod.POST) //同下
    @PostMapping("/method")
    @ResponseBody
    public String postUser(String userName,String email){
        return "邮箱："+userName+"密码："+email+"传递方式POST-张三";
    }

    /**
     * 想要PUT和DELETE可用  只好用  postman
     * 需要使用过滤器见 hiddenHttpMethodFilter 方法 暂无
     * @see com.zjl.springMVC.SpringMVCApplication
     *
     * @RequestMapping(value = "/method",method = RequestMethod.PUT)//同下
     * @return
     */
    @PutMapping("/method")//同上
    @ResponseBody
    public String putUser(){
        return "传递方式PUT-张三";
    }

    //@RequestMapping(value = "/method",method = RequestMethod.DELETE)//同下
    @DeleteMapping("/method")//同上
    @ResponseBody
    public String deleteUser(){
        return "传递方式DELETE-张三";
    }
}
