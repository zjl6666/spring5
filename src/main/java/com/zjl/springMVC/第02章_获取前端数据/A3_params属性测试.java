package com.zjl.springMVC.第02章_获取前端数据;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * ClassName: A2_method属性测试
 * Package: com.zjl.springMVC.第02章_常用注解
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/22 21:52
 * @Version 1.0
 * http://127.0.0.1:8080/params
 */
@Controller
public class A3_params属性测试 {

    /**
     *
     *  params = {"username"}   表示必须有一个传参为username
     *  params = {"!username"}   表示不能有传参为username的
     *  params = {"username=555"}
     *            要求请求映射所匹配的请求必须携带username请求参数且username=555
     *  params = {"username!=555"}
     *            要求请求映射所匹配的请求必须携带username请求参数且username！=555
     *
     * headers  同理于 params ，只不过headers限制的是报文头
     */
    @RequestMapping(
            name = "起名，无实际作用"
            //表示必须有username这个信息而且值必须是555   链接?username=555
            ,params = {"username=555","password=123456"}
            ,value = {"/params"}//访问路径 可写多个，满足一个即可
    )
    @ResponseBody
    public String hello(){
        return "测试成功";//templates 会自己找  指定字符.html
    }
}
