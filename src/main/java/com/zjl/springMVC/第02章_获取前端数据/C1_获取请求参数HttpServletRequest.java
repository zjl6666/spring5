package com.zjl.springMVC.第02章_获取前端数据;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import javax.servlet.http.HttpServletRequest;//JDK 低版本使用

import jakarta.servlet.http.HttpServletRequest;//JDK高版本使用 也不可用

/**
 *
 * TODO 由于我用的是 JDK17 Spring Boot 3.x 无法使用 HttpServletRequest 因为 低版本 的javax相关的包都改成jakarta包了，导致很多问题
 *
 *
 */
//@ResponseBody//表示返回值是返回体的内容
//@Controller
//@RestController//这个等于上面两个
public class C1_获取请求参数HttpServletRequest {

//    @RequestMapping("/HttpServletRequest")
    //现在不用 HttpServletRequest 有更好的   且由于jdk版本原因   JDK 9不可使用
    public String ServletRequest(HttpServletRequest request){
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        System.out.println(username+password);
        return request.toString();

    }


}
