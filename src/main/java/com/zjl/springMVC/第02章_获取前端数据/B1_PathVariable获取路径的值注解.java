package com.zjl.springMVC.第02章_获取前端数据;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 *
 * SpringMVC支持路径中的占位符（重点）
 *
 * 原始方式：/deleteUser?id=1
 * rest方式：/deleteUser/1
 *
 * 表示传输的数据，在通过 @PathVariable 注解，
 * 将占位符所表示的数据赋值给控制器方法的形参
 *
 */
@Controller
@RequestMapping(value = "/PathVariable")
@ResponseBody
public class B1_PathVariable获取路径的值注解 {
    @RequestMapping(value = "/test/{username}/{id}")
    public String ant1(
            //获取路径的值   必须添加此 占位符
            @PathVariable("id") String id,
            @PathVariable("username") String username,
            @PathVariable Map<String,String> pv//获取所有路径的变量信息
            ){
        System.out.println(pv);
        return "id为：" + id + "\tusername为：" + username;
    }
}
