package com.zjl.springMVC.第02章_获取前端数据;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: C2_
 * Package: com.zjl.springMVC.第02章_与前端交互
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/23 22:07
 * @Version 1.0
 */
@Controller
@ResponseBody
@RequestMapping("/RequestParam")
//http://127.0.0.1:8080/RequestParam/test2?userName=002&email=666
//http://127.0.0.1:8080/RequestParam/test?userName=000&email=888wsad
public class C2_RequestParam注解获取路径问号后 {

    @RequestMapping("/test2")
    public Map<String,Object> test2(
            //获取请求体数据   变量名必须和 入参的值一致  否则获取不到，但是可以不用传入
            String userName,
            String email){
        Map<String,Object> map=new HashMap<>();
        map.put("userName",userName);
        map.put("email",email);
        System.out.println(map);
        return map;
    }
    //上下两种是等价的   默认就是  @RequestParam 的值变量名一致
    @RequestMapping("/test")//获取请求体数据
    public Map<String,Object> test(
            @RequestParam Map<String,Object> variableAll,//获取所有？后的数据
            @RequestParam(name = "userName") String userName,
            //value 与  name 一样  指定 参数名字
            // required 默认true 必须有此参数  false 可以不写
            // defaultValue 默认值 不写或传参为空 会改为默认值
            @RequestParam(name = "email",required = false,defaultValue = "888@qq.com")
            String email
    ){

        System.out.println("userName:" + userName);
        System.out.println("email:" + email);
        System.out.println(variableAll);
        return variableAll;
    }

}
