package com.zjl.springMVC.第02章_获取前端数据;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class},
        scanBasePackages = {"com.zjl.springMVC.第02章_获取前端数据"}
)
public class SpringMVC02Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringMVC02Application.class, args);
    }

}

