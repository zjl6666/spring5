package com.zjl.springMVC.第02章_获取前端数据;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 与
 * @see C2_RequestParam注解获取路径问号后 用法一致
 * http://127.0.0.1:8080/RequestHeader/test
 */
@Controller
@ResponseBody
@RequestMapping("/RequestHeader")
public class C3_RequestHeader注解获取请求头 {

    @RequestMapping("/test")//获取请求体数据
    public Map<String,Object> test(
            @RequestHeader Map<String,Object> headerAll,//获取所有请求头
            @RequestHeader(name = "Accept") String Accept,
            //value 与  name 一样  指定 参数名字
            // required 默认true 必须有此参数  false 可以不写
            // defaultValue 默认值 不写或传参为空 会改为默认值
            @RequestHeader(name = "head1",required = false,defaultValue = "我是大头")
            String head
    ){

        System.out.println("Accept:" + Accept);
        System.out.println("head:" + head);
        System.out.println(headerAll);
        return headerAll;
    }

}
