package com.zjl.springMVC.第02章_获取前端数据;

//import javax.servlet.http.Cookie;
//import jakarta.servlet.http.Cookie;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 获取前端 Cookie
 * @see C3_RequestHeader注解获取请求头 用法一致
 * TODO 由于我用的是 JDK17 Spring Boot 3.x 无法使用 HttpServletRequest，无法获取所有 Cookie
 *
 * http://127.0.0.1:8080/CookieValue/test
 *
 * 由于  Cookie 也是保存在 请求头中，所以可以使用获取请求头的方式  获取
 *
 */
@Controller
@ResponseBody
@RequestMapping("/CookieValue")
public class C4_CookieValue获取Coolie {

    @RequestMapping("/test")//获取请求体数据
    public String test(
            //value 与  name 一样  指定 参数名字
            // required 默认true 必须有此参数  false 可以不写
            // defaultValue 默认值 不写或传参为空 会改为默认值
            @RequestHeader(name = "Cookie") String Cookie,//获取所有 Cookie
            // @CookieValue("_ga") Cookie cookie,//JDK17 和 Spring Boot3.x版本问题 不可使用
            @CookieValue(name = "head",required = false,defaultValue = "我是Cookie")
            String head
    ){

        System.out.println("head:" + head);
        return Cookie;
    }

}
