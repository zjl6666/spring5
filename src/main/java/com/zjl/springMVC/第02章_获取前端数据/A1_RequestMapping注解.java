package com.zjl.springMVC.第02章_获取前端数据;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @RequestMapping注解
 * ### 1、@RequestMapping注解的功能
 * 从注解名称上我们可以看到，
 * @RequestMapping 注解的作用就是将请求和处理请求的控制器方法关联起来，建立映射关系。
 * SpringMVC 接收到指定的请求，就会来找到在映射关系中对应的控制器方法来处理这个请求。
 * ### 2、@RequestMapping注解的位置
 * @RequestMapping 标识一个类：设置映射请求的请求路径的初始信息
 * @RequestMapping 标识一个方法：设置映射请求请求路径的具体信息
 */

@Controller //开启控制器  表示这是一个控制器
//@RequestMapping("/RequestMapping")
public class A1_RequestMapping注解 {//建议用 Postman  工具

    /**
     * 浏览器发送请求，若请求地址符合前端控制器的url-pattern，
     * 该请求就会被前端控制器DispatcherServlet处理。
     * 前端控制器会读取SpringMVC的核心配置文件，通过扫描组件找到控制器，
     * 将请求地址和控制器中 @RequestMapping 注解的value属性值进行匹配，
     * 若匹配成功，该注解所标识的控制器方法就是处理请求的方法。
     * 处理请求的方法需要返回一个字符串类型的视图名称，
     * 该视图名称会被视图解析器解析，
     * 加上前缀和后缀组成视图的路径，通过 Thymeleaf 对视图进行渲染，
     * 最终转发到视图所对应页面
     * @RequestMapping 写在类上  表示其方法所有的地址都要加他的端口
     * @RequestMapping 写在方法上
     *
     */
    @RequestMapping(value = "/index")//请求地址必须唯一   否者报错，
//    @ResponseBody//这个地址转换  不能使用 此注解，此注解的意思是返回值是数据，就不能被 templates 所识别了
    public String index(Model model){
        model.addAttribute("msg",5);
        return "index";//
    }

    /**
     * value = {"/www","/ww"}  访问路径 可以多个  至少有一个 默认是value
     *
     * 默认  任何方式都可以  相当与全部加上了
     * method = RequestMethod.POST    //一般用于添加   速度慢   很安全   无大小限制
     * method = RequestMethod.GET     //一般用于查询   速度快   不安全   有大小限制
     * method = RequestMethod.DELETE  //一般用于删除
     * method = RequestMethod.PUT     //一般用于修改
     * 1、对于处理指定请求方式的控制器方法，
     *   SpringMVC中提供了@RequestMapping的派生注解
     *  处理get请求的映射-->@GetMapping
     *  处理post请求的映射-->@PostMapping
     *  处理put请求的映射-->@PutMapping
     *  处理delete请求的映射-->@DeleteMapping
     *  2、常用的请求方式有get，post，put，delete
     *  但是目前浏览器只支持get和post，若在 form 表单提交时，
     *  为 method 设置了其他请求方式的字符串（put或delete照默认的请求方式get处理
     *  若要发送put和delete请求，
     *  则需要通过spring提供的过滤器 HiddenHttpMethodFilter
     *
     *  params = {"username"}   表示必须有一个传参为username
     *  params = {"!username"}   表示不能有传参为username的
     *  params = {"username=555"}
     *            要求请求映射所匹配的请求必须携带username请求参数且username=555
     *  params = {"username!=555"}
     *            要求请求映射所匹配的请求必须携带username请求参数且username！=555
     *
     * headers  同理于 params ，只不过headers限制的是报文头
     *
     * 404: 请求头信息不匹配 headers 不匹配
     * 405：请求方式不匹配  GET POST DELETE PUT
     * 400：请求参数不匹配  params 不匹配
     *
     */
    @RequestMapping(
            name = "起名，无实际作用"
            //GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
            //,method = {RequestMethod.GET,RequestMethod.GET}//满足一个即可,根据请求方式 请求  即 POST 请求 或 GET 请求

            // params headers value  这三者，可以只 存在部分，

            //表示必须有username这个信息而且值必须是555   链接?username=555
            ,params = {"username=555"}
            //表示请求头必须有这个才能访问 和 params 格式相同
            ,headers = {"Host=localhost:8080"}//有多个，必须全部满足
            ,value = {"/xxxx"}//访问路径 可写多个，满足一个即可

            //请求提交的内容 必须是  json格式
            ///*/* 就是所有都可
            ,consumes = "application/json"

            // 制定返回值的格式    */*  表示都可以
            //text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7
            ,produces = "text/html"
    )
    public String hello(){

        return "hello";//templates 会自己找  指定字符.html
    }

}
