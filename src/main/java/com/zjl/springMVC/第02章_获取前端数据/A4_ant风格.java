package com.zjl.springMVC.第02章_获取前端数据;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 *  SpringMVC支持ant风格的路径
 *
 * ?：表示任意的单个字符  //不能用   /   ?
 * *：表示任意的0个或多个字符
 * **：表示任意的一层或多层目录
 * 注意：在使用 **时，只能使用   / ** /  即 ** 要在两个/之间  否则就表示 单纯的**
 * xxx的方式
 * @RequestMapping("/a?a")   表示路径?可以随意代替  不能用   /   ?
 *
 *
 */
@Controller
@RequestMapping(value = "/ant")
@ResponseBody
public class A4_ant风格 {

    @RequestMapping(value = "/a?a")  //
    public String ant1(){
        return "/ant/a?a 都可访问";
    }
    @RequestMapping(value = "/b*")
    public String ant2(){
        return "/ant/b* 都可访问";
    }
    @RequestMapping(value = "/**")// **  左右不可有除/外字符，否则会认为**为单纯的两个 *号字符
    public String ant3(){
        return "/**   都可访问";
    }

}
