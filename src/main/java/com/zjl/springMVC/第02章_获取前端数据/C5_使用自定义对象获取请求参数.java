package com.zjl.springMVC.第02章_获取前端数据;

import lombok.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ServletModelAttributeMethodProcessor;

/**
 * 使用 自定义对象获取请求参数  ?之后的
 *
 * http://127.0.0.1:8080/pojo/test?name=张蛟龙&age=25&pet.name=宠物&pet.weight=66
 *
 */
@Controller
@ResponseBody
@RequestMapping("/pojo")
public class C5_使用自定义对象获取请求参数 {
    /**
     * 自定义类型参数 封装POJO
     * ServletModelAttributeMethodProcessor  这个参数处理器支持
     *  是否为简单类型。
     *
     *  @FunctionalInterfacepublic interface Converter<S, T>
     *      converter那个可以将这个数据类型（request带来参数的字符串）
     *      转换到指定的类型（JavaBean -- Integer）
     */
    @RequestMapping("/test")//获取请求体数据
    public String test(User user){

        System.out.println(user);
        return user.toString();
    }

}
@ToString//toString方法
@Data//get set方法
@NoArgsConstructor//无参构造器
@AllArgsConstructor//全参构造器
@EqualsAndHashCode//EqualsAndHashCode方法
class User{
    private String name;
    private Integer age;
    private Pet pet;

}
@ToString//toString方法
@Data//get set方法
@NoArgsConstructor//无参构造器
@AllArgsConstructor//全参构造器
@EqualsAndHashCode//EqualsAndHashCode方法
class Pet {
    private String name;
    private Double weight;
}
