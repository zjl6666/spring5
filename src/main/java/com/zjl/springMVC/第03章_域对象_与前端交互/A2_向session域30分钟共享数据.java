package com.zjl.springMVC.第03章_域对象_与前端交互;

import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

//import javax.servlet.http.HttpSession;
/**
 *
 * TODO 由于此版本过高，都不可使用
 */
@Controller
public class A2_向session域30分钟共享数据 {
//作用范围：存活时间内（默认30分钟），一次会话内有效，转发和重定向都有效；
    @GetMapping("/sessionGX")
    public String sessionGX(HttpSession session){
        session.setAttribute("mag","HttpSession");
        return "success";//设置视图名称 转发到/success请求
        //前端获取方式有所不同 详见
        //页面见templates/success.html
        //也会封装到ModelAndView里
    }

}
/**
 * session 的钝化和活化，就是服务器突然gg，
 * 需要将 session 中的内容保存到硬盘，这叫钝化，
 * 服务器重启，将钝化的东西读取到 session 中 叫活化
 * session 和服务器是否关闭没关系，只和浏览器是否关闭有关系
 * 钝化和活化水针对session中的对象，而不是session本身
 *
 * cookie携带jsessionID
 * request对象只创建一次  所以可以共享数据
 *
 * 实际上就是说数据只是使用在一次请求中就没必要放在范围更大的session域，
 * 浪费了session域的空间
 *
 *
 */
