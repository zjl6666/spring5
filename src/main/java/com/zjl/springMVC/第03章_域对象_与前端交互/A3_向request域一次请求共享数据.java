package com.zjl.springMVC.第03章_域对象_与前端交互;


import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

//import javax.servlet.http.HttpServletRequest;
import java.util.Map;
/**
 *
 * TODO 由于此版本过高，都不可使用
 */
@Controller
public class A3_向request域一次请求共享数据 {//页面见templates/success.html
//向前端发送信息
//        作用范围：一次请求范围内，转发有效重定向失效；
    @GetMapping("/requestGX")
    public String requestGX(HttpServletRequest request){
        request.setAttribute("mag","HttpServletRequest");
        request.setAttribute("class",request.getClass().getName());
        return "success";//设置视图名称 转发到/success请求
    }
    @GetMapping("/modelAndViewGX")
    public ModelAndView modelAndViewGX(){
        ModelAndView mav = new ModelAndView();
        mav.addObject("mag","ModelAndView");
        mav.addObject("class",mav.getClass().getName());
        mav.setViewName("success");//设置视图名称 //转发到/success请求
        return mav;//转发到/success请求
    }
    /**
     * Map、Model ModelMap
     * （map、model里面的数据会被放在request的请求域
     * request.setAttribute）
     *
     * Map、Model ModelMap（这仨个相当于同一个对象）类型的参数会返回
     * org.springframework.validation.support.BindingAwareModelMap
     * mavContainer.getModel（）；
     * ---> BindingAwareModelMap 是Model 也是Map
     * mavContainer.getModel(); 获取到值的
     *
     */
    @GetMapping("/modelGX")
    public String modelGX(Model model){
        model.addAttribute("mag","Model");
        model.addAttribute("class",model.getClass().getName());
        return "success";//设置视图名称 转发到/success请求
    }
    @GetMapping("/mapGX")
    public String testParam(Map<String,Object> map){
        map.put("mag","Map");
        map.put("class",map.getClass().getName());
        return "success";//设置视图名称 转success请求
    }
    @GetMapping("/modelMapGX")
    public String modelMapGX(ModelMap modelMap){
        modelMap.addAttribute("mag","ModelMap的addAttribute");
        modelMap.put("mag1","ModelMap的put");
        modelMap.put("class",modelMap.getClass().getName());
        return "success";//设置视图名称 转success请求
    }
/**
 * 前端给 DispatcherServlet的 doDispatch 方法开始 (通过方法栈  可以找到)
 * doDispatch 用反射调用 @GetMapping("/requestGX")这些方法
 * 经过这些方法的封装 （除了HttpServletRequest外  因为这个用的是原生的）
 * 以上的方法 的信息都封装在一个 ModelAndView 中
 * M：Model，模型层  信息
 * V：View，视图层   前端页面
 *
 * 在发送给前端
 *
 *
 */
}


