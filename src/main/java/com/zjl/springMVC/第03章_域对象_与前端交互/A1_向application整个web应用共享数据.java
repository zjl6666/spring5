package com.zjl.springMVC.第03章_域对象_与前端交互;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

//import javax.servlet.ServletContext;
//import javax.servlet.http.HttpSession;

/**
 *
 * TODO 由于此版本过高，都不可使用
 */
@Controller
public class A1_向application整个web应用共享数据 {
//作用范围：整个web应用；
    @GetMapping("/applicationGX")
    public String applicationGX(HttpSession session){
        //他的域对象是整个工程  因为ServletContext在项目启动就创建一个
        //可以增删改查
        ServletContext application = session.getServletContext();

        application.setAttribute("mag","application");
//        application.removeAttribute();
        return "success";//设置视图名称 转发到/success请求
        //前端获取方式有所不同 详见
        //页面见templates/success.html
        //也会封装到ModelAndView里
    }

}
