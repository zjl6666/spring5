package com.zjl.springMVC.第03章_域对象_与前端交互;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class},
        scanBasePackages = {"com.zjl.springMVC.第03章_域对象_与前端交互"}
)
public class SpringMVC03Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringMVC03Application.class, args);
    }

}

