package com.zjl.springMVC.第01章_概述;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ClassName: HelloController
 * Package: com.zjl.springMVC.第01章_概述
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/22 20:28
 * @Version 1.0
 * http://127.0.0.1:8080/hello
 */
@Controller
public class HelloController {

    @RequestMapping("/hello")
    public String hello(){

        return "hello";//templates 会自己找  指定字符.html

    }

    @RequestMapping("/target")
    public String toTarget(){

        return "target";//templates 会自己找  指定字符.html

    }
}
