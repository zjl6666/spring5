package com.zjl.springMVC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.filter.HiddenHttpMethodFilter;

/**
 * spring6
 *      jdk要求最低 17
 *      maven 3.6+
 *      spring 6.0.2
 *      idea:2022.1.2
 *
 * --spring.profiles.active=MVC
 */
//exclude = DataSourceAutoConfiguration.class   和关闭数据库的配置加载
//@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
//@SpringBootConfiguration
//@ComponentScan("com.zjl.springMVC")
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class},
        scanBasePackages = {"com.zjl.springMVC"}
)
public class SpringMVCApplication {
    @Bean
    public HiddenHttpMethodFilter hiddenHttpMethodFilter(){
        HiddenHttpMethodFilter methodFilter=new HiddenHttpMethodFilter();
        methodFilter.setMethodParam("_method");
        return  methodFilter;
    }
    public static void main(String[] args) {
        SpringApplication.run(SpringMVCApplication.class, args);
    }

}

