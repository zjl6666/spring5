package com.zjl.MyBatis.第01章_初入;

/**
 * 1、MyBatis历史
 *      MyBatis最初是Apache的一个开源项目iBatis,
 *      2010年6月这个项目由Apache Software Foundation迁移到了Google Code.
 *      随着开发团队转投Google Code旗下，iBatis3.xI正式更名为MyBatis。
 *      代码于2013年11月迁移到Github。
 *      iBatis一词来源于"internet"和"abatis"的组合,是一个基于Java的持久层框架。
 *      iBatis提供的持久层框架包括SQL Maps和Data Access Objects (DAO) 。
 * 2、MyBatis特性
 *      1) MyBatis 是支持定制化SQL、存储过程以及高级映射的优秀的持久层框架
 *      2) MyBatis 避免了几乎所有的JDBC代码和手动设置参数以及获取结果集
 *      3) MyBatis可以使用简单的XML 或注解用于配置和原始映射，
 *           将接口和Java的POJO (Plain Old Java Objects,普通的Java对象)映射成数据库中的记录
 *      4) MyBatis 是一个半自动的ORM (Object Relation Mapping) 框架
 *
 *
 */
public class A_简介 {
}
