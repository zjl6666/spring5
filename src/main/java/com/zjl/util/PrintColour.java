package com.zjl.util;

/**
 * ClassName: PrintColour
 * Package: com.zjl.springbatch.util
 * Description:
 *
 * @Author 张蛟龙
 * @Create 2024/7/31 20:41
 * @Version 1.0
 */
public class PrintColour {
    public static void main(String[] args) {
        System.out.println("普通输出的白色");
        System.out.format("\33[30;1m30:黑色%n");
        System.out.format("\33[31;1m31:红色%n");
        System.out.format("\33[32;1m32:绿色%n");
        System.out.format("\33[33;1m33:不知道什么黄的黄色%n");
        System.out.format("\33[34;1m34:蓝色%n");
        System.out.format("\33[35;1m35:紫色%n");
        System.out.format("\33[36;1m36:蒂芙尼蓝%n");
        System.out.format("\33[37;1m37:深灰色%n");
        System.out.format("\33[38;1m38:浅灰色%n");
        System.out.println("********************************************************************");

        System.out.format("\33[40;1m40:背景黑色%n");
        System.out.format("\33[41;1m41:背景红色%n");
        System.out.format("\33[42;1m42:背景绿色%n");
        System.out.format("\33[43;1m43:背景不知道什么黄的黄色%n");
        System.out.format("\33[44;1m44:背景蓝色%n");
        System.out.format("\33[45;1m45:背景紫色%n");
        System.out.format("\33[46;1m46:背景蒂芙尼蓝色%n");
        System.out.format("\33[47;1m47:背景深灰色%n");
        System.out.format("\33[48;1m48:背景浅灰色%n");

    }

    // ANSI转义代码  \u001B = \33
    public final static String ANSI_RESET = "\u001B[0m";//停止
    public final static String ANSI_RED = "\u001B[31;1m";//红色
    public final static String ANSI_GREEN = "\u001B[32m";//绿色
    public final static String ANSI_YELLOW = "\u001B[33;1m";//黄色
    public final static String ANSI_BLUE = "\u001B[34m";//蓝色
    public final static String ANSI_PURPLE = "\u001B[35m";//紫色

    public static void printlnGreen(Object... out) {
        println(ANSI_GREEN,out);
    }
    public static void printlnPurple(Object... out) {
        println(ANSI_PURPLE,out);
    }
    public static void printlnRed(Object... out) {
        println(ANSI_RED,out);
    }

    public static void printlnBlue(Object... out) {
        println(ANSI_BLUE,out);
    }

    public static void printlnYellow(Object... out) {
        println(ANSI_YELLOW,out);
    }
    public static void println(String colour,Object... out) {
        StringBuilder sb = new StringBuilder();
        sb.append(colour);
        out(sb, out);
        sb.append(ANSI_RESET);
        System.out.println(sb);
    }

    private static StringBuilder out(StringBuilder sb, Object... out) {
        for (Object o : out) {
            if(o instanceof Object[] values){
                out(sb,values);
            }else{
                sb.append(o);
            }
        }
        return sb;
    }
}
