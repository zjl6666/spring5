/*
 Navicat Premium Dump SQL

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80012 (8.0.12)
 Source Host           : localhost:3306
 Source Schema         : springbatch

 Target Server Type    : MySQL
 Target Server Version : 80012 (8.0.12)
 File Encoding         : 65001

 Date: 18/08/2024 19:01:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (1, '㦯痼仱', 41, '男');
INSERT INTO `employee` VALUES (2, '蝬㗗蹸', 29, '男');
INSERT INTO `employee` VALUES (3, '寺赕貢', 46, '女');
INSERT INTO `employee` VALUES (4, '篊㰟鰲', 18, '男');
INSERT INTO `employee` VALUES (5, '歃勈躾', 70, '男');
INSERT INTO `employee` VALUES (6, '逷氯憾', 33, '男');
INSERT INTO `employee` VALUES (7, '额搝忋', 93, '男');
INSERT INTO `employee` VALUES (8, '鞹飴銐', 23, '女');
INSERT INTO `employee` VALUES (9, '㱰蒠讲', 82, '男');
INSERT INTO `employee` VALUES (10, '咶䮗灨', 29, '男');
INSERT INTO `employee` VALUES (11, '㨖䞼愔', 94, '女');
INSERT INTO `employee` VALUES (12, '洔馵択', 41, '男');

SET FOREIGN_KEY_CHECKS = 1;
